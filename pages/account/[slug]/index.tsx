import {useRouter} from "next/router"
import {useTranslation} from "react-i18next"

/*---Components---*/
import MainComponent from "../../../components/Main-Component"
import {Order} from '../../../components/pages-component/account/Order'
import {PaymentMethod} from '../../../components/pages-component/account/Payment-method'
import {Addresses} from "../../../components/pages-component/account/Addresses"
import {PersonalDetails} from "../../../components/pages-component/account/Personal-details"
import {UserProducts} from "../../../components/pages-component/account/User-products"

/*---VirtDb----*/
import {link} from "../../../virtDb/arrLinksAccount"

/*---Icons----*/
import {TimeClock} from "../../../components/icons/time-oclock"
import {Card} from "../../../components/icons/Card"
import {AddressBook} from "../../../components/icons/Address-book"
import {PersonCircle, ChevronLeft, List} from "react-bootstrap-icons"



/*----Bootstrap----*/
import {Container} from 'react-bootstrap'
import classes from "../../../styles/pages-components/account/account-slug.module.sass";
import Link from "next/link";

/*------Redux-------*/
import {connect} from 'react-redux'
import {useState} from "react";


/*--Interface----*/
interface IObject{
    id: number,
    slug: string,
    name: {
        ru: string,
        uz: string
    },
    description: string,
    icon: string,
    link_name: string
}



function AccountRoutes({regions, district}: {regions:[], district:[]}){
    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    const [apiErrors, setApiErrors] = useState({})
    const [isSuccess, setIsSuccess] = useState(false)
    const [alertSuccess, setAlertSuccess] = useState('')
    const [errors, setErrors] = useState([])
    const mainLink = link.filter(item=>item.slug===router.query.slug).length?
        link.filter(item=>item.slug===router.query.slug)[0].name['ru']:''

    const [menuActive, setActiveMenu] = useState(false)


    const switchPages = (object:IObject[])=>{
        switch(router.query.slug){
            case 'order-history':
                return <Order object={object}/>
            case 'credit-cards':
                return <PaymentMethod object={object}/>
            case 'address':
                return <Addresses
                    object={object}
                    regions={regions}
                    district={district}
                    apiErrors={apiErrors}
                    setApiErrors={setApiErrors}
                    setIsSuccess={setIsSuccess}
                    alertSuccess={alertSuccess}
                    setErrors={setErrors}
                />
            case 'personal-details':
                return <PersonalDetails object={object}/>
            case 'your-products':
                return <UserProducts object={object}/>
            default:
                return ''
        }
    }

    return(
        <MainComponent>
            <Container fluid>
                <div className={`${classes['bread-crumbs']}`}>
                    <p className='mb-0'>
                        <Link href='/' locale={locale}>
                            <a>Farq</a>
                        </Link>
                    </p>
                    <p className='mb-0'>
                        <Link href='/account' locale={locale}>
                            <a>{t('account.your-account')}</a>
                        </Link>
                    </p>
                    <p className='mb-0'>{mainLink}</p>
                </div>
                <button onClick={()=>setActiveMenu(true)} className={`${classes['mobile-menu-open']} d-lg-none d-flex align-items-center `}>
                    <List />
                    {t('account.your-account')}
                </button>
                <div className={`${classes['wrap-container-account']} row`}>
                    <SideMenu setActiveMenu={setActiveMenu} menuActive={menuActive}/>
                    {switchPages(link)}
                </div>
            </Container>
        </MainComponent>
    )
}


function SideMenu({menuActive, setActiveMenu}:{menuActive:boolean, setActiveMenu:(va:boolean)=>void}){
    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router

    const HTMLIcons = (icon)=>{
        switch(icon){
            case 'TimeClock':
                return <TimeClock />
            case 'Card':
                return <Card />
            case 'AddressBook':
                return <AddressBook />
            case 'PersonCircle':
                return <PersonCircle />
            default:
                return ''
        }
    }

    return(
        <div className={`col-lg-3 col-12 ${classes['menu-side-bar']} 
        ${menuActive?classes['active']:''}`} onClick={()=>setActiveMenu(false)}>
            <ul className={`${classes['side-nav-links']}`}>
                <li>
                    <Link href='/account' locale={locale}>
                        <a>
                            <span><ChevronLeft /></span>
                            <span>{t('Твой акаунт')}</span>
                        </a>
                    </Link>
                </li>
                {
                    link.map(item=>{
                        return(
                            <li key={item.id} className={`${item.slug===router.query.slug?`${classes['active']}`:''}`}>
                                <Link href={`/account/${item.slug}`} locale={locale}>
                                    <a>
                                        <span>{HTMLIcons(item.icon)}</span>
                                        <span>{item.name['ru']}</span>
                                    </a>
                                </Link>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

const mapStateToProps = state=>({
    regions: state.regions,
    district: state.district
})


export default connect (mapStateToProps)(AccountRoutes)
