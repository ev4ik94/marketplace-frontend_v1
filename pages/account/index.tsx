import Link from "next/link"
import {useEffect, useState} from "react";
import {useRouter} from "next/router"
import {useTranslation} from "react-i18next"

/*---Components----*/
import MainComponent from "../../components/Main-Component"
import {CardLinkGroup} from '../../components/pages-component/account/card-link-group'

/*---Bootstrap----*/
import {Container} from "react-bootstrap"
import classes from "../../styles/pages-components/account/account.module.sass"



/*---Hooks---*/
import {useAuth} from "../../hooks/authentication.hook"

/*---Interface---*/
interface IUser{
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    password: string,
    address: string,
    phone: string
}

function Account(){

    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    const {checkAuth, getUserData, logout} = useAuth()
    const [userData, setUserData] = useState<IUser | null>(null)



    useEffect(()=>{
        if(!checkAuth()){
            router.push('/', undefined ,{locale})
        }else{
            setUserData(getUserData())
        }

    }, [])


    return (
        <MainComponent>
            <>
                <Container fluid className={`${classes['wrap-top-content']}`}>
                    <Container fluid>
                        <div className={`${classes['bread-crumbs']}`}>
                            <p className='mb-0'>
                                <Link href='/' locale={locale}>
                                    <a>Farq</a>
                                </Link>
                            </p>
                            <p className='mb-0'>{t('account.your-account')}</p>
                        </div>
                        <div className={`${classes['hello-block']} d-flex align-items-center justify-content-between`}>
                            <div>
                                <p className="mb-0">Hi, {userData!==null && userData.first_name?userData.first_name:
                                    (userData!==null&&userData.email?userData.email:'')}</p>
                                <p className="mb-0">{t('account.main')}</p>
                            </div>
                            <div className='col-2 pr-0 d-lg-block d-none'>
                                <button onClick={logout} className={`${classes['sign-out-button']}`}>{t('account.sign-out')}</button>
                            </div>
                        </div>
                    </Container>
                </Container>
                <CardLinkGroup logout={logout}/>

            </>
        </MainComponent>
    )
}

export default Account
