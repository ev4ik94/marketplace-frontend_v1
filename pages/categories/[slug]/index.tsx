import {useRouter} from "next/router"
import {useEffect, useState} from 'react'
import Link from 'next/link'


/*---Components---*/
import MainComponent from "../../../components/Main-Component"

/*---Bootstrap---*/
import {Container} from 'react-bootstrap'

/*----Components---*/
import {LazyLoadImage} from "../../../components/preloader/Lazy-Load-Image"

/*----Redux----*/
import {connect} from 'react-redux'


/*----Styles----*/
import classes from '../../../styles/pages-components/categories/categories-list.module.sass'



/*---Interfaces----*/

interface ICategories {
    slug: string,
    recursive_children: ICategories[],
    description: string,
    icon_url: {},
    images: IImages,
    name: string,
    parent_id: number
}

interface IImages {
    original_image: string,
    image_srcset: string
}

function CategoriesPage({categories}: {categories:ICategories[]}){
    const router = useRouter()
    const {locale} = router
    const {slug} = router.query
    const [categoriesArr, setCategories] = useState<ICategories[]>([])
    const [title, setTitle] = useState('')



    useEffect(()=>{
       nestedCat(categories)
    }, [categories])

    const nestedCat = (arr)=>{
        if(arr && arr!==null){
            for(let i=0; i<arr.length; i++){
                if(arr[i].slug===slug){
                    setTitle(arr[i].name)
                    setCategories(arr[i].recursive_children?arr[i].recursive_children:[])
                }
                nestedCat(arr[i].recursive_children)
            }
        }
    }


    return(
        <MainComponent>

            <>
                <Container className={`pt-3`}>
                    <h1 className='font-weight-bold'>{title}</h1>
                </Container>

                <Container>
                    <div className={`d-flex ${classes['wrap-categories-list']} flex-wrap`}>
                        {
                            (categoriesArr || []).map((item,index)=>{

                                return(
                                    <div key={item.slug} className={`col-lg-4`}>
                                        <div className={`${classes['picture-category']}`}>
                                            <LazyLoadImage image={{
                                                src: item.images[0]?item.images[0].original_image:'',
                                                srcSet: item.images[0]?item.images[0].image_srcset:'',
                                                alt: ''
                                            }} />

                                        </div>
                                        <div className={`mt-3`}>
                                            <Link href={`${item.recursive_children.length? `/categories/${item.slug}`:`/products/${item.slug}`}`} locale={locale}>
                                                <a>{item.name}</a>
                                            </Link>
                                            <p>{item.description}</p>
                                        </div>
                                        <div>
                                            {
                                                item.recursive_children.map(child=>{

                                                    return(
                                                        <p key={child.slug}>
                                                            <Link href={`/products/${child.slug}`} locale={locale}>
                                                                <a>{child.name}</a>
                                                            </Link>
                                                        </p>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </Container>
            </>
        </MainComponent>
    )
}


const mapStateToProps = state=>({
    categories: state.categories
})


export default connect (mapStateToProps)(CategoriesPage)
