import {useRouter} from "next/router"
import {useEffect, useState} from 'react'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'



/*---Components---*/
import MainComponent from "../../../../components/Main-Component"
import {TopProductMain} from "../../../../components/pages-component/product-view/top-product-main"
import {Accordion} from '../../../../components/pages-component/product-view/acordion'
import {ModalViewImage} from "../../../../components/pages-component/product-view/modal-view-image"
import {AddToCartModal} from '../../../../components/alerts/add-to-cart'
import {ProductViewPreloader} from "../../../../components/skeleton/Product-View"

/*---Styles----*/
import classes from '../../../../styles/pages-components/product/product-view.module.sass'

/*---Bootstrap---*/
import {Container} from 'react-bootstrap'


/*----Hooks----*/
import {AxiosApi} from "../../../../hooks/axios.hook"


/*-------Functions-------*/
import {isSave} from "../../../../components/secondary-func"


/*----Redux----*/
import {connect} from 'react-redux'
import {setProduct} from "../../../../redux/actions/actionProduct"
import {setCart} from '../../../../redux/actions/actionCart'
import {setSaved} from "../../../../redux/actions/actionSaved"
import {setCompare} from "../../../../redux/actions/actioncompare"


/*---Interfaces---*/

interface ICategories {
    id: number,
    children: ICategories[],
    description: string,
    icon_url: {},
    image_urls: IImages,
    name: string,
    parent_id: number
}

interface IImages {
    md: string,
    org: string,
    sm: string,
    xs: string
}


interface IProduct{
    id: number,
    brand: {
        name: string,
        slug: string
    },
    category: {
        slug: string
    },
    code: string,
    attribute_groups: [],
    name: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    price: {
        old_price: number,
        price: number
    },
    shop:{
        name: string,
        slug: string
    },
    warehouse: {
        name: string,
        slug: string
    },
    slug: string,
    skus: {
        id: number,
        sku_slug: string,
        warehouse_slug: string
    }[],
    variations: IVariations[],
    breadcrums: IBreadCrumbs[]
}

interface IVariations{
    name: string,
    children: IChild[],
}

interface IChild{
    id: number,
    name: string,
    sku_ids: {

    }[],
    value: string
}

interface IBreadCrumbs{
    category_slug: string,
    name: string,
    category_id: number
}




function ProductView({
                         setProduct,
                         product,
                         setCart,
                         setSave,
                         cart,
                         compare,
                         setCompare,
                         save
                    }:{
                        setProduct: (product:{})=>void,
                        product: IProduct | null,
                        setCart:(value:any)=>void,
                        setSave: (value: any)=>void,
                        setCompare: (value: any)=>void,
                        compare,
                        cart,
                        save
                    }){

    const router = useRouter()
    const {locale} = router
    const {sku_slug, product_slug} = router.query
    const [productObj, setProductObj] = useState<IProduct>(null)
    const [show, setShow] = useState(false)
    const [addCart, setCartObj] = useState(null)
    const [showCartModal, setCartShowCart] = useState(false)

    const [isAddedCart, setAddedCart] = useState(false)
    const [isAddedSave, setAddSave] = useState(false)
    const [isAddedCompare, setAddCompare] = useState(false)

    const [mount, setMount] = useState(true)

    const {request} = AxiosApi()

    const [loadingCart, setLoadingCart] = useState(false)
    const [loadingSave, setLoadingSave] = useState(false)
    const [loadingCompare, setLoadingCompare] = useState(false)
    const [loadingProduct, setLoadingProduct] = useState(false)



    const {t} = useTranslation()


    useEffect(()=>{

        if(mount){
            if(JSON.stringify(product)==='{}'){
                getProduct()
            }else{
                if(product_slug!==product.slug){
                    getProduct()
                }else{
                    setProductObj(product)
                }
            }
            setMount(false)
        }

    }, [mount])

    useEffect(()=>{
        isSave(product_slug,sku_slug,save)
    }, [save])

    useEffect(()=>{
        isCart((cart.items||[]), product_slug, sku_slug)
    }, [cart])

    useEffect(()=>{
        isCompare(product_slug, sku_slug)
    }, [compare])


    /*--------- Requests API-------------*/

    /*--------Get Product------------*/
    const getProduct = async()=>{
        setLoadingProduct(true)
             await request(`${process.env.API_PRODUCT_VIEW[locale]}/${product_slug}/${sku_slug}`)
            .then(result=>{

                setProductObj(result.data)
                setProduct(result)
                setLoadingProduct(false)
            })

    }

    /*-------- Adding to Cart------------*/
    const addToCart = async(warehouse:string, slug:string)=>{
        if(!isAddedCart){
            setLoadingCart(true)

            await request(`${process.env.ADD_TO_CART[locale]}/${warehouse}/${slug}`, 'PUT')
                .then(result=>{

                    let data = result.data.items.filter(item=>item.sku.slug===slug).length?
                        result.data.items.filter(item=>item.sku.slug===slug)[0]:{}

                    const dataO = {product:data, cart:{
                            totalCount: result.data.total_quantity,
                            totalPrice: result.data.total_price
                        }}

                    setCart(result.data)
                    setCartObj(dataO)
                    setLoadingCart(false)

                    ShowHandle()

                }).catch(e=>{
                    setLoadingCart(false)
                    alert(e.response)
                })

        }

    }

    /*----------Add to Save------------*/
    const addSave = async(warehouse_slug:string, slug:string)=>{
        setLoadingSave(true)
        await request(`${process.env.ADD_SAVED[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                setAddSave(true)
                setSave([...save, setProductObj])
                setLoadingSave(false)
            }).catch(e=>{
                setLoadingSave(false)
            })
    }

    /*----------Delete Save------------*/
    const deleteSave = async(warehouse_slug:string, slug:string)=>{
        setLoadingSave(true)
        await request(`${process.env.REMOVE_SAVED[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                setAddSave(false)
                setSave(save.filter(item=>item.warehouse.slug!==warehouse_slug && item.slug!==slug))
                setLoadingSave(false)
            }).catch(e=>{
                setLoadingSave(false)
            })
    }


    /*-------Add To Compare--------------*/
    const addToCompare = async(warehouse_slug:string, slug:string, category_slug:string)=>{

        if(compare.category_slug===null || compare.category_slug === category_slug){
            setLoadingCompare(true)

            await request(`${process.env.COMPARES_ADD[locale]}/${warehouse_slug}/${slug}`, 'PUT')
                .then(result=>{

                    setLoadingCompare(false)
                    setAddCompare(true)
                    setCompare({
                        items: [...compare.items?compare.items:[], productObj],
                        attribute_groups: compare.attribute_groups,
                        category_slug: compare.category_slug
                    })
                })
        }else{
            const quest = confirm(t('product-list.err-compare-add'))
            if(quest){
                await request(`${process.env.COMPARES_REMOVE_ALL_ADD[locale]}/${warehouse_slug}/${slug}`, 'PUT')
                    .then(result=>{
                        setLoadingCompare(false)
                        setAddCompare(true)
                        setCompare(result.data)
                    })
            }else{
                return
            }
        }
    }

    /*----------Delete Compare----------------*/

    const deleteCompare = async(warehouse_slug, slug)=>{
        setLoadingCompare(true)
        const dataCompare = compare.items?compare.items:[]
        await request(`${process.env.COMPARES_REMOVE[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                setLoadingCompare(false)
                setAddCompare(false)
                setCompare({
                    items: dataCompare.filter(item=>item.slug!==slug),
                    attribute_groups: dataCompare.length?compare.attribute_groups: [],
                    category_slug: dataCompare.length?compare.category_slug:null
                })
            })
    }



/*--------Function Check isAdded---------*/



    const isCart = (array, warehouse_slug, slug)=>{
        const arr = array.filter(item=>item.sku.warehouse.slug===warehouse_slug && item.sku.slug===slug)
        if(arr.length){
            setAddedCart(true)
        }
    }

    const isCompare = (warehouse_slug, slug)=>{
        const dataCompare = compare.items?compare.items:[]
        const arr = dataCompare.filter(item=>item.warehouse.slug===warehouse_slug && item.slug===slug)
        if(arr.length){
            setAddCompare(true)
        }else{
            setAddCompare(false)
        }
    }


/*----------Handle Functions---------*/
    const handleClose = () => setShow(false)
    const HideHandle = () => setCartShowCart(false)
    const ShowHandle = () => setCartShowCart(true)
    const handleShow = () => setShow(true)

    const imagesFilter = ()=>{
        return productObj!==null ? productObj.images : []
    }


    if(productObj && productObj!==null){

    return(
        <MainComponent>

            <>
                <ModalViewImage images={imagesFilter()} show={show} handleClose={handleClose}/>
                <AddToCartModal cartObj={addCart!==null?addCart:{}} show={showCartModal} onHide={HideHandle}
                                catId={productObj!==null&&productObj.breadcrums&&productObj.breadcrums.length?productObj.breadcrums[productObj.breadcrums.length-1].category_id:null}/>
                <Container fluid>

                    <div className={`d-flex ${classes['bread-crumbs']} flex-wrap`}>
                        <p className='mb-0'>
                            <Link href={'/'} locale={locale}>
                                <a href="">Farq</a>
                            </Link>
                        </p>
                        {
                            (productObj!==null && productObj.breadcrums?productObj.breadcrums:[]).map((item, index, array)=>{

                                return(
                                    <p key={index} className='mb-0'>
                                        <Link href={`/products/${item.category_slug}`} locale={locale}>
                                            <a>{item.name}</a>
                                        </Link>
                                    </p>
                                )
                            })
                        }
                        <p className='mb-0'>
                            {productObj.name}
                        </p>

                    </div>

                    <TopProductMain
                        productInfo = {productObj?productObj:null}
                        handleShow = {handleShow}
                        addToCart = {addToCart}
                        addSave = {addSave}
                        loading = {loadingCart}
                        isAddedCart = {isAddedCart}
                        isAddedSave = {isAddedSave}
                        isAddedCompare = {isAddedCompare}
                        addToCompare = {addToCompare}
                        deleteCompare = {deleteCompare}
                        loadingSave = {loadingSave}
                        loadingCompare = {loadingCompare}
                        loadingProduct = {loadingProduct}
                        deleteSave = {deleteSave}
                    />
                    <div className={`${classes['description-block']} mx-3 mt-5`}>
                        <h2>{t('product-view.overview')}</h2>
                    </div>
                    <Accordion characters={productObj!==null && productObj.attribute_groups?productObj.attribute_groups:[]}/>
                </Container>
            </>

        </MainComponent>
    )
    }

    return (
        <MainComponent>
            <ProductViewPreloader />
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    categories: state.categories,
    product: state.product,
    cart: state.cart,
    save: state.saved,
    compare: state.compare
})

const mapDispatchToPRops = {
    setProduct,
    setCart,
    setSaved,
    setCompare

}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(ProductView)


