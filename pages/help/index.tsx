import {useRouter} from "next/router"
import Link from 'next/link'
import {useEffect, useState} from 'react'
import {useTranslation} from "react-i18next"

/*---Components----*/
import MainComponent from "../../components/Main-Component"
import {FeedBackHelp} from "../../components/pages-component/help/feedBack-help"

/*-----Bootstrap-----*/
import {Container} from 'react-bootstrap'

/*----Icons----*/
import {TimeClock} from "../../components/icons/time-oclock"


/*----Styles----*/
import classes from '../../styles/pages-components/help/help-page.module.sass'

/*-----Redux----*/
import {setCategoriesFaq} from "../../redux/actions/actionCategoriesFaq"
import {connect} from 'react-redux'

/*---VirtDb---*/
import {help} from '../../virtDb/help'

function Help({setCategoriesFaq, categoriesFaq}){
    const router = useRouter()
    const {locale} = router
    const [mountCategoriesFaq, setMountCategoriesFaq] = useState(true)
    const {t} = useTranslation()

    useEffect(()=>{
        if(mountCategoriesFaq && !categoriesFaq.length){
            getCategoriesFaq()
        }
    }, [mountCategoriesFaq])

    const getCategoriesFaq = async()=>{
        new Promise(resolve=>{
            setTimeout(()=>{
                resolve(help)
            }, 500)
        }).then(result=>{
            setCategoriesFaq(result)
            setMountCategoriesFaq(false)
        })
    }


    return(
        <MainComponent>
            <>

                <Container fluid>
                    <div className={`${classes['bread-crumbs']} d-flex mt-3`}>
                        <Link href={'/'} locale={locale}>
                            <a>Home</a>
                        </Link>
                        <p>{'>'}</p>
                        <Link href={'/home'} locale={locale}>
                            <a className={classes['active']}>Help</a>
                        </Link>
                    </div>
                    <h4>{t('home.title-home')}</h4>
                </Container>


                <div className={`${classes['block-categories-list']} mt-3 pt-3 pb-3`}>
                    <Container fluid>
                        <h1 className='font-weight-bold'>{t('home.title-category-list')}</h1>
                        <div className={`${classes['categories-list']} d-flex mt-3 flex-wrap`}>

                            {
                                (help || []).map((item, index)=>{
                                    return(
                                        <div className={`${classes['item-category-help']} col-lg-3 mr-3 mb-3`} key={index}>
                                            <div className='mt-3'><TimeClock /></div>
                                            <p className='font-weight-bold mt-3'>{item.title}</p>
                                            <div className={`${classes['bot-line']} mt-3`} />

                                            <div className={`${classes['list-child-links']} mt-3`}>
                                                {
                                                    item.children.map((child, index)=>{
                                                        return(
                                                            <p key={index} className='mb-1'>
                                                                <Link href={`/help/${child.slug}`} locale={locale}>
                                                                    <a>{child.title}</a>
                                                                </Link>
                                                            </p>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Container>
                </div>
                <Container fluid className='mt-3'>
                    <FeedBackHelp />
                </Container>
            </>
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    categoriesFaq: state.categoriesFaq
})

const mapDispatchToPRops = {
    setCategoriesFaq
}

export default connect (mapStateToProps, mapDispatchToPRops)(Help)
