import {useEffect, useState} from "react"

/*---Components----*/
import MainComponent from "../../../components/Main-Component"
import {ArticleContent} from "../../../components/pages-component/help/articleContent"
import {SideBarCategories} from "../../../components/pages-component/help/side-bar-categories"

/*----Bootstrap------*/
import {Container} from 'react-bootstrap'

/*---VirtDb---*/
import {help} from '../../../virtDb/help'

/*-----Redux----*/
import {setCategoriesFaq} from "../../../redux/actions/actionCategoriesFaq"
import {connect} from 'react-redux'


function SupportArticles({categoriesFaq, setCategoriesFaq}){
    const [mountCategoriesFaq, setMountCategoriesFaq] = useState(true)

    useEffect(()=>{
        if(mountCategoriesFaq && !categoriesFaq.length){
            getCategoriesFaq()
        }
    }, [mountCategoriesFaq])

    const getCategoriesFaq = async()=>{
        new Promise(resolve=>{
            setTimeout(()=>{
                resolve(help)
            }, 500)
        }).then(result=>{
            setCategoriesFaq(result)
            setMountCategoriesFaq(false)
        })
    }

    return(
        <MainComponent>
            <Container fluid>
                <div className='d-flex'>
                    <SideBarCategories categories={categoriesFaq}/>
                    <ArticleContent />
                </div>
            </Container>
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    categoriesFaq: state.categoriesFaq
})

const mapDispatchToPRops = {
    setCategoriesFaq
}

export default connect (mapStateToProps, mapDispatchToPRops)(SupportArticles)
