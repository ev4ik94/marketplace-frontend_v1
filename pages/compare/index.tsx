import {useTranslation} from "react-i18next"
import {useEffect, useState, useRef} from 'react'
import {useRouter} from "next/router"
import {connect} from 'react-redux'



/*---Components---*/
import MainComponent from "../../components/Main-Component"
import {CompareListProducts} from "../../components/pages-component/compare/compare-list-products"
import {ModalViewImage} from "../../components/pages-component/product-view/modal-view-image"
import {CompareCharactersTable} from "../../components/pages-component/compare/compare-characters-table"
import {CompareListPreloader} from "../../components/skeleton/Compare-list-preloader"
import {CompareReviewRating} from "../../components/pages-component/compare/compare-review-rating"


/*----Bootstrap----*/
import {Container} from 'react-bootstrap'


/*---Icons---*/
import {ChevronLeft} from "react-bootstrap-icons"

/*---Styles---*/
import classes from '../../styles/pages-components/compare/compare-list-products.module.sass'
import {setCompare} from "../../redux/actions/actioncompare"

/*----Hooks-----*/
import {AxiosApi} from "../../hooks/axios.hook"


/*------Redux-----*/
import {setSaved} from "../../redux/actions/actionSaved"


/*----Interfaces---*/

interface IComparesBlock{
    attribute_groups: [],
    items: ICompares[],
    category_slug: string|null

}

interface ICompares{
    id: number,
    brand: {},
    category?: {},
    attribute_groups: [],
    code: string,
    images: IImages[],
    name: string,
    sku_id: number,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface IImages{
    image_srcset: string,
    original_image: string,
}



function Compare({compare, setCompare, setSaved ,saved}: {
    saved: ICompares[],
    setSaved: (val:any)=>void,
    compare:IComparesBlock,
    setCompare:(compare: IComparesBlock)=>void}){

    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    const [compares, setCompares] = useState<ICompares[]>([])
    const [recentlyMount, setMountRecently] = useState(true)
    const [packages, setPackages] = useState([])
    const [sortSkus, setSortSkus] = useState([])
    const [show, setShow] = useState(false)
    const [currentImageId, setCurrentImageId] = useState(null)
    // const [titleCharacter, setTitleCharacter] = useState('')
    // const [compareSticky, setCompareSticky] = useState(false)



    let arrRef = []
    const slide1 = useRef(null)
    const slideSlick = useRef(null)



    /*------Carousel Settings-----------*/

    const settings_1 = {
        dots: false,
        infinite: false,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        touchMove: false,
        onSwipe: (e)=>{
            if(e=='right'){
                prevSlide()
            }else{
                nextSlide()
            }
        },
        autoplay: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    touchMove:true
                }
            }
        ]
    }

    const settings_2 = {
        dots: false,
        infinite: false,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        onSwipe: (e)=>{
            if(e=='right'){
                prevSlide()
            }else{
                nextSlide()
            }
        },
        autoplay: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            }
        ]
    }


    const {request, loading} = AxiosApi()


    const handleClose = ()=>{
        setShow(false)
    }

    useEffect(()=>{
        if(recentlyMount){
            getRecently()
        }
    }, [recentlyMount])

    useEffect(()=>{
        setCompares(compare.items?compare.items:[])
        setSortSkus(compare.items?compare.items.map(item=>item.sku_id):[])
        setPackages(compare.attribute_groups?compare.attribute_groups:[])
    }, [compare])


    /*------- Window Scroll Sticky--------------*/
    useEffect(()=>{
        if(typeof window !== 'undefined'){

            var scrollPos = 0

            window.onscroll  = (e)=>{

                const el = document.getElementById('compare-list-block')

                if(el!==null){
                    if(document.body.getBoundingClientRect().top>scrollPos){
                        if(el.getBoundingClientRect().top>0){
                            if(el.classList.contains(classes['compares-products-stick'])){
                                el.classList.remove(classes['compares-products-stick'])
                                el.classList.add(classes['compare-list-carousel'])
                            }
                        }
                    }else{
                        if(el.getBoundingClientRect().top<=0){
                            if(el.classList.contains(classes['compare-list-carousel'])){
                                el.classList.remove(classes['compare-list-carousel'])
                                el.classList.add(classes['compares-products-stick'])
                            }
                        }
                    }

                    scrollPos = document.body.getBoundingClientRect().top
                }

            }
        }
    }, [])


    const getRecently = async()=>{
        await request(process.env.COMPARES_SUGGESTIONS[locale])
            .then(result=>{
                setMountRecently(false)
            })
    }


    const imagesFilter = (id=null):IImages[]=>{
        const arrayImages = compares.length? compares.filter(item=>item.sku_id===id):[]
        return arrayImages.length?arrayImages[0].images:[]
    }



    /*-------Carousel Events---------*/

    const nextSlide = ()=>{
        slide1.current.slickNext()
        arrRef.forEach(item=>{
            if(item!==null){
                item.slickNext()
            }
        })
    }

    const prevSlide = ()=>{
        slide1.current.slickPrev()
        arrRef.forEach(item=>{
            if(item!==null){
                item.slickPrev()
            }
        })
    }

    if(loading){
        return(
            <MainComponent>
                <CompareListPreloader />
            </MainComponent>
        )
    }




    return(
        <MainComponent>
            <Container fluid>
                <ModalViewImage images={imagesFilter(currentImageId)} show={show} handleClose={handleClose}/>
                <button onClick={()=>router.back()} className={classes['button-back']}>
                    <ChevronLeft />
                    {t('compare-page.button-back')}
                </button>
                <h1 className={`font-weight-bold ${classes['title-compare-products']}`}>{t('compare-page.title')}</h1>
                <CompareListProducts
                    compares={compare}
                    setSaved={setSaved}
                    saved={saved}
                    openModal = {(id, state)=>{
                        setShow(state)
                        setCurrentImageId(id)
                    }}
                    setCompare={setCompare}
                    slide={slide1}
                    slide2={slideSlick}
                    settings={settings_2}
                    nextSlide={nextSlide}
                    prevSlide={prevSlide}
                />

                <CompareCharactersTable
                    compare={compares}
                    packages={packages}
                    sortSkus={sortSkus}
                    arrRef={arrRef}
                    settings={settings_1}
                />
                {/*<CompareReviewRating />*/}
            </Container>

        </MainComponent>
    )
}


const mapStateToProps = state=>({
    compare: state.compare,
    saved: state.saved
})

const mapDispatchToPRops = {
    setCompare,
    setSaved

}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(Compare)
