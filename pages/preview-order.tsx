import {useState, useEffect} from 'react'
import {useTranslation} from "react-i18next"
import Link from 'next/link'
import {useRouter} from "next/router"

var CryptoJS = require("crypto-js");


/*---Styles----*/
import classes from '../styles/pages-components/preview-order/preview-order.module.sass'

/*---Components---*/
import MainComponent from "../components/Main-Component"
import {CostReplace} from "../components/secondary-func"
import {Tracker} from "../components/pages-component/checkout/tracker"
import {OrderForm} from "../components/pages-component/checkout/order-form"
import {LazyLoadImage} from "../components/preloader/Lazy-Load-Image"
import { decryptString} from '../components/secondary-func'


/*---Bootstrap----*/
import {Container} from "react-bootstrap"


/*------Hooks----*/
import { AxiosApi } from '../hooks/axios.hook'

/*----Redux----*/
import {connect} from 'react-redux'
import {setCart} from "../redux/actions/actionCart"



interface ICart{
    total_price: number,
    quantity: number,
    items: ISkus[]
}

interface ISkus {
    id: number,
    brand: {},
    category?: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface IRegion{
    id: number,
    name: string
}

interface IDistrict{
    id: number,
    name: string
}


function PreviewOrder({regions, district, setCart}:{regions: IRegion[], district: IDistrict[], setCart:(val:any)=>void}){

    const {t} = useTranslation()
    const [products, setProducts] = useState([])
    const [dataForm, setDataForm] = useState(null)
    const router = useRouter()
    const {locale} = router

    /*-----Data Values------*/
    const [region, setRegion] = useState('')
    const [districtVal, setDistrict] = useState('')
    const [country, setCountry] = useState('Uzbekistan')

    const {request, error, loading} = AxiosApi()


    useEffect(()=>{
        if(typeof window!=='undefined'){
            const data = window.localStorage.getItem('place_order')

            if(data){
                const dataObj = decryptString(data)
                setDataForm(dataObj)
              
                setProducts(dataObj.product_skus?dataObj.product_skus:[])
                const reg = regions.filter(region=>region.id===dataObj.region_id).length?
                    regions.filter(region=>region.id===dataObj.region_id)[0].name:''
                const dis = district.filter(item=>item.id===dataObj.district_id).length?
                    district.filter(item=>item.id===dataObj.district_id)[0].name:''
                setRegion(reg)
                setDistrict(dis)
            }else{
                router.push('/')
            }
        }

    }, [])

    const sendOrderForm = async()=>{
        await request(`${process.env.ORDER_SEND[locale]}`, 'POST', dataForm)
            .then(result=>{
                if(result.data){
                    
                    setCart([])
                    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(result.data), 'secret_key_farq-uJ8CKmiX').toString();
                    window.localStorage.setItem('order_track', ciphertext)
                    window.localStorage.removeItem('place_order')
                    router.push('/invoice', undefined, {locale})
                }
            }).catch(e=>console.log(error))


    }


    return(
        <MainComponent>

            <Container fluid className='mt-3'>
                <Tracker />

                <div className={`${classes['preview-order-wrap']} d-flex flex-wrap`}>
                    <div className='col-lg-9'>
                        <div className={`d-flex align-items-center justify-content-between mt-3`}>
                            <h3>{t('preview.title-address-shipping')}</h3>
                        </div>
                        <div className={`${classes['block-address-preview']} mt-3`}>
                            {
                                dataForm && dataForm!==null?(<>
                                    <p className='font-weight-bold'>{`${dataForm.first_name?dataForm.first_name:''} 
                                ${dataForm.last_name?dataForm.last_name:''}`} - {dataForm.address?dataForm.address:''}</p>
                                    <p>{country}, {region}, {districtVal}</p>
                                    <p>{dataForm.address?dataForm.address:''}</p>
                                </>):('')
                            }
                        </div>
                        <div className='mt-3'>
                            <div className='d-flex align-items-center justify-content-between'>
                                <h3>{t('preview.title-shipping-details')}</h3>
                            </div>
                            <div className={`${classes['product-list-order']} mt-3`}>
                                {
                                    products.map((item, index)=>{
                                        return(
                                            <div className='d-flex' key={index}>
                                                <div className={`${classes['image-product-order']} col-lg-3 col-md-3 col-6 pl-0`}>
                                                    <Link href={`/product/${item.sku.warehouse.slug}/${item.sku.slug}`}>
                                                        <a>
                                                            <LazyLoadImage image={{
                                                                src: item.sku.images[0].original_image,
                                                                srcSet: item.sku.images[0].image_srcset,
                                                                alt: item.sku.name
                                                            }}/>
                                                        </a>
                                                    </Link>
                                                </div>
                                               <div className='col-lg-9 col-md-9 col-6 d-flex flex-wrap pr-0'>
                                                   <div className={`${classes['name-cost-order']} col-lg-7`}>
                                                       <Link href={`/product/${item.sku.warehouse.slug}/${item.sku.slug}`}>
                                                           <a>{item.sku.name}</a>
                                                       </Link>
                                                       <p>{t('preview.text3')}: <span>{item.sku.quantity} шт</span></p>
                                                   </div>
                                                   <div className='col-lg-2'>
                                                       <p className='font-weight-bold'>{CostReplace(item.sku.price.price+'')} UZS</p>
                                                   </div>
                                               </div>
                                            </div>
                                        )
                                    })
                                }

                            </div>

                            <div className={`mt-3`}>
                                <div className='d-flex align-items-center justify-content-between'>
                                    <h4>{t('preview.title-payment-method')}</h4>
                                </div>
                                <div className={`${classes['payment-method']}`}>
                                    <p>Оплата наличными</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='col-lg-3 pt-3'>
                        <div className={`${classes['wrap-form']}`}>
                            <h3 className={`font-weight-bold`}>{t('cart-page.form-application1')}</h3>
                            <div className={classes['summery-order-cost']}>
                                <div className={`d-flex justify-content-between`}>
                                    <p className={`mb-0`}>{t('cart-page.form-application2')}</p>
                                    <p className={`mb-0`}>{CostReplace(dataForm!==null?dataForm.total_price:0+'')} UZS</p>
                                </div>
                                <div className={`d-flex justify-content-between`}>
                                    <p className={`mb-0`}>{t('cart-page.form-application3')}</p>
                                    <p className={`mb-0`}>{'0'} UZS</p>
                                </div>
                                <div className={`d-flex justify-content-between`}>
                                    <p className={`mb-0`}>{t('cart-page.form-application4')}</p>
                                    <p className={`mb-0`}>{'Free'}</p>
                                </div>
                            </div>

                            <div className={classes['total-cost']}>
                                <div className={`d-flex justify-content-between`}>
                                    <p className={`mb-0`}>{t('cart-page.form-application5')}</p>
                                    <p className={`mb-0`}>{CostReplace(dataForm!==null?dataForm.total_price:0+'')} UZS</p>
                                </div>
                            </div>

                            <button className={classes['continue-checkout']} type='submit' disabled={loading} onClick={(e)=>{
                                e.preventDefault()
                                sendOrderForm()
                            }}>{`${t('preview.finish-order-button')}`}</button>
                        </div>
                    </div>
                </div>


            </Container>
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    regions: state.regions,
    district: state.district
})

const mapDispatchToPRops = {
    setCart
}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(PreviewOrder)
