import {useTranslation} from "react-i18next"
import {useEffect, useState} from 'react'
import {useRouter} from "next/router"

/*---Components----*/
import MainComponent from "../../components/Main-Component"
import {ProductsList} from "../../components/pages-component/recently-page/products-list-recently"
import {AddToCartModal} from "../../components/alerts/add-to-cart"
import {RecentlyPreloader} from "../../components/skeleton/Recently-preloader"


/*----Bootstrap-----*/
import {Container} from "react-bootstrap"

/*------Icons-------*/
import {Save} from '../../components/icons/Save'
import {TimeClock} from "../../components/icons/time-oclock"

/*---Styles---*/
import classes from '../../styles/pages-components/recently-viewed/recently-list.module.sass'

/*----Redux-----*/
import {connect} from 'react-redux'
import {setRecently} from "../../redux/actions/actionRecently"
import {setSaved} from "../../redux/actions/actionSaved"
import {setCart} from "../../redux/actions/actionCart"
import {setCompare} from "../../redux/actions/actioncompare"


/*---Hooks---*/
import {AxiosApi} from "../../hooks/axios.hook"


/*---Interface---*/
interface ICart{
    items: [],
    total_price: number,
    quantity: number
}

interface IRecently{
    brand: {
        name: string,
        slug: string
    },
    name: string,
    category: {
        name: string,
        slug: string
    },
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    price: {
        old_price: number,
        price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}


function RecentlyViewed({
                            recently,
                            saved,
                            loadingState,
                            setRecently,
                            setSaved,
                            cart,
                            setCart,
                            setCompare,
                            compare
                        }:
                            {
                                recently: IRecently[],
                                saved: IRecently[],
                                loadingState: boolean,
                                cart: ICart,
                                setRecently: (value: IRecently[], lang?: string)=>void,
                                setSaved: (value: any[], lang?: string)=>void,
                                setCart: (value: [])=>void,
                                setCompare: (value: [])=>void,
                                compare: {items: []}
                            }){

    const {t} = useTranslation()
    const [recentlyToggle, setRecentlyToggle] = useState(true)
    const [ProductCart, setProductCart] = useState(null)

    const {request} = AxiosApi()
    const router = useRouter()
    const {locale} = router


    /*------Request functions START-------*/


    /*-------Controller DELETE Recently-Saved Products ------*/

    async function deleteProduct(type='', item, warehouse_slug, slug){
        preloaderShow(item, 'show')
        if(type==='save_delete'){
            deleteSave(item, warehouse_slug, slug)
        }else{
            if(recentlyToggle){
                deleteRecently(item, warehouse_slug, slug)
            }else{
                deleteSave(item, warehouse_slug, slug)
            }
        }
    }

    /*------Request Add to Cart -----*/

    const addToCart = async(item, warehouse_slug, slug)=>{
        preloaderShow(item, 'show')
        await request(`${process.env.ADD_TO_CART[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{

                preloaderShow(item, 'hide')
                setCart(result.data)
                const productCart = result.data.items.filter(item=>item.sku.warehouse.slug===warehouse_slug && item.sku.slug===slug)[0]
                setProductCart({
                    product: productCart?productCart:null,
                    cart: {
                        totalPrice: result.data.total_price,
                        totalCount: result.data.quantity
                    }
                })

            }).catch(e=>e.message)
    }

    /*------Request Add to Compare -----*/

    const addToCompare = async(item, warehouse_slug, slug)=>{
        preloaderShow(item, 'show')
        await request(`${process.env.COMPARES_ADD[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                preloaderShow(item, 'hide')
                setCompare(result.data)
            }).catch(e=>e.message)
    }

    /*------Request Delete Compare -----*/

    const deleteCompare = async(item, warehouse_slug, slug)=>{
        preloaderShow(item, 'show')
        await request(`${process.env.COMPARES_REMOVE[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                preloaderShow(item, 'hide')
                setCompare(result.data)
            }).catch(e=>e.message)
    }

    /*------Request Add to Save -----*/

    const addToSave = async(item, warehouse_slug, slug)=>{
        await request(`${process.env.ADD_SAVED[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                preloaderShow(item, 'hide')
                setSaved(result.data.list)
            }).catch(e=>e.message)
    }

    /*------Request Delete Save -----*/

    const deleteSave = async(item, warehouse_slug, slug)=>{
        await request(`${process.env.REMOVE_SAVED[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                preloaderShow(item, 'hide')
                setSaved(result.data.list)
            }).catch(e=>e.message)
    }

    /*------Request Delete Recently----*/
    const deleteRecently = async(item, warehouse_slug, slug)=>{
        await request(`${process.env.REMOVE_RECENTLY[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(()=>{
                const data = recently.filter(ite=>{
                    if(ite.warehouse.slug===warehouse_slug && ite.slug===slug){
                        return false
                    }else{
                        return ite
                    }
                })
                setRecently(data)
                preloaderShow(item, 'hide')
            }).catch(e=>e.message)


    }

    /*------Request Delete All Recently----*/
    const deleteRecentlyAll = async()=>{
        await request(`${process.env.REMOVE_RECENTLY_ALL[locale]}`, 'DELETE')
            .then(()=>{
                setRecently([])
            }).catch(e=>e.message)


    }

    /*------Request Delete All Saved----*/
    const deleteSavedAll = async()=>{

        await request(`${process.env.REMOVE_ALL_SAVED[locale]}`, 'DELETE')
            .then(()=>{
                setSaved([])
            }).catch(e=>e.message)
    }


    /*------Request Functions END-----*/


    const preloaderShow = (item, event)=>{
        const element = document.querySelector(`[data-item='${item}']`)

        if(event==='show'){
            element.classList.add('d-block')
        }else{
            element.classList.remove('d-block')
            element.classList.add('d-none')
        }
    }

    if((!recently.length &&  !saved.length) && loadingState){
        return (
            <MainComponent>
                <RecentlyPreloader />
            </MainComponent>
        )
    }

    return(
        <MainComponent>
            <Container fluid className='mb-5'>
                <AddToCartModal
                    cartObj={ProductCart}
                    show={ProductCart!==null}
                    onHide={()=>setProductCart(null)}
                />

               <div className={`${classes['nav-toggles-button']}`}>
                    <div className='d-flex mt-3'>
                        <button
                            className={`${recentlyToggle?classes['active']:''} d-flex align-items-center mr-3`}
                            onClick={()=>{
                                setRecentlyToggle(true)
                            }}
                        >
                            <div className='mr-1'><TimeClock /></div>
                            <p className='mb-0'>{t('recently.button-toggle-recently')}</p>
                        </button>

                        <button
                            className={`${!recentlyToggle?classes['active']:''} d-flex align-items-center`}
                            onClick={()=>{
                                setRecentlyToggle(false)
                            }}
                        >
                           <div className='mr-1'> <Save /></div>
                            <p className='mb-0'>{t('recently-page.save-toggle')}</p>
                        </button>
                    </div>
               </div>
                {
                    recentlyToggle ? (
                        <>
                            {
                                recently.length ? (
                                    <button className={classes['button-clear-all']} onClick={()=>deleteRecentlyAll()}>
                                        {t('recently-page.clear-all')}</button>
                                ): ('')
                            }
                        </>
                    ) : (
                        <>
                            {
                                saved.length ? (
                                    <button className={classes['button-clear-all']} onClick={()=>deleteSavedAll()}>
                                    {t('recently-page.clear-all')}</button>
                                ): ('')
                            }
                        </>
                    )
                }
                <div className={`${classes['list-products-recently-saved']} d-flex flex-wrap justify-content-start`}>
                    <ProductsList
                        products={recentlyToggle?recently:saved}
                        save={saved}
                        deleteProduct={deleteProduct}
                        recentlyToggle={recentlyToggle}
                        cart={cart.items?cart.items:[]}
                        setCart={setCart}
                        addToCart={addToCart}
                        addToCompare={addToCompare}
                        deleteCompare={deleteCompare}
                        addToSave={addToSave}
                        compare={compare.items?compare.items:[]}
                    />
                </div>
            </Container>
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    recently: state.recently,
    saved: state.saved,
    cart: state.cart,
    compare: state.compare,
    loadingState: state.loadingState
})


const mapDispatchToPRops = {
    setRecently,
    setSaved,
    setCart,
    setCompare

}

// @ts-ignore
export default connect(mapStateToProps, mapDispatchToPRops)(RecentlyViewed)
