import {useEffect,useState} from "react"

/*----Components----*/
import MainComponent from "../components/Main-Component"
import { decryptString} from '../components/secondary-func'

/*-----Bootstrap components----*/
import {Container} from "react-bootstrap"

/*----Styles-----*/
import classes from '../styles/pages-components/checkout/track-order.module.sass'
import React from "react";
import {Preloader} from "../components/preloader/Preloader";


export default function Invoice(){

    const [dataOrder, setOrder] = useState(null)

    useEffect(()=>{
        if(document!==undefined){
            setOrder(decryptString(localStorage.getItem('order_track')))
        }
    }, [])


    if(dataOrder!==null && dataOrder){
        return(
            <MainComponent>
                <Container>
                    <div className={`${classes['info-order-track']} d-flex`}>
                        <h1>Спасибо за покупку</h1>
                        <p>Ваш номер заказа: {dataOrder.track_id}</p>
                    </div>
                </Container>
            </MainComponent>
        )
    }
    return (
        <MainComponent>
            <Container fluid>
                <Preloader />
            </Container>
        </MainComponent>
    )
}


