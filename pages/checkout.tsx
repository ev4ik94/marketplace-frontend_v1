import {useTranslation} from "react-i18next"
import {useEffect, useState} from 'react'
import * as Yup from "yup";
import {useRouter} from "next/router"
import {Field, Form, Formik} from "formik";

var CryptoJS = require("crypto-js");




/*---Components----*/
import MainComponent from "../components/Main-Component"
import {Tracker} from '../components/pages-component/checkout/tracker'
import {OrderForm} from '../components/pages-component/checkout/order-form'
import {MaskInput} from "../components/pages-component/checkout/masked-input"
import {Preloader} from "../components/preloader/Preloader"
import { deleteCookie, getCookie, setCookie } from "../components/secondary-func";



/*---Styles---*/
import classes from '../styles/pages-components/checkout/main-style.module.sass'


/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


/*----Redux----*/
import {connect} from 'react-redux'
import {setCart} from '../redux/actions/actionCart'

/*----Hooks---*/
import {AxiosApi} from "../hooks/axios.hook"
import {useAuth} from "../hooks/authentication.hook"

/*---interface----*/

interface ICart{
    items: [],
    total_price: number,
    total_quantity: number
}

interface IRegion{
    id: number,
    name: string
}

interface IDistrict{
    id: number,
    name: string
}


function Checkout({cartObj, setCart, regions, district}:{cartObj:ICart, setCart:(cart:ICart)=>void, regions:IRegion[], district: IDistrict[]}){

    const {t} = useTranslation()
    const {request, loading, error} = AxiosApi()
    const {checkAuth, logout} = useAuth()
    const router = useRouter()
    const {locale} = router

    const [formData, setFormData] = useState({
        name: '',
        lastName: '',
        phone: '',
        email: '',
        comment: '',
        address: '',
        city: '',
        country: '',
        delivery_type: 'delivery',
        payment_type: 'cash',
        is_lifting: true,
        date_delivery: null
    })

    const [totalPrice, setTotalPrice] = useState(0)
    const [mountAddress, setMountAddress] = useState(true)
    const [getUserMount, setUserMount] = useState(true)
    const [valuesAddress, setValuesAddress] = useState([])
    const [dataUser, setDataUser] = useState({
        name: '',
        lastName: '',
        email: ''
    })

    /*---Loaders------*/
    const [addressLoader, setAddressLoader] = useState(true)

    const ph = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/



    const ClientSchema = Yup.object().shape({
        name: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        lastName: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        address: Yup.string()
            .required('Required'),
        phone: Yup.string()
            .min(9, 'Phone error').required('Required'),
        email: Yup.string().email('Invalid email')

    })



    useEffect(()=>{

        if(!cartObj || JSON.stringify(cartObj)==='[]'){
           
        }else{
            if(!cartObj.items.length){
                router.push('/', undefined, {locale})
            }
            totalPriceF(cartObj.items)
        }
    }, [cartObj])

    useEffect(()=>{
        if(getUserMount && checkAuth()){
            if(typeof localStorage !=='undefined'){
                if(localStorage.getItem('user')){
                    const user = JSON.parse(localStorage.getItem('user'))
                    setDataUser({...dataUser, name: user.first_name, lastName: user.last_name, email: user.email})
                    setUserMount(false)
                }
            }
        }
    }, [getUserMount])

    useEffect(()=>{
        if(mountAddress && checkAuth()){
            getAddress()
        }
    }, [mountAddress])

    // const getCart = async()=>{

    //     await request(process.env.GET_CART[locale])
    //         .then(result=>{
    //             setCart(result.data)
    //             totalPriceF(result.data.items)
    //         })
    // }

    const totalPriceF = (arr)=>{
        let total_p = 0
        arr.forEach(item=>{
            total_p+= (item.sku.price.price*item.quantity)
        })
        setTotalPrice(total_p)
    }

    const getAddress = async()=>{
        await request(`${process.env.API_GET_ADDRESS_LIST[locale]}`)
            .then(result=>{
                setMountAddress(false)
                setAddressLoader(false)
                setValuesAddress(result.data)
            })
    }



    const dataSubmit = (values)=>{

        const data = {
            first_name: values.name,
            last_name: values.lastName,
            country_id: 1,
            region_id: values.region_id,
            district_id: values.district_id,
            email: values.email!==''?values.email:undefined,
            phone: values.phone,
            address: values.address,
            delivery_type: formData.delivery_type,
            payment_type: formData.payment_type,
            is_lifting: formData.is_lifting,
            delivery_date: formData.date_delivery,
            cart: cartObj.total_quantity?cartObj.total_quantity:0,
            product_skus: cartObj.items?cartObj.items:[],
            total_price: cartObj.total_price?cartObj.total_price:0,
            shipping_price: 0
        }


        return data
    }

    const saveOrderInfoCokkie = (data)=>{
      
        var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), 'secret_key_farq-uJ8CKmiX').toString();
        window.localStorage.setItem('place_order', ciphertext)
        router.push('/payment', undefined, {locale})
        
    }


    const selectAddress = (id)=>{
        const address = valuesAddress.filter(item=>item.id===id)
        const data = {...address[0], name: dataUser.name, lastName: dataUser.lastName, email: dataUser.email}

        saveOrderInfoCokkie(dataSubmit(data))
    }



    return(
        <MainComponent>
            <Container fluid>
                <Tracker />
                {
                    checkAuth() ? (<div className='col-lg-9 pt-3 mt-3 position-relative' style={{minHeight: '200px'}}>
                        <p>{t('checkout-page.item-7')}</p>
                        <div className={`justify-content-center ${addressLoader?'d-flex':'d-none'}`}>
                            <Preloader />
                        </div>
                        <div className={`${classes['address-list']} d-flex flex-wrap`}>
                            {
                                (valuesAddress || []).map(item=>{
                                    const region = regions.filter(region=>region.id===item.region_id).length?
                                        regions.filter(region=>region.id===item.region_id)[0].name : ''

                                    const districtO = district.filter(district=>district.id===item.district_id).length?
                                        district.filter(region=>region.id===item.region_id)[0].name : ''

                                    const country = 'Uzbekistan'
                                    return(
                                        <div className='d-flex justify-content-between col-lg-3  col-md-6 col-12' key={item.id} onClick={()=>selectAddress(item.id)}>
                                            <div className={`${item.defaultAddress?classes['default-address']:''} position-relative`}>
                                                <h4>{item.name}, {item.address}</h4>
                                                <p>{dataUser.name} {dataUser.lastName}<br/>
                                                    {country}, {region}, {districtO}<br/>
                                                    {item.address}<br/>
                                                    {item.phone}</p>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>): ''
                }
                <div className={`d-flex ${classes['main-wrap']}`}>

                    <Formik
                        initialValues={{
                            name: formData.name,
                            lastName: formData.lastName,
                            address: '',
                            email: '',
                            phone: formData.phone,
                            region_id: 1,
                            district_id: 1,
                            country_id: 1,
                            delivery_type: formData.delivery_type,
                            payment_type: formData.payment_type,
                            is_lifting: formData.is_lifting,
                            date_delivery: formData.date_delivery
                        }}
                        validationSchema={ClientSchema}
                        onSubmit={async (values) => {

                        }}


                    >
                        {({
                              errors,
                              values,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                              touched
                          })=>(

                            <Form className={`w-100 pt-3 ${classes['form-wrap-shipping']} d-flex flex-wrap`}
                                  onSubmit={(e)=>e.preventDefault()} autoComplete="on" >

                                <div className={`col-lg-9 col-12 pt-3`}>

                                    <div className={`${classes['contact-info']} ${classes['group-form']}`}>
                                        <p>{t('checkout-page.form-contact-title')}</p>

                                        <div className={`d-flex justify-content-between flex-wrap mt-3`}>
                                            <div className={`col-lg-6 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-shipping-label1')}</p>
                                                <Field name='name'
                                                        className={`${errors.name && touched.name ? classes['error-field']:''}`}
                                                        value={values.name}
                                                        onBlur={handleBlur}
                                                        onChange={handleChange}
                                                />
                                            </div>
                                            <div className={`col-lg-6 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-shipping-label2')}</p>
                                                <Field name='lastName'
                                                        className={`${errors.lastName && touched.lastName ? classes['error-field']:''} `}
                                                        value={values.lastName}
                                                        onBlur={handleBlur}
                                                        onChange={handleChange}
                                                />
                                            </div>

                                            <div className={`col-lg-6 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-shipping-label9')}</p>

                                                <div className="input-group mb-3 d-flex input-group-phone flex-nowrap">
                                                    <span className="input-group-text" id="basic-addon1">+998</span>
                                                    <Field value={values.phone}
                                                           name='phone'
                                                           className={`${errors['phone'] && touched['phone'] ? classes['error-field'] : '' } mb-0`}
                                                           onChange={handleChange}
                                                           onBlur={handleBlur}
                                                           component = {MaskInput}
                                                    />
                                                </div>


                                            </div>

                                            <div className={`col-lg-6 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-contact-label')}</p>
                                                <Field name='email'
                                                    className={`${errors.email && touched.email ? classes['error-field']:''}`}
                                                    value={values.email}
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}

                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className={`${classes['group-form']} mt-3`}>

                                        <div className={`d-flex justify-content-between flex-wrap`}>

                                            <div className={`col-lg-12 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.item-1')}</p>
                                                <Field name='address'
                                                       className={`${errors.address && touched.address ? classes['error-field']:''}`}
                                                       value={values.address}
                                                       onBlur={handleBlur}
                                                       onChange={handleChange}

                                                />
                                            </div>

                                            <div className={`col-lg-4 pl-0`}>
                                                <p className='mb-0'><label className='mb-0'>{t('account.address-input-label2')}</label></p>
                                                <Field name='country_id'
                                                        as='select'
                                                        disabled
                                                        className={`${errors.country_id && touched.country_id ? classes['error-field']:''} w-100`}
                                                        value={values.country_id}
                                                        onBlur={handleBlur}
                                                        onChange={handleChange}
                                                >
                                                    <option value={values.country_id} selected>Uzbekistan</option>
                                                </Field>
                                            </div>

                                            <div className={`col-lg-4 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-shipping-label3')}</p>
                                                <Field name='region_id'
                                                       as='select'
                                                       className={`${errors.region_id && touched.region_id ? classes['error-field']:''} w-100`}
                                                       value={values.region_id}
                                                       onBlur={handleBlur}
                                                       onChange={handleChange}
                                                >
                                                    {
                                                        regions.map(region=>{
                                                            return(
                                                                <option value={region.id}  data-id={region.id} key={region.id}>{region.name}</option>
                                                            )
                                                        })
                                                    }

                                                </Field>
                                            </div>

                                            <div className={`col-lg-4 pl-0`}>
                                                <p className={`mb-0`}>{t('checkout-page.form-shipping-label4')}</p>
                                                <Field name='district_id'
                                                       as='select'
                                                       className={`${errors.district_id && touched.district_id ? classes['error-field']:''} w-100`}
                                                       value={values.district_id}
                                                       onBlur={handleBlur}
                                                       onChange={handleChange}
                                                >
                                                    {
                                                        district.map(distr=>{
                                                            return(
                                                                <option value={distr.id}  data-id={distr.id} key={distr.id}>{distr.name}</option>
                                                            )
                                                        })
                                                    }
                                                </Field>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`${classes['group-form']} my-3`}>
                                        <p>{t('checkout.calendar-title')}</p>
                                        <div className={`${classes['wrap-calendar']} col-lg-6 pl-0`}>
                                            <div className={`${classes['date-picker']}`}>
                                                <input type="datetime-local" className="mt-3"
                                                       onChange={(e)=>{
                                                           setFormData({...formData, date_delivery: e.target.value})
                                                       }}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`col-lg-3 col-12 pt-5 pb-3`}>
                                    <OrderForm
                                        cartTotal={totalPrice}
                                        handleSubmit={handleSubmit}
                                        values={values}
                                        dataSubmit={dataSubmit}
                                        errors={errors}
                                        sendOrderForm={saveOrderInfoCokkie}
                                        isSubmitting={isSubmitting}
                                        touched={touched}
                                    />
                                </div>

                            </Form>
                        )}
                    </Formik>
                </div>
            </Container>
        </MainComponent>
    )
}

const mapStateToProps = state=>({
    cartObj: state.cart,
    regions: state.regions,
    district: state.district
})

const mapDispatchToPRops = {
    setCart

}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(Checkout)
