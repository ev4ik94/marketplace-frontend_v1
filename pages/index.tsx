import MainComponent from '../components/Main-Component'
import React, {useEffect, useState} from 'react'
import Skeleton from "react-loading-skeleton"
import {useRouter} from "next/router"

/*---Bootstrap-components---*/
import {Container} from "react-bootstrap"

/*---Components---*/

import CarouselBanner from "../components/pages-component/home-components/carousel-banner"
import {CarouselProducts} from "../components/pages-component/home-components/carousel-products"
import {RecentlyViewed} from "../components/pages-component/home-components/recently-viewed"
import {CarouselProducts2} from "../components/pages-component/home-components/carousel-products-2"
import {OffersPartners} from "../components/pages-component/home-components/offers-partners"

/*----Virt Db----*/
import {offers} from '../virtDb/offers-partner'


/*----Redux----*/
import {connect} from 'react-redux'

/*-----Hooks----*/
import {AxiosApi} from "../hooks/axios.hook"


/*---Fetch Data ----*/

function FetchBanner(){
    return new Promise((resolve:(value: {}[])=>void) => {
        setTimeout(() => {
            resolve([
                {
                    id: 1,
                    src: '/images/homepage_banner-1.jpg',
                    title: 'Smart Телевизоры',
                    text1: 'Всего за 2 млн. сум',
                    text2: 'Смотрите свои любимые фильмы напрямую и откройте для себя мир развлечений',
                    button: 'Купить сейчас'


                },
                {
                    id: 2,
                    src: '/images/116.jpg',
                    title: '',
                    text1: '',
                    text2: '',
                    button: null
                },
                {
                    id: 3,
                    src: '/images/HomeConnect_OVERALL_open_Keyvisual_v03.jpg',
                    title: '',
                    text1: '',
                    text2: '',
                    button: null
                }
            ]);
        }, 500);
    });
}

function FetchOffers(){
    return new Promise((resolve:(value:{}[])=>void) => {
        setTimeout(() => {
            resolve(offers);
        }, 500);
    });
}



function Home({recently}){

    const [loadingState, setStateLoading] = useState({
        bannerLoading: true,
        offersLoading: true,
        homeLoading: true
    })
    const [banner, setBanner] = useState([])
    const [offersData, setOffers] = useState([])
    const [windowWidth, setWindow] = useState(0)
    const router = useRouter()
    const {locale} = router


    const [productsHome, setProductsHome] = useState([])
    const {request} = AxiosApi()

    useEffect(()=>{
        if(typeof window !== 'undefined'){
            setWindow(window.innerWidth)
            window.onresize = ()=>setWindow(window.innerWidth)
        }
    }, [])


    useEffect(()=>{
        if(loadingState.bannerLoading){
            FetchBanner().then(result=>{
                setBanner(result)
                setStateLoading({...loadingState, bannerLoading: false})
            })
        }
        if(loadingState.offersLoading){
            FetchOffers().then(result=>{
                setOffers(result)
                setStateLoading({...loadingState, offersLoading: false})
            })
        }

        if(loadingState.homeLoading){
            getProductsHome()
        }
    }, [loadingState])

    const getProductsHome = async()=>{
        await request(`${process.env.API_PRODUCTS_HOME[locale]}?perPage=12`)
            .then(result=>{
               setProductsHome(result.data)
            })
    }




    return(
        <MainComponent>
            <div>
                <Container fluid className={`d-flex pt-5 flex-wrap`} style={{height: windowWidth>=992?'600px':'auto'}}>
                    {
                        loadingState.bannerLoading?(<div className='col-lg-6 col-12'><Skeleton width={'100%'} height={'100%'}/></div>):(<CarouselBanner banner={banner}/>)
                    }
                    <CarouselProducts products={productsHome} />

                </Container>
                {
                    recently.length?(<RecentlyViewed recently={recently}/>):('')
                }
                <Container fluid className='pt-3'>
                    <CarouselProducts2 products={productsHome} />
                    <OffersPartners offers={offersData}/>
                </Container>
            </div>
        </MainComponent>
    )
}




const mapStateToProps = state=>({
    popular: state.popular,
    recently: state.recently
})

export default connect(mapStateToProps)(Home)
