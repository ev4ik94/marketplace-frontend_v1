import {useState, useEffect} from 'react'
import {useRouter} from "next/router"
import {useTranslation} from "react-i18next"

/*-----Hooks---------*/
import {AxiosApi} from "../../../hooks/axios.hook"
import {useAuth} from "../../../hooks/authentication.hook"

/*----Components----*/
import MainComponent from "../../../components/Main-Component"
import {AlertMessage} from "../../../components/pages-component/identity/forms/Alert-message"

/*------Bootstrap------*/
import {Container} from "react-bootstrap";


export default function Verification(){
    const [valueSubmit, setValueSubmit] = useState('')
    const [user, setUser] = useState(null)
    const [success, setSuccess] = useState(false)
    const [errors, setErrors] = useState([])
    const [alertSuccess, setAlertSuccess] = useState('')

    const {request} = AxiosApi()
    const {t} = useTranslation()
    const {login} = useAuth()
    const router = useRouter()
    const {locale} = router

    useEffect(()=>{
        if(typeof window !== 'undefined'){
            const emailVer = JSON.parse(localStorage.getItem('verE'))
            setUser(emailVer && emailVer!==null ? emailVer: '')
            localStorage.removeItem('verE')
        }
    }, [])

    const submitVerifi = async()=>{
        setValueSubmit('')
        await request(`${process.env.VERIFICATION_EMAIL[locale]}`, 'POST', {
            email: user.email,
            code: valueSubmit
        }).then(result=>{

            if(result.data.email_verified){
                setAlertSuccess(result.data.message)
                setErrors([])
                router.push('/account', null, {locale})
                setSuccess(true)
                login({
                    first_name: user.firstName,
                    last_name: user.lastName,
                    email: user.email,
                    phone: user.phone
                })
            }else{
                const errArr = []
                setSuccess(false)
                if(result.data.message){
                    errArr.push({
                        field: '',
                        message: result.data.message
                    })

                }else{
                    for(let value in result.data){
                        errArr.push({
                            field: value,
                            message: result.data[value]
                        })
                    }

                }
                setErrors(errArr)
            }

        })

    }
    return(
        <MainComponent>
            <>
                <Container style={{minHeight: '530px'}} className='pt-5'>
                    <h1 className='text-center font-weight-bold'>{t('identity.verifi-1')} <span>{user!==null?user.email:''}</span></h1>
                    <p className='text-center'>{t('identity.verifi-2')}</p>
                    <div className='d-flex justify-content-center mt-3'>
                        <input type="text" value={valueSubmit} onChange={(e)=>setValueSubmit(e.target.value)}/>
                        <button onClick={()=>submitVerifi()}>Submit</button>
                    </div>
                    <AlertMessage isSuccess={success} errors={errors} alertSuccess={alertSuccess}/>
                </Container>
                <style jsx>
                    {
                        `

                      h1{
                      color: #1d2938;
                      }
                      h1 > span{
                      color: #3de7bc
                      }
                      button{
                        background-color: #007aff;
                        border: none;
                        outline: none;
                        color: #fff;
                        border-radius: 5px;
                        padding: 5px 10px;
                        margin-left: 10px;
                      }
                      
                      .container-verifi{
                        height: 600px;
                        min-height: 600px;
                      }
                    
                    `
                    }
                </style>
            </>
        </MainComponent>
    )
}
