import  App, { AppProps } from 'next/app'
import React from 'react'
import Router from 'next/router'
import '../styles/global.sass'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-inner-image-zoom/lib/InnerImageZoom/styles.css'
import '../i18n'



/*---Redux---*/
import {Provider} from 'react-redux'
import withRedux from 'next-redux-wrapper'
import store from '../redux/store'

/*---Components----*/
import MainComponent from "../components/Main-Component"
import {Preloader} from "../components/preloader/Preloader"

/*---Skeleton----*/
import {HomePreloader} from "../components/skeleton/Home-Preloader"
import {CategoriesList} from "../components/skeleton/Categories-List"
import {ProductList} from "../components/skeleton/Product-List"
import {ProductViewPreloader} from "../components/skeleton/Product-View"
import {CartPreloader} from "../components/skeleton/Cart-preloader"
import {CompareListPreloader} from '../components/skeleton/Compare-list-preloader'
import {Profile} from '../components/skeleton/Profile'
import {OrderTrackPreloader} from "../components/skeleton/OrderTrack-preloader"
import {AccountAddressPreloader} from "../components/skeleton/Account-address-preloader"
import {AccountPersonalPreloader} from "../components/skeleton/Account-personal-preloader"
import {AccountHistoryPreloader} from "../components/skeleton/Account-history-preloader"
import {AccountDefaultPreloader} from "../components/skeleton/Account-default-preloader"
import {RecentlyPreloader} from "../components/skeleton/Recently-preloader"

interface IState{
  loading: boolean
}

interface IProps{

}

class MyApp extends App<IProps,IState> {

  state = {
    loading: true,
    path: ''
  }




  static async getInitialProps({Component, ctx}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}
    return {pageProps: pageProps}
  }

  handlerStart(url){
      this.setState({path: url })
      if(!this.state.loading) this.setState({loading: true})
  }

    handlerComplete(){
        this.setState({loading: false})

    }

    componentDidMount(){

      window.onload = (e)=>{

          if(e.type==='load'){
              this.handlerStart(this.props.router.asPath)
              setTimeout(()=>this.handlerComplete(), 50)
          }
      }

        Router.events.on('routeChangeStart', (url)=>{
            console.log('start')
            this.handlerStart(url)
        })
        Router.events.on('routeChangeComplete', ()=>{
            console.log('complete')
            this.handlerComplete()
        })
        Router.events.on('routeChangeError', ()=>this.handlerComplete())
    }

    componentWillUnmount() {

        Router.events.off('routeChangeStart', (url)=>{
            this.handlerStart(url)
        })
        Router.events.off('routeChangeComplete', ()=>{
            this.handlerComplete()
        })
        Router.events.off('routeChangeError', ()=>this.handlerComplete())
    }




    skeletonTemplates(url){

      const urlP = url!==''?url:this.props.router.asPath

      if(urlP==='/'){
          return <HomePreloader />
      }else if(urlP.match(/\/categories/gi)!==null){
          return <CategoriesList />
      }else if(urlP.match(/\/products/gi)!==null){
          return <ProductList />
      }else if(urlP.match(/\/product/gi)!==null){
          return <ProductViewPreloader />
      }else if(urlP.match(/\/cart/gi)!==null){
          return <CartPreloader />
      }else if(urlP.match(/\/compare/gi)!==null){
          return <CompareListPreloader />
      }
      else if(urlP.match(/\/account\/address/gi)!==null){
          return <AccountAddressPreloader />
      }
      else if(urlP.match(/\/account\/personal/gi)!==null){
          return <AccountPersonalPreloader />
      }
      else if(urlP.match(/\/account\/order/gi)!==null){
          return <AccountHistoryPreloader />
      }
      else if(urlP.match(/\/account\//gi)!==null){
          return <AccountDefaultPreloader />
      }
      else if(urlP.match(/\/account/gi)!==null){
          return <Profile />
      }
      else if(urlP.match(/\/track-order/gi)!==null){
          return <OrderTrackPreloader />
      }
      else if(urlP.match(/\/recently-viewed/gi)!==null){
          return <RecentlyPreloader />
      }
      else{
          return <Preloader />
      }

  }


  render(){

    const {Component, pageProps} = this.props

      if(this.state.loading){
        if(typeof window !== 'undefined'){
            window.scrollTo({
                top: 0,
                behavior: "smooth"
            })
        }

        return(
            <>
                {this.skeletonTemplates(this.state.path)}
            </>
        )
      }

        return (

            <Provider store={store}>
              <Component {...pageProps} />
            </Provider>

        )

  }

}

const makeStore = ()=> store

export default withRedux(makeStore)(MyApp)
