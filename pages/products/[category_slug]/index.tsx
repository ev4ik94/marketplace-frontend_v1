import {useRouter} from "next/router"
import {useState, useEffect} from 'react'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'




/*---Components----*/
import MainComponent from "../../../components/Main-Component"
import {CostReplace} from '../../../components/secondary-func'
import {AddToCartModal} from "../../../components/alerts/add-to-cart"
import {LazyLoadImage} from "../../../components/preloader/Lazy-Load-Image"
import {BreadCrumbs} from "../../../components/pages-component/BreadCrumbs"
import {Preloader} from "../../../components/preloader/Preloader"
import {GetProducts} from "../../../components/skeleton/getProducts-Preloader"
import {PaginationBlock} from "../../../components/Paginatin-Block"


/*----Hooks----*/
import {AxiosApi} from "../../../hooks/axios.hook"


/*---Constant---*/
import {SORT_NEW, SORT_ASC, SORT_DESC, SORT_PRICE} from "../../../constants"


/*---Styles---*/
import classes from '../../../styles/pages-components/products/products.module.sass'

/*---Bootstrap---*/
import {Container, Dropdown, Spinner} from "react-bootstrap"

/*---Icons---*/
import {Compare} from "../../../components/icons/Compare"
import {Settings} from '../../../components/icons/Settings'
import {X} from 'react-bootstrap-icons'
import {Truck} from '../../../components/icons/Truck'
import {Setting} from '../../../components/icons/Setting'
import {Garantiya} from '../../../components/icons/Garantiya'
import {Cart} from '../../../components/icons/Cart'

/*----Components----*/
import {FilterProducts} from "../../../components/pages-component/products/Filter"

/*----Redux----*/
import {connect} from 'react-redux'
import {setCompare} from "../../../redux/actions/actioncompare";
import {setCart} from "../../../redux/actions/actionCart"
import {Rating} from "../../../components/pages-component/Rating";
import {setFilters} from "../../../redux/actions/actionFilters"
import {setCurrentCategory} from "../../../redux/actions/actionCategory"
import {setBreadCrumbs} from "../../../redux/actions/actionBreadCrums"
import {setSaved} from "../../../redux/actions/actionSaved";

/*------Functions------*/
import {isSave} from "../../../components/secondary-func"


/*---Interfaces----*/

interface ICategories {
    id: number,
    recursive_children: ICategories[],
    description: string,
    icon_url: {},
    name: string,
    parent_id: number
}

interface ILinks{
    category_slug:string,
    name: string
}



interface IProducts{
    products: {
        total: number,
        data: IData[]
    },
    cart: {
        items: IData[],
        total_price: number,
        total_quantity: number
    },
    error: {
        state: boolean,
        status: string | null
    }
}

interface IData{
    id: number,
    count: number,
    current_page: number,
    total: number,
    data: IProductsList[]
}

interface IProductsList{
    brand: string,
    category: {slug: string},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {
        price: number,
        old_price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface ICart{
    items: ICartItems[],
    total_price: number,
    total_quantity: number
}

interface ICartItems{
    cart_id: number,
    quantity: number,
    sku: IProductsList
}

interface ICompare{
    items: IProductsList[],
    category_slug: string | null,
    attribute_groups: []
}





function ProductList(
    {
        filters,
        setFilters,
        setCurrentCategory,
        compare,
        setCompare,
        setCart,
        cart,
        currentCat,
        breadCrumbs,
        setBreadCrumbs,
        setSaved,
        saved

    }:
        {
            filters:{},
            setFilters: (value: any)=>void
            compare: ICompare,
            setCompare: (compare:ICompare)=>void,
            setCart: (cart:any)=>void,
            setCurrentCategory: (category:string)=>void,
            cart: ICart,
            currentCat: string,
            breadCrumbs: ILinks[],
            setBreadCrumbs: (value: ILinks[])=>void,
            setSaved: (value: [])=>void,
            saved: IProductsList[]
        }){




    const [productsList, setProducts] = useState([])

    const {t} = useTranslation()
    const [currentSortValue, setSortValue] = useState('')
    const [totalProducts, setTotalProducts] = useState(0)
    const [compareProducts, setCompareProducts] = useState([])
    const [cartArr, setCartArr] = useState([])
    const [CartObj, setCartObj] = useState(null)
    const [showCartModal, setCartShowCart] = useState(false)
    const [stateShow, setStateShow] = useState(false)
    const [listFilters, setListFilters] = useState([])



    /*-----Mount Query------*/

    const [showContent, setShowContent] = useState(false)
    const [mountProducts, setMountProducts] = useState(true)
    const [mountFilter, setMountFilter] = useState(true)

    const {request, loading} = AxiosApi()
    const router = useRouter()
    const {locale} = router
    const category_slug = router.query.category_slug


    const sortArr = [
        {
            queries: [
                {
                    name: 'sortBy',
                    value: SORT_NEW
                },
                {
                    name: 'order',
                    value: SORT_ASC
                }
            ],
            title: t('product-list.dropdown-button-sort-new')
        },
        {
            queries: [
                {
                    name: 'sortBy',
                    value: SORT_PRICE
                },
                {
                    name: 'order',
                    value: SORT_ASC
                }
            ],
            title: t('product-list.dropdown-button-sort-price-to-high')
        },
        {
            queries: [
                {
                    name: 'sortBy',
                    value: SORT_PRICE
                },
                {
                    name: 'order',
                    value: SORT_DESC
                }
            ],
            title: t('product-list.dropdown-button-sort-price-to-low')
        }
    ]


    useEffect(()=>{
        let a = []
        for(let filter in router.query){
            if(filter!=='page'&&filter!=='category_slug'){
                if(filter.match(/brands/g)){
                    const srch = listFilters.filter(item=>item.value===router.query[filter])
                    if(!srch.length){
                        a.push({
                            id: filter,
                            value: router.query[filter]
                        })
                    }
                }else if(filter.match(/attributes/g)){
                    const data = searchFilter(filters['attributes'], Number(router.query[filter]))
                    const srch = listFilters.filter(item=>item.id===Number(router.query[filter]))

                    if(!srch.length){
                        a.push({
                            id: Number(router.query[filter]),
                            value: data?data.name:''
                        })

                    }
                }
            }
        }

        setListFilters([...a])
    }, [filters])

    const searchFilter = (arr, filterId)=>{

        if(arr){
            for(let k=0; arr.length>k; k++){
                if(arr[k].id===filterId) {
                    return arr[k]
                }
                else{
                    if(arr[k].children){
                        for(let i=0; arr[k].children.length>i; i++){
                            if(arr[k].children[i].id===filterId){
                                return arr[k].children[i]
                            }
                        }
                    }
                }
            }
        }

    }



    useEffect(()=>{
        if(currentCat===''){
            setCurrentCategory(category_slug.toString())
            getFilters()
            setBreadCrumbs([])
        }else{
            if(currentCat!==category_slug){
                getFilters()
                setCurrentCategory(category_slug.toString())
                setBreadCrumbs([])
            }
        }

    }, [router.query])



    useEffect(()=>{
        setSortValue(router.query.sortBy && router.query.order?sortSwitch():t('product-list.dropdown-button-sort-new'))
    }, [])

    useEffect(()=>{
        setCompareProducts(compare.items?compare.items:[])
    }, [compare])



    useEffect(()=>{
        if(JSON.stringify(filters)==='{}'){
            getFilters()
        }
    }, [])


    useEffect(()=>{
        if(mountProducts){
            getProducts()
        }

    }, [mountProducts])

    const getProducts = async()=>{
        const queryString = generateRequest()

        await request(`${process.env.API_PRODUCTS_LIST[locale]}${queryString}`)
            .then(result=>{
                setProducts(result.data.data)
                setShowContent(true)
                setTotalProducts(result.data.total)
            })
    }

    /*-----Function Generate new Request for product list---------*/
    function generateRequest(){
        let queryString = ''
        let count = 0

        for(let query in router.query){

            if(count === 0){
                queryString += `?${query}=${router.query[query]}`
            }else{
                queryString += `&${query}=${router.query[query]}`
            }
            count++
        }
        return queryString
    }

    const getFilters = async()=>{
        await request(`${process.env.FILTER_PRODUCT[locale]}?category_slug=${category_slug}`)
            .then(result=>{
                const data = result.data
                setBreadCrumbs(data.breadcrums)
                setFilters(data)
                setMountFilter(false)
            })
    }

    const selectAttribute = (arrQueries)=>{
        let dataQuery = {}
        const resultQueries = router.query
        arrQueries.forEach(item=>{
            if(item.value===null){
                delete resultQueries[item.name]
            }else{
                return dataQuery[item.name] = item.value
            }
        })

        router.push(
            {
                pathname: router.pathname,
                query: Object.assign(resultQueries,dataQuery, {page: 1})
            },
            undefined,
            { shallow: true, locale })
    }




    //----Сортировка запросов для фильтров----//

    const sortSwitch = ()=>{
        switch(router.query.sortBy){
            case SORT_NEW:
                return t('product-list.dropdown-button-sort-new')
            case SORT_PRICE:
                if(router.query.order===SORT_ASC){
                    return t('product-list.dropdown-button-sort-price-to-high')
                }else{
                    return t('product-list.dropdown-button-sort-price-to-low')
                }
            default:
                return t('product-list.dropdown-button-sort-new')

        }
    }


    /*-----Preloader Controller----*/

    const responsePreloader = (element, response)=>{
        const buttons = element.querySelectorAll('button')

        if(response){
            buttons.forEach(item=>item.setAttribute('disabled', 'true'))
            element.classList.add(classes['loading-transition'])
        }else{
            buttons.forEach(item=>item.setAttribute('disabled', 'false'))
            element.classList.remove(classes['loading-transition'])
        }
    }

    /*------Обработчик кликов по кнопкам compare, save---*/

    const buttonsClick = (e)=>{

        const data_id = e.currentTarget.getAttribute('data-productid')
        const element = document.querySelector(`[data-productid='${data_id}']`)


        if(element && element!==null){
            responsePreloader(element, true)
        }



        return async(type, warehouse_slug, slug, category_slug)=>{
            if(type==='compare'){
                if(isCompare(warehouse_slug, slug)){
                    deleteCompares(element ,warehouse_slug, slug)
                }else{

                    if(compare.category_slug!==null && compare.category_slug!==category_slug){
                        const quest = confirm(t('product-list.err-compare-add'))
                        if(quest){
                            return removeAllAdd(element, warehouse_slug, slug)
                        }else{
                            responsePreloader(element, false)
                            return
                        }

                    }

                    await addCompare(element, warehouse_slug, slug)

                }
            }else if(type==='save'){
                if(isSave(warehouse_slug, slug, saved)){
                    deleteSave(element,warehouse_slug, slug)
                }else{
                    await addSave(element, warehouse_slug, slug)
                }
            }

        }
    }

    /*--------Request Save------------*/

    const addSave = async(element, warehouse_slug, slug)=>{
        await request(`${process.env.ADD_SAVED[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                setSaved(result.data.list)
                if(element!==null){
                    responsePreloader(element, false)
                }
            })
    }

    const deleteSave = async(element, warehouse_slug, slug)=>{
        await request(`${process.env.REMOVE_SAVED[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                setSaved(result.data.list)
                if(element!==null){
                    responsePreloader(element, false)
                }
            })
    }

    /*-----Request Добавить в сравнить----*/

    const addCompare = async(element, warehouse_slug, slug)=>{

        if(compareProducts.length<4){
            await request(`${process.env.COMPARES_ADD[locale]}/${warehouse_slug}/${slug}`, 'PUT')
                .then(result=>{
                    setCompare(result.data)
                    if(element!==null){
                        responsePreloader(element, false)
                    }
                }).catch(()=>{
                    if(element!==null){
                        responsePreloader(element, false)
                    }
                })

        }else{
            responsePreloader(element, false)
            alert(t('product-list.err-compare-add1'))
        }


    }

    /*-------Request Remove All and Add -------------*/
    const removeAllAdd = async(element, warehouse_slug, slug)=>{

        await request(`${process.env.COMPARES_REMOVE_ALL_ADD[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                setCompare(result.data)
                if(element!==null){
                    responsePreloader(element, false)
                }
            }).catch(()=>{
                if(element!==null){
                    responsePreloader(element, false)
                }
            })


    }

    /*-----Request Удалить из сравнить----*/

    const deleteCompares = async(element, warehouse_slug, slug)=>{
        setCompare({
            items: compareProducts.filter(item=>item.slug!==slug),
            category_slug: compareProducts.length?compare.category_slug:null,
            attribute_groups: compareProducts.length?compare.attribute_groups:[]
        })
        await request(`${process.env.COMPARES_REMOVE[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(()=>{
                responsePreloader(element, false)
            }).catch(e=>console.log(''))

    }


    /*-----Проверка на продукт в сравнение добавлен----*/

    const isCompare = (warehouse_slug, slug)=>{
        if(compareProducts.length){
            return compareProducts.filter(item=>item.warehouse.slug===warehouse_slug && item.slug===slug).length>0
        }else{
            return false
        }

    }


    /*-----Проверка на продукт в корзину добавлен----*/

    const isAddCart = (slug, warehouse_slug)=>{

        let arr = (cart.items || []).filter(item=>item.sku.slug===slug && item.sku.warehouse.slug===warehouse_slug)
        return arr.length !== 0
    }


    /*//--------CART EVENTS-----------//*/

    /*-----Request добавить в корзину----*/

    const addToCart = async(e,warehouseSlug, slug)=>{

        const button = Array.from(e.currentTarget.getElementsByClassName(classes['spinner-button']))
        button.forEach(item=>{
            let btn = item as HTMLElement
            btn.classList.remove('d-none')
        })

        await request(`${process.env.ADD_TO_CART[locale]}/${warehouseSlug}/${slug}`, 'PUT')
            .then(result=>{

                let data = result.data.items.filter(item=>item.sku.slug===slug).length?result.data.items.filter(item=>item.sku.slug===slug)[0]:{}

                const dataO = {product:data, cart:{
                        totalCount: result.data.total_quantity,
                        totalPrice: result.data.total_price
                    }}

                setCart(result.data)

                setCartObj(dataO)
                button.forEach(item=>{
                    let btn = item as HTMLElement
                    btn.classList.add('d-none')
                })

                setCartArr([...cartArr, data.sku])
                setCartShowCart(true)

            }).catch(e=>{
                console.log(e.response)
            })


    }

    /*-------FILTER LIST EVENTS------------------*/
    const deleteFilter = (id)=>{
        setListFilters(listFilters.filter(item=>item.id!==id))
        for(let val in router.query){
            if(router.query[val]===String(id)){
                selectAttribute([{name: val, value: null}])
            }else if(val===id){
                selectAttribute([{name: val, value: null}])
            }
        }

    }

    const deleteAll = ()=>{
        setListFilters([])
        router.push(`/products/${category_slug}`, undefined, {locale})
    }


    const HideHandle = () => setCartShowCart(false)

    return(
        <MainComponent>
            <>
                <div className={`${classes['overlay-filter']} position-fixed w-100 h-100 ${stateShow?classes['active']:''}`} onClick={()=>setStateShow(false)}/>
                <AddToCartModal cartObj={CartObj} onHide={HideHandle} show={showCartModal} catId={Number(router.query.category_id)}/>
                <Container fluid className={classes['container-products']}>

                    <BreadCrumbs arr={breadCrumbs}/>

                    <div className={`d-flex ${classes['wrap-product-list']}`}>

                        {/*============= FILTERS-START================================*/}

                        <FilterProducts filters={filters} selectAttribute={selectAttribute} stateShow={stateShow} setStateShow={setStateShow}/>

                        {/*============= FILTERS-END================================*/}

                        <div className={`col-lg-9 col-12 ${classes['products-list-container']} pr-lg-3 pr-0 pl-lg-3 pl-0 `}>
                            <div className={`${classes['top-content-products']} 
                            d-flex justify-content-between flex-lg-row flex-column pt-3 pb-3 p-3`}>
                                <p className={`font-weight-bold mb-lg-0 text-lg-center text-left`}>{totalProducts} {t('product-list.items')}</p>
                                {/*=====================SORT_BY_COST-START=============================*/}
                                <div className='d-lg-block d-flex pr-3 col-lg-6 pl-0'>
                                    <Dropdown className={`${classes['dropdown-filters']} d-flex col-md-5 col-6 col-lg-12 p-0 align-items-center justify-content-lg-end`}>
                                        <p className={`mb-0 text-lg-center text-left pr-3 d-lg-block d-none`}>
                                            {t('product-list.dropdown-title')}
                                        </p>
                                        <Dropdown.Toggle variant="success" id="dropdown-basic" className='position-relative'>
                                            {currentSortValue}
                                        </Dropdown.Toggle>

                                        <Dropdown.Menu className='w-100'>
                                            {
                                                sortArr.map((item, index)=>{
                                                    return(
                                                        <p key={index} onClick={()=>{
                                                            selectAttribute(item.queries)
                                                        }}>{item.title}</p>
                                                    )
                                                })
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <button className={`${classes['button-filter']} 
                                    d-lg-none d-flex ml-3 col-md-5 col-6 align-items-center justify-content-center p-0`}
                                            onClick={()=>setStateShow(true)}
                                    >
                                        <Settings />
                                        <p className='mb-0'>{t('product-view-button-filter')}</p>
                                    </button>
                                </div>
                                {/*=====================SORT_BY_COST-END=============================*/}
                            </div>

                            {/*==========================FILTERS LIST SELECTED START========================*/}
                            {
                                listFilters.length?(<div className={`${classes['filters-list-selected']} d-flex align-items-center justify-content-between flex-wrap`}>
                                    <p className='mb-0 font-weight-bold'>{t('product-list.filters-list')}</p>
                                    <div className={`${classes['filters-list-block']} d-flex col-8 flex-wrap`}>
                                        {
                                            listFilters.map((filter, index)=>{

                                                return(
                                                    <div key={index} className='d-flex align-items-center'>
                                                        <p className='mb-0'>{filter.value}</p>
                                                        <button onClick={()=>{
                                                            deleteFilter(filter.id)
                                                        }}><X /></button>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                    <button onClick={()=>deleteAll()}>{t('product-list.filters-list-delete-all')}</button>
                                </div>):''
                            }
                            {/*==========================FILTERS LIST SELECTED END========================*/}
                            <div className={'mt-3'}>
                                {
                                    showContent ? (<>
                                        {
                                            productsList.map((item, index)=>{

                                                return(
                                                    <div className={`${classes['item-product']} p-3`} key={index} data-productid={index}>
                                                        <p className='mb-lg-3 mb-0 d-lg-none d-block'>
                                                            <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                                <a>{item.name}</a>
                                                            </Link>
                                                        </p>
                                                        <div className='d-flex flex-wrap d-lg-none d-block'>
                                                            <p className='mb-0 font-weight-bold mr-3 mb-1'>{t('product-list.product-model')}: <span className='font-weight-normal'>{item.code}</span></p>
                                                            <p className='mb-0 font-weight-bold mr-3 mb-1'>{t('product-list.product-sku')}: <span className='font-weight-normal'>{item.skus[0].id}</span></p>
                                                        </div>
                                                        <div className='d-flex position-relative'>
                                                            <div className={`position-absolute ${classes['preloader-spinner']}`}><Preloader /></div>
                                                            <div className={`${classes['picture-product']} col-lg-3 col-4 pl-0`}>
                                                                <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                                    <a>
                                                                        <LazyLoadImage image={{
                                                                            src: item.images[0].original_image,
                                                                            srcSet: item.images[0].image_srcset,
                                                                            alt: item.name
                                                                        }}/>
                                                                    </a>
                                                                </Link>
                                                            </div>

                                                            <div className='d-flex col-lg-9 col-8 flex-wrap pr-0'>
                                                                <div className={`${classes['info-product']} col-lg-8 col-12 d-flex flex-column justify-content-center`}>
                                                                    <p className='mb-lg-3 mb-0 d-lg-block d-none'>
                                                                        <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                                            <a>{item.name}</a>
                                                                        </Link>
                                                                    </p>
                                                                    <div className='d-lg-flex flex-wrap d-none'>
                                                                        <p className='mb-0 font-weight-bold mr-3'>{t('product-list.product-model')}: <span className='font-weight-normal'>{item.code}</span></p>
                                                                        <p className='mb-0 font-weight-bold mr-3'>{t('product-list.product-sku')}: <span className='font-weight-normal'>{item.skus[0].id}</span></p>
                                                                    </div>
                                                                    <h3 className='font-weight-bold d-lg-none d-block mt-3'>{CostReplace(item.price.price)} СУМ</h3>
                                                                    <Rating />
                                                                    <div className={`${classes['info-settings']} mb-3 pl-0`}>
                                                                        <div>
                                                                            <div className={`${classes['icon']} d-lg-block d-none`}>
                                                                                <Truck />
                                                                            </div>
                                                                            <div>
                                                                                <p>{t('product-list.product-delivery')}: <span>{t('product.delivery')}</span></p>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className={`${classes['icon']} d-lg-block d-none`}>
                                                                                <Garantiya />
                                                                            </div>
                                                                            <div>
                                                                                <p>{t('product-list.product-garantiya')}: <span>{t('product.garantiya')}</span></p>
                                                                            </div>
                                                                        </div>
                                                                        <div>
                                                                            <div className={`${classes['icon']} d-lg-block d-none`}>
                                                                                <Setting />
                                                                            </div>
                                                                            <div>
                                                                                <p>{t('product-list.product-setting')}: <span>{t('product-list.product-setting-1')}</span></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className={`${classes['buttons-product']} d-flex flex-wrap`}>
                                                                        {
                                                                            isAddCart(item.slug, item.warehouse.slug)?
                                                                                (<Link href={'/cart'} locale={locale}>
                                                                                    <a className={`${classes['added-cart']} d-lg-none d-flex justify-content-center align-items-center`}>
                                                                                        <Cart />
                                                                                        <span>{t('button-added-cart')}</span>
                                                                                    </a>
                                                                                </Link>):(
                                                                                    <button
                                                                                        className={`d-lg-none justify-content-center d-flex ${classes['add-cart-button']}`}
                                                                                        disabled={loading}
                                                                                        onClick={(e)=>{
                                                                                            addToCart(e,item.warehouse.slug, item.slug)

                                                                                        }}
                                                                                    >

                                                                                        <Cart />

                                                                                        <p className='mb-0'>{t('cart-button')}</p>
                                                                                        <Spinner animation="border" role="status" className={`d-none ${classes['spinner-button']}`}>
                                                                                            <span className="sr-only">Loading...</span>
                                                                                        </Spinner>
                                                                                    </button>
                                                                                )
                                                                        }
                                                                        <div
                                                                            className={`d-flex col-lg-5 col-12 ${classes['button-compare']} ${isCompare(item.warehouse.slug, item.slug)?classes['active']:''}`}
                                                                            onClick={(e)=>{
                                                                                let clickFunc = buttonsClick(e)
                                                                                clickFunc('compare',
                                                                                    item.warehouse.slug, item.slug, item.category.slug)
                                                                            }} data-productid={index}>
                                                                            <Compare />
                                                                            <p className='mb-0'>{t('compare-button.compare')}</p>
                                                                        </div>

                                                                        <div className={`d-flex ${classes['button-save']} add-save-button col-lg-5 col-12 ${isSave(item.warehouse.slug, item.slug, saved)?'added-save':''}`}
                                                                             onClick={(e)=>{

                                                                                 let clickFunc = buttonsClick(e)
                                                                                 clickFunc('save', item.warehouse.slug, item.slug, item.category.slug)


                                                                             }} data-productid={index}>
                                                                            <svg height="404pt" viewBox="-58 0 404 404.54235" width="404pt" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="m277.527344 0h-267.257813c-5.519531 0-10 4.476562-10 10v374.527344c-.007812 7.503906 4.183594 14.378906
                                                        10.855469 17.808594 6.675781 3.425781 14.707031 2.828124 20.796875-1.550782l111.976563-80.269531 111.980468
                                                        80.265625c6.09375 4.371094 14.117188 4.964844 20.789063 1.539062 6.667969-3.425781 10.863281-10.296874
                                                        10.863281-17.792968v-374.527344c0-5.523438-4.480469-10-10.003906-10zm-10 384.523438-117.796875-84.441407c-3.484375-2.496093-8.171875-2.496093-11.652344
                                                        0l-117.800781 84.445313v-364.527344h247.25zm0 0"/>
                                                                            </svg>
                                                                            <p className='mb-0'>{t('button-save')}</p>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className={`${classes['product-price-cart']} col-lg-4 col-12 flex-column mt-lg-0 mt-3 d-lg-flex d-none`}>
                                                                    <p>{CostReplace(item.price.price)} СУМ</p>
                                                                    {
                                                                        isAddCart(item.slug, item.warehouse.slug)?
                                                                            (<Link href={'/cart'} locale={locale}>
                                                                                <a className={`${classes['added-cart']} d-flex justify-content-center align-items-center`}>
                                                                                    <Cart />
                                                                                    <span>{t('button-added-cart')}</span>
                                                                                </a>
                                                                            </Link>):(
                                                                                <button
                                                                                    className={`d-flex justify-content-center`}
                                                                                    disabled={loading}
                                                                                    onClick={(e)=>{
                                                                                        addToCart(e,item.warehouse.slug, item.slug)

                                                                                    }}
                                                                                >

                                                                                    <Cart />
                                                                                    <p className='mb-0'>{t('cart-button')}</p>
                                                                                    <Spinner animation="border" role="status" className={`d-none ${classes['spinner-button']}`}>
                                                                                        <span className="sr-only">Loading...</span>
                                                                                    </Spinner>
                                                                                </button>
                                                                            )
                                                                    }

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </>) : (<GetProducts />)
                                }
                            </div>
                        </div>
                    </div>
                    <PaginationBlock total={totalProducts}/>
                </Container>
            </>
        </MainComponent>
    )
}




const mapStateToProps = state=>({
    categories: state.categories,
    compare: state.compare,
    cart: state.cart,
    filters: state.filters,
    currentCat: state.currentCat,
    breadCrumbs: state.breadCrumbs,
    saved: state.saved

})

const mapDispatchToPRops = {
    setCompare,
    setCart,
    setFilters,
    setCurrentCategory,
    setBreadCrumbs,
    setSaved

}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(ProductList)



