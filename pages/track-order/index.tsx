import {useTranslation} from "react-i18next"
import {useEffect, useState} from "react";
import React from "react";
import {useRouter} from "next/router"

/*----Components----*/
import MainComponent from "../../components/Main-Component"
import {LazyLoadImage} from "../../components/preloader/Lazy-Load-Image"
import {OrderTrackPreloader} from '../../components/skeleton/OrderTrack-preloader'
import { decryptString} from '../../components/secondary-func'

/*-----Bootstrap components----*/
import {Container} from "react-bootstrap"

/*----Styles-----*/
import classes from '../../styles/pages-components/checkout/track-order.module.sass'

/*----Redux----*/
import {connect} from 'react-redux'



/*----Hooks----*/
import {AxiosApi} from "../../hooks/axios.hook";


/*------Interfaces----------*/
interface IOrder {
    address: string,
    country_id: number,
    discount_price: number,
    district_id: number,
    email: string,
    first_name: string,
    id: number,
    last_name: string,
    product_skus: IOrderProduct[],
    region_id: number,
    total_price: number,
    track_id: string,
    provided_delivery_date: string,
    user_id: number
}

interface IOrderProduct{
    brand: {},
    category: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {},
    quantity: number,
    slug: string,
    warehouse: {}
}

interface IRegion{
    id: number,
    name: string
}
interface IDistrict{
    id: number,
    name: string
}



 function TrackOrder({regions, district}:{regions:IRegion[], district: IDistrict[]}){

    const {t} = useTranslation()
    const [dataOrder, setOrder] = useState<IOrder | null>(null)
    const [products, setProducts] = useState<IOrderProduct[]>([])
    const [ordersBtn, setOrdersBtn] = useState(true)

    const {request, loading} = AxiosApi()
     const router = useRouter()
     const {locale} = router

    useEffect(()=>{

        if(document!==undefined){
            const order_track = decryptString(localStorage.getItem('order_track'))

            if(order_track!==null){
               
                const {track_id} = order_track
                getTrackInfo(track_id || null)
               
            }

        }
    }, [])

    const getTrackInfo = async(track_id)=>{
        await request(`${process.env.TRACK_ORDER[locale]}/${track_id}`)
            .then(result=>{
                console.log(result.data)
                setOrder(result.data)
                setProducts(result.data.product_skus?result.data.product_skus:[])
            }).catch(e=>console.log(e))
    }

   

    const getCurrentDate = (dateF=null)=>{
        const date = dateF!==null?new Date(dateF):new Date()
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jule', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']

        const month = months[date.getMonth()]
        const day = date.getDate()<9?`0${date.getDate()}`:date.getDate()
        const year = date.getFullYear()


        return `${month} ${day}, ${year}`
    }

    const getAddress  = (region, districtB, address)=>{
        const regionText = regions.filter(item=>item.id===region)
        const districtText = district.filter(item=>item.id===districtB)

        return `Uzbekistan, ${regionText.length? regionText[0].name:''}, ${districtText.length? districtText[0].name:''}, ${address}`
    }



    if(dataOrder!==null){
        return(
            <MainComponent>
                <Container fluid>
                    <div className={`${classes['tabs']} d-flex mt-5`}>
                        <button className={ordersBtn?classes['active']:''} onClick={()=>setOrdersBtn(true)}>{t('order-track.text-button1')}</button>
                        <button className={!ordersBtn?classes['active']:''} onClick={()=>setOrdersBtn(false)}>{t('order-track.text-button2')}</button>
                    </div>
                    {/*============ ORDERS LIST ACTIVE START=========================*/}
                    <div className={`${classes['items-orders']} mt-5 mb-5 ${!ordersBtn? 'd-none':''}`}>
                        <div className={`${classes['title-order-list']} d-flex flex-wrap`}>
                            <div className='col-lg-5 col-md-5 col-12'>
                                <p className='mb-0 font-weight-bold'>Online Order
                                    <span className='font-weight-normal'> {dataOrder.track_id}</span></p>
                            </div>
                            <div className='col-lg-5 col-md-5 col-12'>
                                <p className='font-weight-bold mb-0'>
                                    {getCurrentDate()}
                                </p>
                            </div>
                        </div>
                        <div className={`${classes['list-products']} pt-3 pb-3`}>
                            {
                                products.map((item, index)=>{
                                    return(
                                        <div className={`${classes['item-product']} d-flex align-items-center justify-content-between`} key={index}>
                                            <div className='col-lg-2 col-md-5 col-sm-6 col-5'>
                                                <LazyLoadImage  image={{
                                                    src: item.images[0].original_image,
                                                    srcSet: item.images[0].image_srcset,
                                                    alt: item.name
                                                }}/>
                                            </div>

                                            <div className='col-lg-10 col-md-7 col-sm-6 col-7 d-flex justify-content-center flex-wrap pl-0'>
                                                <div className='col-lg-4 col-12'>
                                                    <p>{item.name}</p>
                                                    <p>{t('order-track.text-text3')}: <span>{item.quantity}</span></p>
                                                </div>

                                                <div className='col-lg-4 col-12'>
                                                    <p className='font-weight-bold'>{t('order-track.text-text1')}</p>
                                                    <p>{getAddress(dataOrder.region_id, dataOrder.district_id, dataOrder.address)}</p>
                                                </div>

                                                <div className={`${classes['watch-delivery-status']} col-lg-2 col-12`}>
                                                    <p>{t('order-track.text-text4')}</p>
                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" style={{width: '25%'}} />
                                                    </div>
                                                    <p className='mt-1'>{t('order-track.text-text2')} <span className='font-weight-bold'>{dataOrder.provided_delivery_date}</span></p>
                                                </div>
                                            </div>




                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                    {/*============ ORDERS LIST ACTIVE END=========================*/}

                    {/*============ ORDERS LIST CANCELED START=========================*/}
                    <div className={`${classes['orders-canceled']} ${ordersBtn?'d-none':''}`}>
                        <h1>{t('order-track.text-text6')}</h1>
                    </div>
                    {/*============ ORDERS LIST CANCELED END=========================*/}

                </Container>
            </MainComponent>
        )
    }

    else if(loading){
        return(
            <MainComponent>
                <OrderTrackPreloader />
            </MainComponent>
        )
    }



    return(
        <MainComponent>
            <Container fluid>
                <div style={{height: '300px'}} className='pt-5'>
                    <h1 className='text-center'>{t('order-track.text-text5')}</h1>
                </div>
            </Container>
        </MainComponent>
    )



}


const mapStateToProps = state=>({
    regions: state.regions,
    district: state.district
})



// @ts-ignore
export default connect (mapStateToProps)(TrackOrder)
