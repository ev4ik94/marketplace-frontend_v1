var CryptoJS = require("crypto-js");

export const CostReplace = (n:string)=>{
    let parts = n.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}

export function randomId (){
    const a = Math.floor(Math.random() * 10000),
        b = Math.floor(Math.random() * 10000),
        c = Math.floor(Math.random() * 10000),
        d = Math.floor(Math.random() * 10000);

    return String(`${a}-${b}-${c}-${d}`);
}

export function checkErrors(errorObj){
    var arr = {}
    for(let err in errorObj){
        arr = {...arr, [err]: errorObj[err].length?errorObj[err]:errorObj[err]}
    }
    return arr
}

/*======= Check save product=============*/

export function isSave(warehouse_slug, slug, saved){
    const arr = saved.length?saved.filter(item=>{
        return item.warehouse.slug===warehouse_slug && item.slug===slug
    }):[]
    return arr.length!==0
}


export function decryptString(str){
   
    var bytes = CryptoJS.AES.decrypt(str, 'secret_key_farq-uJ8CKmiX');
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    
    return decryptedData
}



export function setCookie(name, value, options = null) {

    options = {
      path: '/',
      expires: new Date(),
      // при необходимости добавьте другие значения по умолчанию
      ...options
    };
  
    if (options.expires instanceof Date) {
      options.expires = options.expires.toUTCString();
    }
  
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
  
    for (let optionKey in options) {
      updatedCookie += "; " + optionKey;
      let optionValue = options[optionKey];
      if (optionValue !== true) {
        updatedCookie += "=" + optionValue;
      }
    }
  console.log(updatedCookie)
    document.cookie = updatedCookie;
  }
  
//   // Пример использования:
//   setCookie('user', 'John', {secure: true, 'max-age': 3600});

export function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }


export function deleteCookie(name){
    document.cookie = name+'=;'+'Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;domain=.farq.uz;samesite=strict;';
}
