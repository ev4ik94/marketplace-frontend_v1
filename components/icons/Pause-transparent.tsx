

export function PauseTransparent(){
    return(
        <svg width="16" height="16" fill="currentColor"
             className="bi bi-pause-circle" viewBox="0 0 16 16">
            <circle className="circle" cx="8" cy="8" r="7" strokeWidth="1" fillOpacity="0"></circle>
            <path
                d="M5 6.25a1.25 1.25 0 1 1 2.5 0v3.5a1.25 1.25 0 1 1-2.5 0v-3.5zm3.5 0a1.25 1.25 0 1 1 2.5 0v3.5a1.25 1.25 0 1 1-2.5 0v-3.5z"/>
        </svg>
    )
}
