import Skeleton from "react-loading-skeleton"
import {useEffect, useState} from "react"

/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function AccountAddressPreloader(){

    const [windowWidth, setWindowWidth] = useState(0)

    useEffect(()=>{
        if(window!==undefined){
            setWindowWidth(window.innerWidth)
            window.addEventListener('resize', ()=>{
                setWindowWidth(window.innerWidth)
            })
        }
    }, [])


    return(
        <Container fluid>
            <div className='mt-5'>
                <p><Skeleton  width={'60%'} height='30px'/></p>
                <p><Skeleton  width={'40%'} height='30px'/></p>
            </div>
            <div className='d-flex flex-wrap justify-content-start mt-5 mb-3'>
                <div className='col-lg-3 d-lg-block d-none'>
                    <Skeleton height={'100%'}></Skeleton>
                </div>
                <div className='col-lg-8 col-12'>
                    <div className='d-lg-none d-block'>
                        <p><Skeleton  width={'100%'} height='50px'/></p>
                    </div>
                    <div>
                        <p><Skeleton  width={'60%'} height='30px'/></p>
                        <p><Skeleton  width={'40%'} height='30px'/></p>
                    </div>
                    <div>
                        <p><Skeleton  width={'100%'} height='200px'/></p>
                        <p><Skeleton  width={'100%'} height='200px'/></p>
                    </div>
                    <div>
                        <p><Skeleton  width={'100%'} height='80px'/></p>
                    </div>
                </div>
            </div>
        </Container>
    )
}
