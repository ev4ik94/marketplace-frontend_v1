import Skeleton from "react-loading-skeleton"


/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function ProductViewPreloader() {
    return(
        <Container fluid>
            <div className='pt-5'>
                <Skeleton height='30px'/>
                <Skeleton width='60%' height='30px'/>
                <Skeleton  width='50%' height='30px'/>
            </div>
            <div className='d-flex pt-3 flex-wrap'>
                <div className='col-lg-7 col-12 pl-0'>
                    <Skeleton width='100%' height={'100%'} style={{minHeight: '500px'}}/>
                </div>
                <div className='col-lg-5 col-12 d-flex flex-column pl-lg-3 pl-0 mt-lg-0 mt-3'>
                    <Skeleton width='60%' height='30px'/>
                    <Skeleton width='40%' height='30px'/>
                    <div className='row mt-3'>
                        <div className='col-4'>
                            <Skeleton height={'50px'}/>
                        </div>
                        <div className='col-4'>
                            <Skeleton height={'50px'}/>
                        </div>
                        <div className='col-4'>
                            <Skeleton height={'50px'}/>
                        </div>
                    </div>
                    <Skeleton height={'50px'} className='mt-3'/>
                    <div className='row pt-3'>
                        <div className='col-6'>
                            <Skeleton height={'50px'}/>
                        </div>
                        <div className='col-6'>
                            <Skeleton height={'50px'}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className={'mt-3'}>
                <Skeleton height={'100px'}/>
                <Skeleton height={'300px'} className='mt-3'/>
            </div>
        </Container>
    )
}
