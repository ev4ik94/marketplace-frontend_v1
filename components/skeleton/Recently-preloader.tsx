import Skeleton from "react-loading-skeleton"

/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function RecentlyPreloader(){

    return(
        <Container fluid>
            <div className='mt-5 d-flex'>
                <div className='col-lg-4'>
                    <p><Skeleton  height='250px'/></p>
                    <div className='mb-2'>
                        <Skeleton  height='30px'/>
                        <Skeleton  height='30px' width='50%'/>
                        <Skeleton  height='25px' width='80%'/>
                        <Skeleton  height='30px' width='40%'/>
                    </div>

                    <div className='d-flex flex-wrap justify-content-between'>
                        <p className='col-12 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                    </div>
                </div>
                <div className='col-lg-4'>
                    <p><Skeleton  height='250px'/></p>
                    <div className='mb-2'>
                        <Skeleton  height='30px'/>
                        <Skeleton  height='30px' width='50%'/>
                        <Skeleton  height='25px' width='80%'/>
                        <Skeleton  height='30px' width='40%'/>
                    </div>

                    <div className='d-flex flex-wrap justify-content-between'>
                        <p className='col-12 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                    </div>
                </div>
                <div className='col-lg-4'>
                    <p><Skeleton  height='250px'/></p>
                    <div className='mb-2'>
                        <Skeleton  height='30px'/>
                        <Skeleton  height='30px' width='50%'/>
                        <Skeleton  height='25px' width='80%'/>
                        <Skeleton  height='30px' width='40%'/>
                    </div>

                    <div className='d-flex flex-wrap justify-content-between'>
                        <p className='col-12 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                        <p className='col-lg-5 p-0'><Skeleton  height='50px'/></p>
                    </div>
                </div>
            </div>
        </Container>
    )
}
