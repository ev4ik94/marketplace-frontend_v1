import Skeleton from "react-loading-skeleton"

/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function AccountHistoryPreloader(){

    return(
        <Container fluid>
            <div className='d-flex flex-wrap justify-content-start mt-5 mb-3'>
                <div className='col-lg-3 d-lg-block d-none'>
                    <Skeleton height={'100%'}></Skeleton>
                </div>
                <div className='col-lg-8 col-12'>
                    <div className='d-flex'>
                       <div className='col-lg-8'>
                           <p><Skeleton  width={'100%'} height='30px'/></p>
                           <p><Skeleton  width={'100%'} height='30px'/></p>
                       </div>
                        <div className='col-lg-4'>
                            <p><Skeleton  width={'100%'} height='120px'/></p>
                        </div>
                    </div>
                    <div className='mt-3'>
                        <p><Skeleton  width={'100%'} height='220px'/></p>
                    </div>

                </div>
            </div>
        </Container>
    )
}
