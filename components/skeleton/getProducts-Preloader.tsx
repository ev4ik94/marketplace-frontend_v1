import Skeleton from "react-loading-skeleton"
import classes from "../../styles/pages-components/products/products.module.sass";



export function GetProducts(){
    const products = [1,2,3,4,5,6]
    return(
        <div>
            {
                products.map(item=>{
                    return(
                        <div className={`d-flex ${classes['item-product']} p-3 position-relative`} key={item} data-productid={item}>
                            <div className={`${classes['picture-product']} col-lg-3`}>
                                <div>
                                    <Skeleton height={'200px'}/>
                                </div>
                            </div>

                            <div className="d-flex col-lg-9 col-8 flex-wrap pr-0">
                                <div className={`${classes['info-product']} col-lg-6 d-flex flex-column justify-content-between`}>
                                    <div>
                                        <p className='mb-1'><Skeleton height={'30px'} width={'100%'}/></p>
                                        <p className='mb-1'><Skeleton height={'30px'} width={'50%'}/></p>
                                        <p className='mb-1'><Skeleton height={'30px'} width={'40%'}/></p>
                                    </div>
                                    <div className='mt-3'>
                                        <Skeleton height={'30px'} width={'80%'}/>
                                    </div>
                                    <div className={`${classes['buttons-product']} d-flex justify-content-between mt-3 flex-wrap`}>
                                        <div className={'col-lg-5 p-0'}>
                                            <Skeleton height={'40px'}/>
                                        </div>

                                        <div className={'col-lg-5 p-0'}>
                                            <Skeleton height={'40px'}/>
                                        </div>
                                    </div>
                                </div>

                                <div className={`${classes['product-price-cart']} col-lg-3 d-flex flex-column justify-content-center`}>
                                    <p className='d-lg-block d-none'><Skeleton height={'30px'}/></p>
                                    <div>
                                        <Skeleton height={'40px'}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}
