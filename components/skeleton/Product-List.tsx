import Skeleton from "react-loading-skeleton"


/*----Bootstrap---*/
import {Container} from 'react-bootstrap'

export function ProductList(){
    return(
        <Container fluid>
           <div className='pt-5'>
               <Skeleton height='30px'/>
               <Skeleton width='60%' height='30px'/>
           </div>
            <div className='d-flex pt-3 flex-wrap'>
                <div className='col-lg-3 col-4 d-lg-block d-none'>
                    <Skeleton width='100%' height='90vh'/>
                </div>
                <div className='col-lg-3 col-4 d-lg-none d-block col-12 mb-5'>
                    <Skeleton width='100%' height='150px'/>
                </div>
                <div className='col-lg-9 col-12'>
                    <Skeleton count={3} width='100%' height='250px'/>
                </div>
            </div>
        </Container>
    )
}
