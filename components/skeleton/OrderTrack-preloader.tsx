import Skeleton from "react-loading-skeleton"

import {Container} from "react-bootstrap";


export function OrderTrackPreloader(){
    return(
        <Container fluid>
            <div className='mt-5'>
                <p><Skeleton width={'60%'} height={'30px'}/></p>
               <p><Skeleton width={'40%'} height={'30px'}/></p>
            </div>

            <div className='mt-5'>
                <Skeleton width={'100%'} height={'180px'}/>
            </div>
        </Container>
    )
}
