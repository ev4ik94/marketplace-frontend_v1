import Skeleton from "react-loading-skeleton"

/*----Bootstrap----*/
import {Container} from 'react-bootstrap'


export function CompareListPreloader(){
    return(
        <Container fluid>
            <Skeleton className='mt-5' width='50%' height={'30px'}/>
            <div className='d-flex mt-3'>
                <div  style={{width: '300px'}} className='mr-5'>
                    <div>
                        <Skeleton width='100%' height={'300px'}/>
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>

                    <div className='mt-3'>
                        <Skeleton width='80%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'44px'} />
                    </div>
                </div>
                <div  style={{width: '300px'}} className='mr-5'>
                    <div>
                        <Skeleton width='100%' height={'300px'}/>
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>

                    <div className='mt-3'>
                        <Skeleton width='80%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'44px'} />
                    </div>
                </div>
                <div  style={{width: '300px'}}>
                    <div>
                        <Skeleton width='100%' height={'300px'}/>
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>

                    <div className='mt-3'>
                        <Skeleton width='80%' height={'30px'} />
                        <Skeleton width='60%' height={'30px'} />
                    </div>
                    <div className='mt-3'>
                        <Skeleton width='100%' height={'44px'} />
                    </div>
                </div>
            </div>
            <div className='mt-3'>
                <Skeleton width='100%' height={'44px'} />
            </div>
            <div className='mt-3'>
                <Skeleton width='100%' height={'44px'} />
            </div>
        </Container>
    )
}
