import Skeleton from "react-loading-skeleton"
import {useEffect, useState} from "react"

/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function Profile(){

    const [windowWidth, setWindowWidth] = useState(0)

    useEffect(()=>{
        if(window!==undefined){
            setWindowWidth(window.innerWidth)
            window.addEventListener('resize', ()=>{
                setWindowWidth(window.innerWidth)
            })
        }
    }, [])


    return(
        <Container fluid>
            <div className='mt-5'>
                <p><Skeleton  width={'60%'} height='30px'/></p>
                <p><Skeleton  width={'40%'} height='30px'/></p>
            </div>
            <div className='d-flex flex-wrap justify-content-start mt-5 mb-3'>
                <div className='col-lg-4 mt-3'>
                    <Skeleton  width={'100%'} height={windowWidth>992?'200px':'60px'}/>
                </div>
                <div className='col-lg-4 mt-3'>
                    <Skeleton   width={'100%'} height={windowWidth>992?'200px':'60px'}/>
                </div>
                <div className='col-lg-4 mt-3'>
                    <Skeleton   width={'100%'} height={windowWidth>992?'200px':'60px'}/>
                </div>
                <div className='col-lg-4 mt-3'>
                    <Skeleton   width={'100%'} height={windowWidth>992?'200px':'60px'}/>
                </div>
                <div className='col-lg-4 mt-3'>
                    <Skeleton   width={'100%'} height={windowWidth>992?'200px':'60px'}/>
                </div>
            </div>
        </Container>
    )
}
