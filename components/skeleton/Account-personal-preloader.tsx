import Skeleton from "react-loading-skeleton"
import {useEffect, useState} from "react"

/*----Bootstrap---*/
import {Container} from 'react-bootstrap'


export function AccountPersonalPreloader(){


    return(
        <Container fluid>
            <div className='d-flex flex-wrap justify-content-start mt-5 mb-3'>
                <div className='col-lg-3 d-lg-block d-none'>
                    <Skeleton height={'100%'}></Skeleton>
                </div>
                <div className='col-lg-8 col-12'>
                    <div>
                        <p><Skeleton  width={'60%'} height='30px'/></p>
                        <p><Skeleton  width={'40%'} height='30px'/></p>
                    </div>
                    <div className='mt-3'>
                        <p><Skeleton  width={'100%'} height='80px'/></p>
                        <p><Skeleton  width={'100%'} height='80px'/></p>
                        <p><Skeleton  width={'100%'} height='80px'/></p>
                    </div>

                </div>
            </div>
        </Container>
    )
}
