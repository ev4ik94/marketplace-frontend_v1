import {useRouter} from "next/router";
import Pagination from "react-js-pagination";


/*----Style-----*/
import classes from '../styles/pages-components/pagination.module.sass'

export function PaginationBlock ({total}:{total:number}){

    const router = useRouter()
    const {page} = router.query
    const activePage = page?page:1
    const totalPage = Math.ceil(total/12)

    function changePage(page){
        router.push(
            {
                pathname: router.pathname,
                query: {...router.query, page:page}
            },
            undefined,
            { shallow: true })
    }

    if(total>0){
        return(
            <div className={`d-flex ${classes['pagination']} justify-content-center mt-5 mb-5`}>


                <div className="pagination">

                    <Pagination
                        activePage={Number(activePage)}
                        itemsCountPerPage={12}
                        totalItemsCount={total}
                        pageRangeDisplayed={5}
                        innerClass={classes['pagination_link']}
                        activeLinkClass={classes['active_page']}
                        prevPageText={Number(activePage)===1?'':'⟨'}
                        firstPageText={Number(activePage)<6?'':'«'}
                        lastPageText={Number(activePage)===totalPage?'':'»'}
                        nextPageText={Number(activePage)===totalPage?'':'⟩'}
                        onChange={changePage}
                    />
                </div>
            </div>
        )
    }

    return (<></>)
}
