import {useEffect, useState} from 'react'
import {useRouter} from "next/router"
import {useTranslation} from "react-i18next"




/*---Components---*/
import {Header} from './main-components/Header'
import {Footer} from './main-components/Footer'
import {CompareBlock} from "./main-components/compare-block"
import { setCookie } from './secondary-func'

/*---Virt Db----*/
import {categoriesBot} from '../virtDb/categoriesM'

/*----Redux----*/
import {connect} from 'react-redux'
import {setCategories} from '../redux/actions/actionCategories'
import {setCompare} from '../redux/actions/actioncompare'
import {setCart} from '../redux/actions/actionCart'
import {getBrands, setBrands} from '../redux/actions/actionBrands'
import {setRecently} from "../redux/actions/actionRecently"
import {setPopularProducts} from "../redux/actions/actionPopularProducts"
import {setSaved} from "../redux/actions/actionSaved"
import {setRegions} from "../redux/actions/actionRegions"
import {setDistrict} from "../redux/actions/actionDistrict"
import {setLoading} from "../redux/actions/actionLoading"

/*----Hooks----*/
import {AxiosApi} from "../hooks/axios.hook"



interface IProducts{
    brand: string,
    category: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {
        price: number,
        old_price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface ICart{
    items: [],
    total_price: number,
    total_quantity: number
}

interface ICompare{
    items: ICompareItems[],
    attribute_groups: [],
    category_slug: string | null
}

interface ICompareItems{
    brand: string,
    category: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {
        price: number,
        old_price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}



function MainComponent (
    {
        children,
        categories,
        compare,
        setLoading,
        getBrands,
        setCompare,
        cart,
        brands,
        saved,
        regions,
        setRegions,
        setDistrict,
        setCart,
        setSaved,
        setCategories,
        setBrands,
        recently,
        setRecently,
        setPopularProducts,
        popular


    }:
        {
            children?: React.ReactElement,
            categories: [],
            setLoading: (val:boolean)=>void,
            compare: ICompare,
            setCompare: (compare:ICompare)=>void,
            setDistrict: (district:any)=>void,
            cart: ICart,
            regions: [],
            setRegions: (value: [])=>void,
            setCart: (value: ICart)=>void,
            getBrands: ()=>void,
            brands: [],
            saved: [],
            setCategories: (value: [])=>void,
            setBrands: (value: [])=>void,
            setSaved: (value:any)=>void,
            recently: IProducts[],
            setRecently: (value: IProducts[])=>void,
            setPopularProducts: (value: IProducts[])=>void,
            popular: IProducts[]
        }
        ){


    const [dropDownState, setDropDown] = useState(false)
    var activeDropdowns = []
    const {request} = AxiosApi()
    const {i18n} = useTranslation()

    const router = useRouter()
    const {locale} = router
    var isMount = true

   
    useEffect(()=>{
        
        if(isMount){
            callAllRequest()
        }
        
        return ()=>{
            isMount = false
        }
    }, [])

    useEffect(()=>{
        i18n.changeLanguage(router.locale)
    }, [router.locale])



    const callAllRequest = ()=>{
    
       
        Promise.all([
            getBrandsF(),
            getCategoriesF(),
            getCartF(),
            getDistrict(),
            getCompareF(),
            getRecently(),
            getSaved(),
            getPopularProducts(),
            getRegions(),
        ]).then(() => {setLoading(false)})
            .catch(ex => console.error(ex));
    }


    /*--------Handler Functions --------*/

    const onToggleMenu = (id, state)=>{
    
        if(state){
            activeDropdowns.push(id)
        }else{
            activeDropdowns = activeDropdowns.filter(elem=>elem!=id)
        }
    
        setDropDown(activeDropdowns.length>0)
    }


    /*-----Queries GET---------------*/

    const getCompareF = async()=>{
        await request(process.env.COMPARES_GET_LIST[locale])
            .then(result=>{
                setCompare(result.data)
            }).catch(e=>console.log(e))
    }

    const getCartF = async()=>{
        await request(process.env.GET_CART[locale])
            .then(result=>{
                setCart(result.data)
            }).catch(err=>console.log(err.message))
    }

    const getCategoriesF = async()=>{

        await request(process.env.API_CATEGORIES[locale])
            .then(result=>{
                setCategories(result.data)
            })
    }

    const getBrandsF = async()=>{
        await request(process.env.API_BRANDS[locale])
            .then(result=>{
                setBrands(result.data)
            })
    }

    const getRecently = async()=>{

        await request(process.env.GET_RECENTLY_LIST[locale])
            .then(result=>{
                setRecently(result.data)
            }).catch(e=>{console.log(e)})
    }

    const getPopularProducts = async()=>{
        await request(process.env.GET_POPULAR_PRODUCTS[locale])
            .then(result=>{
                setPopularProducts(result.data)
            }).catch(e=>{})
    }

    const getSaved = async()=>{
        await request(`${process.env.GET_SAVED[locale]}`)
            .then(result=>{
                setSaved(result.data)
            }).catch(e=>{ })
    }

    const getRegions = async()=>{

        await request(process.env.API_GET_REGIONS[locale])
            .then(result=>{
                setRegions(result.data)
            })
    }

    const getDistrict = async()=>{

        await request(process.env.API_GET_DISTRICTS[locale])
            .then(result=>{
                setDistrict(result.data)
            })
    }

    




    return(
        <>
            <Header
                menu={categories}
                cart={cart&&cart.total_quantity?cart.total_quantity:0}
                cartObj={cart}
                brands={brands}
                onToggleMenu={onToggleMenu}
                recently={recently}
                popular = {popular}
                setCart={setCart}
                save={saved}
            />
            <div className={`${dropDownState?'d-block':'d-none'} position-absolute w-100 h-100`}
                 style={{
                     backgroundColor: 'rgba(0,0,0,.8)',
                     left: '0',
                     top: '0',
                     zIndex: 9
                 }}/>
                <div className='position-relative'>
                    {children}

                    <CompareBlock compare={compare} setCompare={setCompare}/>
                </div>
            <Footer categoriesTop={categories} categoriesBot={categoriesBot}/>
        </>
    )
}

const mapStateToProps = state=>({
    categories: state.categories,
    compare: state.compare,
    cart: state.cart,
    brands: state.brands,
    recently: state.recently,
    popular: state.popular,
    saved: state.saved,
    regions: state.regions,
    district: state.district
})

const mapDispatchToPRops = {
    setCompare,
    setCart,
    getBrands,
    setCategories,
    setBrands,
    setLoading,
    setRecently,
    setPopularProducts,
    setRegions,
    setSaved,
    setDistrict


}

// @ts-ignore
export default connect (mapStateToProps, mapDispatchToPRops)(MainComponent)
