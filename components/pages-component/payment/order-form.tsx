import { useTranslation } from 'react-i18next'
import {useRouter} from "next/router"

/*----Styles----*/
import classes from '../../../styles/pages-components/cart/form-application.module.sass'

/*---Components---*/
import {CostReplace} from "../../secondary-func";

export function PaymentOrderForm({
                              cartTotal,
                              shipping
                          }:
                              {cartTotal:number,
                                  shipping: number | string


                              }){
    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router


    return(
        <div className={`${classes['wrap-form']}`}>
            <h3 className={`font-weight-bold`}>{t('cart-page.form-application1')}</h3>
            <div className={classes['summery-order-cost']}>
                <div className={`d-flex justify-content-between`}>
                    <p className={`mb-0`}>{t('cart-page.form-application2')}</p>
                    <p className={`mb-0`}>{CostReplace(cartTotal+'')} UZS</p>
                </div>
                <div className={`d-flex justify-content-between`}>
                    <p className={`mb-0`}>{t('cart-page.form-application3')}</p>
                    <p className={`mb-0`}>{'0'} UZS</p>
                </div>
                <div className={`d-flex justify-content-between`}>
                    <p className={`mb-0`}>{t('cart-page.form-application4')}</p>
                    <p className={`mb-0`}>{shipping}</p>
                </div>
            </div>

            <div className={classes['total-cost']}>
                <div className={`d-flex justify-content-between`}>
                    <p className={`mb-0`}>{t('cart-page.form-application5')}</p>
                    <p className={`mb-0`}>{CostReplace(cartTotal+'')} UZS</p>
                </div>
            </div>

            <input className={classes['continue-checkout']} type='submit' value={`${t('cart-page.form-submit')}`} onClick={(e)=>{
                e.preventDefault()
                router.push('/preview-order', undefined ,{locale})
            }}/>
        </div>
    )
}
