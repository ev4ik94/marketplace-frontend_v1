import {useRouter} from "next/router"
import Link from "next/link"

import classes from "../../styles/pages-components/breadCrumbs.module.sass"


/*----Interface---*/

interface ILinks{
    category_slug:string,
    name: string
}


export function BreadCrumbs({arr}:{arr:ILinks[]}){

    const router = useRouter()
    const {locale} = router
    return(
        <div className={`${classes['bread-crumbs']}`}>

            <div className='flex-wrap'>
                <p className='mb-0'>
                    <Link href='/' locale={locale}>
                        <a>Farq</a>
                    </Link>
                </p>
                {
                    arr.map((slug, index)=>{

                        if(router.query.category_slug===slug.category_slug){
                            return(
                                <p key={index} className='mb-0'>{slug.name}</p>
                            )
                        }else{
                            return(
                                <p key={index} className='mb-0'>
                                    <Link href={`/products/${slug.category_slug}`} locale={locale}>
                                        <a>{slug.name}</a>
                                    </Link>
                                </p>
                            )
                        }
                    })
                }
            </div>
        </div>
    )
}
