import {useRouter} from "next/router"
import {useTranslation} from "react-i18next"
import {useEffect, useState} from "react"
import * as Yup from "yup"
import {Field, Form, Formik} from "formik"

/*---Styles----*/
import classes from "../../../styles/pages-components/account/account-slug.module.sass"

/*---Bootstrap----*/
import {Alert} from 'react-bootstrap'

/*---Components----*/
import {MaskInput} from "../checkout/masked-input"


/*----Hooks----*/
import {AxiosApi} from "../../../hooks/axios.hook"
import {useAuth} from "../../../hooks/authentication.hook"


/*-----Icons---*/
import {Plus, X} from "react-bootstrap-icons"
import {checkErrors} from "../../secondary-func";




/*--Interface----*/

interface IObjects{
    id: number,
    slug: string,
    name: {
        ru: string,
        uz: string
    },
    description: string,
    icon: string,
    link_name: string
}

interface IAddress{
    id: number,
    name: string,
    lastName: string,
    address: string,
    region_id: number,
    district_id: number,
    country_id: string,
    phone: string,
    addressNickname: string,
    defaultAddress: boolean

}

interface IRegion{
    id: number,
    country_id: number,
    name: string,
    code: string
}

interface IDistrict{
    id: number,
    country_id: number,
    name: string,
    code: string
}

export function Addresses({
                              object ,
                              regions,
                              district,
                              apiErrors,
                              setApiErrors,
                              setIsSuccess,
                              alertSuccess,
                              setErrors
}:
                              {
                                  object:IObjects[],
                                  regions:IRegion[],
                                  district: IDistrict[],
                                  apiErrors: any,
                                  alertSuccess: string,
                                  setApiErrors: (value: any)=>void,
                                  setErrors: (value: any)=>void,
                                  setIsSuccess: (value: any)=>void,
                              }){
    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    const MainLink = object.filter(item=>item.slug===router.query.slug).length?
        object.filter(item=>item.slug===router.query.slug)[0]:null

    const {request, error} = AxiosApi()

    const [showForm, setShowForm] = useState(false)
    const [successSubmitting, setSuccessSubmitting] = useState(false)
    const [showAlert, setShowAlert] = useState(true)
    const [valuesAddress, setValuesAddress]  = useState<IAddress[]>([])
    const [editAddress, setEditAddress] = useState(0)

    const deleteAddress = async (id)=>{

        await request(`${process.env.API_DELETE_ADDRESS[locale]}/${id}`, 'DELETE')
            .then(()=>{
                setValuesAddress(valuesAddress.filter(item=>item.id!==id))
                setShowForm(false)
                setEditAddress(0)
            }).catch(e=>{})

    }

    useEffect(()=>{
        getAddress()
    }, [router])

    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setIsSuccess(false)
            setErrors([{field: '', message: error.message}])
        }else{
            setErrors([])
            setApiErrors({})
        }
    }, [error])

    const getAddress = async()=>{

        await request(process.env.API_GET_ADDRESS_LIST[locale])
            .then(result=>{
                setValuesAddress(result.data)
            })
            .catch(e=>console.log(e.message))

    }



    return(
        <div className={`${classes['address-add-wrap']} col-lg-9`}>
            <h1>{MainLink!==null && MainLink.name?MainLink.name['ru']:''}</h1>
            <p>{t('account.address-title')}</p>

            <div className='col-lg-8 pl-0 mt-5'>
                {
                    successSubmitting ? ( <Alert variant='success' show={showAlert} onClose={() => setShowAlert(false)}>
                        <button className='float-right' onClick={()=>setShowAlert(false)}><X /></button>
                        This is a  alert with{' '}

                    </Alert>):''
                }
                {
                    valuesAddress.length ? (<div className={classes['address-list']}>
                        {
                            valuesAddress.map(item=>{
                                const region = regions.filter(region=>region.id===item.region_id).length?
                                    regions.filter(region=>region.id===item.region_id)[0].name : ''

                                const districtO = district.filter(district=>district.id===item.district_id).length?
                                    district.filter(region=>region.id===item.region_id)[0].name : ''

                                const country = 'Uzbekistan'
                                return(
                                    <div className='d-flex justify-content-between' key={item.id}>
                                        <div className={`${item.defaultAddress?classes['default-address']:''} position-relative`}>
                                            <h4>{item.name}, {item.address}</h4>
                                            <p>{item.name} {item.lastName}<br/>
                                            {country},{region}, {districtO}<br/>
                                            {item.address}<br/>
                                            {item.phone}</p>
                                        </div>
                                        <div className={`${classes['controls-address-item']} d-flex flex-column`}>
                                            <button onClick={()=>{
                                                setEditAddress(item.id)
                                                setShowForm(true)
                                                window.scrollTo({
                                                    top: 1100,
                                                    behavior: "smooth"
                                                });
                                            }}>{t('account.address-address-button')}</button>
                                            <button onClick={()=>deleteAddress(item.id)}>{t('account.address-address-button2')}</button>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>) : ''
                }
                {
                    showForm ? (<AddAddressForm
                        setShowForm={setShowForm}
                        setSuccessSubmitting={setSuccessSubmitting}
                        setValuesAddress={setValuesAddress}
                        valuesAddress={valuesAddress}
                        getAddress={getAddress}
                        apiErrors={apiErrors}
                        setApiErrors={setApiErrors}
                        setIsSuccess={setIsSuccess}
                        setErrors={setErrors}
                        editAddress={editAddress}
                        setEditAddress={setEditAddress}
                        regions = {regions}
                        district = {district}
                    />) : (
                        <div className={classes['add-new-address-block']}>
                            <p className='font-weight-bold'>{t('account.address-title-add')}</p>
                            <button className={`${classes['button-add-card']} d-flex`} onClick={()=>setShowForm(!showForm)}>
                                <div><Plus /></div>
                                <p className='mb-0'>{t('account.address-button-add')}</p>
                            </button>
                        </div>
                    )
                }
            </div>
        </div>
    )
}


function AddAddressForm({
                            setShowForm,
                            apiErrors,
                            setApiErrors,
                            setErrors,
                            setIsSuccess,
                            setSuccessSubmitting,
                            getAddress,
                            regions,
                            district,
                            valuesAddress,
                            setValuesAddress,
                            editAddress,
                            setEditAddress
}:{
    setShowForm:(e:any)=>void,
    getAddress:()=>void,
    setSuccessSubmitting:(value:any)=>void,
    valuesAddress:IAddress[],
    regions: IRegion[],
    district: IDistrict[],
    apiErrors: any,
    setApiErrors: (value:any)=>void,
    setErrors: (value:any)=>void,
    setValuesAddress: (value:IAddress[])=>void,
    setIsSuccess: (value:boolean)=>void,
    editAddress: number,
    setEditAddress:(value:number|null)=>void}){

    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router
    const [defaultAddress, setDefaultAddress] = useState(false)
    const [editAddressValues, setEditAddressValues] = useState(null)
    const [userData, setUserData] = useState(null)
    const {request, error, resetErr} = AxiosApi()
    const {getUserData} = useAuth()



    useEffect(()=>{
        if(editAddress!==0){
            setEditAddressValues(valuesAddress.filter(item=>item.id===editAddress).length?
                valuesAddress.filter(item=>item.id===editAddress)[0]:null)
        }
    }, [editAddress])

    useEffect(()=>{
        const data = getUserData()

        setUserData({
            first_name: data.first_name?data.first_name:'',
            last_name: data.last_name?data.last_name:'',})

    }, [])

    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setIsSuccess(false)
            setErrors([{field: '', message: error.message}])
        }else{
            setErrors([])
            setApiErrors({})
        }

    }, [error])




    const AddressSchema = Yup.object().shape({
        first_name: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        last_name: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        address: Yup.string().required('Required'),
        region_id: Yup.number().default(1),
        district_id: Yup.number().default(1),
        country_id: Yup.string(),
        phone: Yup.string().min(9, 'Error phone').required(),
        name: Yup.string().required('Required')
    })

    const initialVal = {
        first_name: userData!==null && userData.first_name ?userData.first_name:'',
        last_name: userData!==null && userData.last_name ?userData.last_name:'',
        address: editAddressValues!==null && editAddressValues.address ?editAddressValues.address:'',
        region_id: editAddressValues!==null && editAddressValues.region_id ?editAddressValues.region_id:1,
        country_id: 1,
        district_id: editAddressValues!==null && editAddressValues.district_id ? editAddressValues.district_id : 1,
        phone: editAddressValues!==null && editAddressValues.phone ?editAddressValues.phone:'',
        name: editAddressValues!==null && editAddressValues.name ?editAddressValues.name:''

    }

    const submitForm = async(values)=>{

        let data = {...values, is_default: defaultAddress}
        resetErr()
        if(editAddress===0){
            await request(process.env.API_ADD_ADDRESS[locale], 'POST', data)
                .then(()=>{
                    getAddress()
                    setShowForm(false)
                })
                .catch(e=>{
                    console.log(e.message)
                })
        }else{
            await request(`${process.env.API_EDIT_ADDRESS[locale]}/${editAddress}`, 'PUT', data)
                .then(()=>{
                    editAddressSubmit(data)
                    getAddress()
                    setShowForm(false)
                })
                .catch(e=>{
                    console.log(e.message)
                })
        }
    }

    const editAddressSubmit = (values)=>{
        let data = values
        data.defaultAddress = defaultAddress
        data.id = editAddress
        let arr = valuesAddress.filter(item=>item.id!==editAddress)
        arr.push(data)
        setValuesAddress(arr)
        setShowForm(false)
        setEditAddress(0)
    }



    return(
        <Formik
            initialValues={initialVal}
            enableReinitialize
            validationSchema={AddressSchema}
            onSubmit={(values) => {
                submitForm(values)
            }}
        >
            {({
                  errors,
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  touched
              })=>(
                <Form onSubmit={handleSubmit} className={classes['form-add-address']}>
                    <p className='font-weight-bold'>{t('account.address-button-add')}</p>
                    <div className={classes['inputs-form-address-block']}>
                        <div className='d-flex flex-wrap'>
                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6 col-12'>
                                <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label1')}</label></p>
                                <Field name='first_name'
                                       className={`${(errors.first_name && touched.first_name) || apiErrors.first_name  ? classes['error-field']:''} w-100`}
                                       value={values.first_name}
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                />
                                <p className={classes['errors-message']}>{apiErrors.first_name}</p>
                            </div>
                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6 col-12'>
                                <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label2')}</label></p>
                                <Field name='last_name'
                                       className={`${(errors.last_name && touched.last_name) || apiErrors.last_name  ? classes['error-field']:''} w-100`}
                                       value={values.last_name}
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                />
                                <p className={classes['errors-message']}>{apiErrors.last_name}</p>
                            </div>
                        </div>
                        <div className='pl-3 pr-3 pt-1 pb-1'>
                            <p className='mb-0'><label className='mb-0'>{t('account.address-input-label1')}</label></p>
                            <Field name='address'
                                   className={`${(errors.address && touched.address) || apiErrors.address ? classes['error-field']:''} w-100`}
                                   value={values.address}
                                   onBlur={handleBlur}
                                   onChange={handleChange}
                            />
                            <p className={classes['errors-message']}>{apiErrors.address}</p>
                        </div>
                        <div className={`d-flex flex-wrap`}>
                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-4 col-12'>
                                <p className='mb-0'><label className='mb-0'>{t('account.address-input-label2')}</label></p>
                                <Field name='country_id'
                                       as='select'
                                       disabled
                                       className={`${errors.country_id && touched.country_id ? classes['error-field']:''} w-100`}
                                       value={values.country_id}
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                >
                                    <option value="Uzbekistan" selected>Uzbekistan</option>
                                </Field>
                            </div>
                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-4 col-12'>
                                <p className='mb-0'><label className='mb-0'>{t('checkout-page.form-shipping-label3')}</label></p>
                                <Field name='region_id'
                                       as='select'
                                       className={`${errors.region_id && touched.region_id ? classes['error-field']:''} w-100`}
                                       value={values.region_id}
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                >
                                    {
                                        regions.map(region=>{
                                            return(
                                                <option value={region.id}  data-id={region.id} key={region.id}>{region.name}</option>
                                            )
                                        })
                                    }

                                </Field>
                            </div>

                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-4 col-12'>
                                <p className='mb-0'><label className='mb-0'>{t('account.address-input-label2')}</label></p>
                                <Field name='district_id'
                                       as='select'
                                       className={`${errors.district_id && touched.district_id ? classes['error-field']:''} w-100`}
                                       value={values.district_id}
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                >
                                    {
                                        district.map(distr=>{
                                            return(
                                                <option value={distr.id}  data-id={distr.id} key={distr.id}>{distr.name}</option>
                                            )
                                        })
                                    }
                                </Field>
                            </div>

                        </div>

                        <div>
                            <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6'>
                                <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label4')}</label></p>
                                <div className={`input-group mb-3 d-flex input-group-phone flex-nowrap`}>
                                    <span className="input-group-text" id="basic-addon1">+998 </span>
                                    <Field value={values.phone}
                                           name='phone'
                                           className={`${errors['phone'] && touched['phone'] ? classes['error-field'] : '' } mb-0`}
                                           onChange={handleChange}
                                           onBlur={handleBlur}
                                           component = {MaskInput}
                                    />
                                </div>
                                <p className={classes['errors-message']}>{apiErrors.phone}</p>
                            </div>
                        </div>
                    </div>

                    <div className='mt-3'>
                        <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6'>
                            <p className='mb-0'><label className='mb-0'>{t('account.address-input-label3')}</label></p>
                            <Field name='name'
                                   className={`${(errors.name && touched.name) || apiErrors.name ? classes['error-field']:''} w-100`}
                                   value={values.name}
                                   onBlur={handleBlur}
                                   onChange={handleChange}

                            />
                            <p className={classes['errors-message']}>{apiErrors.phone}</p>
                            <small>{t('account.address-input-label4')}</small>
                        </div>
                    </div>
                    <div className='mt-3'>
                        <div className={`pl-3 pr-3 pt-1 pb-1 d-flex align-items-center ${classes['checkbox-form']}`}>
                            <input type="checkbox" checked={defaultAddress} onChange={()=>setDefaultAddress(!defaultAddress)}/>
                            <label className='mb-0 pl-3'>{t('account.address-input-label5')}</label>
                        </div>
                    </div>
                    <div className='mt-3'>
                        <div className='pl-3 pr-3 pt-1 pb-1 row align-items-center justify-content-end'>
                            <button className={classes['button-cancel']} onClick={()=> {
                                setShowForm(false)
                                setEditAddress(0)
                            }}>{t('account.address-input-button')}</button>
                            <button type='submit' className={`${classes['button-save']} mr-0`}>{t('account.address-input-button1')}</button>
                        </div>
                    </div>
                </Form>
            )}


        </Formik>
    )
}


