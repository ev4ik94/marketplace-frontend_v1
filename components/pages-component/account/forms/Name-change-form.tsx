import {useTranslation} from "react-i18next"
import * as Yup from "yup"
import {Field, Form, Formik} from "formik"
import {useEffect} from 'react'

/*---Style---*/
import classes from "../../../../styles/pages-components/account/account-slug.module.sass"

/*----Hooks-----*/
import {AxiosApi} from "../../../../hooks/axios.hook"
import { useAuth } from "../../../../hooks/authentication.hook"


/*----Functions-----*/
import {checkErrors} from "../../../secondary-func"


/*--Interface----*/
interface Person{
    first_name: string,
    last_name: string,
    email: string,
    old_email: string
}


export function NameForm({
                             object,
                             setPerson,
                             setActiveForm,
                             saveChange,
                             setIsSuccess,
                             apiErrors,
                             setApiErrors,
                             setErrorsArr
}:{
    object:Person,
    apiErrors: any,
    setPerson:(value:any)=>void,
    setActiveForm:(value:string|null)=>void
    saveChange: (value:any)=>void,
    setIsSuccess: (value: any)=>void,
    setErrorsArr: (value: any)=>void
    setApiErrors: (value: any)=>void
}){

    const {t} = useTranslation()
    const {request, loading, error, resetErr} = AxiosApi()
    const {getUserData, login} = useAuth()

    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setIsSuccess(false)
            setErrorsArr([{field: '', message: error.message}])
        }else{
            setErrorsArr([])
            setApiErrors({})
        }
    }, [error])

    const AddressSchema = Yup.object().shape({
        firstname: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        lastname: Yup.string()
            .min(4, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
    })

    const initialVal = {
        firstname: object!==null && object.first_name ?object.first_name:'',
        lastname: object!==null && object.last_name ?object.last_name:''
    }

    const submitForm = async(values)=>{
        let user = getUserData()
       
        user = user!==null && user ? user : {}

        resetErr()

        const dataN = {
            firstname: values.firstname,
            lastname: values.lastname
        }


        await request(process.env.API_EDIT_NAME['ru'], 'POST', dataN)
            .then(result=>{
                setActiveForm(null)
                setIsSuccess(true)
                saveChange({
                    first_name: values.firstname,
                    last_name: values.lastname
                })
            }).catch(e=>console.log(e.message))

    }


    return(
        <div>
            <p className='font-weight-bold'>{'Edit yor Name'}</p>
            <Formik
                initialValues={initialVal}
                enableReinitialize
                validationSchema={AddressSchema}
                onSubmit={(values) => {
                    submitForm(values)
                }}
            >
                {({
                      errors,
                      values,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      touched
                  })=>{

                    return(
                        <Form onSubmit={handleSubmit} className={classes['form-edit-personal']}>
                            <div>
                                <div className='d-flex flex-wrap'>
                                    <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6 col-12'>
                                        <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label1')}</label></p>
                                        <Field name='firstname'
                                               className={`${(errors.firstname && touched.firstname) || apiErrors.firstname ? classes['error-field']:''} w-100`}
                                               value={values.firstname}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                        />
                                        <p className={classes['errors-message']}>{apiErrors.firstname}</p>
                                    </div>
                                    <div className='pl-3 pr-3 pt-1 pb-1 col-lg-6 col-12'>
                                        <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label2')}</label></p>
                                        <Field name='lastname'
                                               className={`${(errors.lastname && touched.lastname) || apiErrors.lastname ? classes['error-field']:''} w-100`}
                                               value={values.lastname}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                        />
                                        <p className={classes['errors-message']}>{apiErrors.lastname}</p>
                                    </div>
                                </div>

                                <div className='mt-3'>
                                    <div className='pl-3 pr-3 pt-1 pb-1 row align-items-center justify-content-end'>
                                        <button className={classes['button-cancel']} onClick={()=> {
                                            setActiveForm(null)
                                            resetErr()
                                        }}>{t('account.address-input-button')}</button>
                                        <button type='submit' className={`${classes['button-save']} mr-0`}>{t('account.address-input-button1')}</button>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}
