import {useTranslation} from "react-i18next";
import {useState, useEffect} from "react";
import * as Yup from "yup";
import classes from "../../../../styles/pages-components/account/account-slug.module.sass";
import {Field, Form, Formik} from "formik";


/*---------Hooks-----------*/
import {AxiosApi} from "../../../../hooks/axios.hook"
import {checkErrors} from "../../../secondary-func";

/*--Interface----*/
interface Person{
    first_name: string,
    last_name: string,
    email: string,
    old_email: string
}

export function PasswordForm({
                                 object,
                                 setPerson,
                                 setActiveForm,
                                 setIsSuccess,
                                 setErrors,
                                 apiErrors,
                                 setApiErrors
}:{
    object:Person,
    apiErrors: any,
    setPerson:(value:any)=>void,
    setActiveForm:(value:string|null)=>void,
    setIsSuccess: (value: any)=>void,
    setErrors: (value: any)=>void,
    setApiErrors: (value: any)=>void

}){

    const {t} = useTranslation()
    const {request, error, resetErr} = AxiosApi()


    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setIsSuccess(false)
            setErrors([{field: '', message: error.message}])
        }else{
            setErrors([])
            setApiErrors({})
        }
    }, [error])

    const PasswordSchema = Yup.object().shape({
        old_password: Yup.string().required(t('input.error-message1')),
        password: Yup.string().required('Required'),
        password_confirmation: Yup.string()
            .test('password', t('input.error-message-equal-password"'), function(value:string) {
                return value === this.resolve(Yup.ref('password'))
            }).required('Required')

    })

    const initialVal = {
        old_password: '',
        password: '',
        password_confirmation: ''
    }

    const submitForm = async(values)=>{
        resetErr()
        await request(process.env.API_EDIT_PASSWORD['ru'], 'POST', values)
            .then(result=>{
                setActiveForm(null)
                setIsSuccess(true)
                setPerson({...object, values})
            }).catch(e=>{})
    }

    const changeType = (eve)=>{
        eve.preventDefault()
        let parent = eve.currentTarget.parentElement
        let input = parent.querySelector('input')
        let type = input.getAttribute('type')
        eve.currentTarget.innerText = type==='password'?'hide':'show'
        input.setAttribute('type', type==='password'?'text':'password')


    }

    return(
        <div className={`${classes['wrap-form-person-details']} col-lg-6 pl-0`}>
            <div>
                <p className='mb-0'>{t('account.personal-details-text')}</p>
                <p className='mb-0'>{t('account.personal-details-text4')}</p>
            </div>

            <Formik
                initialValues={initialVal}
                enableReinitialize
                validationSchema={PasswordSchema}
                onSubmit={(values) => {
                    submitForm(values)
                }}
            >
                {({
                      errors,
                      values,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                      touched
                  })=>(
                    <Form onSubmit={handleSubmit} className={classes['form-edit-personal']}>
                        <div className={classes['wrap-change-password']}>
                            <div>
                                <div className='pt-3 pb-3'>
                                    <p className='mb-0'><label className='mb-0'>{t('account.personal-details-input3')}</label></p>
                                    <p className='position-relative'>
                                        <Field name='old_password'
                                               type='password'
                                               className={`${(errors.old_password && touched.old_password) || apiErrors.old_password ? classes['error-field']:''} w-100`}
                                               value={values.old_password}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                               autocomplete="on"
                                        />
                                        <button className='position-absolute' onClick={changeType}>show</button>
                                    </p>
                                    <p className={classes['errors-message']}>{apiErrors.old_password}</p>
                                </div>
                                <div className='pt-3 pb-3'>
                                    <p className='mb-0'><label className='mb-0'>{t('account.personal-details-input4')}</label></p>
                                    <p className='position-relative'>
                                        <Field name='password'
                                               type='password'
                                               className={`${(errors.password && touched.password) || apiErrors.password ? classes['error-field']:''} w-100`}
                                               value={values.password}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                               autocomplete="on"
                                        />
                                        <button className='position-absolute' onClick={changeType}>show</button>
                                    </p>
                                    <p className={classes['errors-message']}>{apiErrors.password}</p>
                                </div>

                                <div className='pt-3 pb-3'>
                                    <p className='mb-0'><label className='mb-0'>{t('identity.form-signup-label3')}</label></p>

                                    <p className='position-relative'>
                                        <Field name='password_confirmation'
                                               type='password'
                                               className={`${(errors.password_confirmation && touched.password_confirmation) || apiErrors.password_confirmation ? classes['error-field']:''} w-100`}
                                               value={values.password_confirmation}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                               autocomplete="on"
                                        />
                                        <button className='position-absolute' onClick={changeType}>show</button>
                                    </p>
                                    <p className={classes['errors-message']}>{apiErrors.password_confirmation}</p>
                                </div>
                            </div>

                            <div className='mt-3'>
                                <div className='pl-3 pr-3 pt-1 pb-1 row align-items-center justify-content-end'>
                                    <button className={classes['button-cancel']} onClick={()=> {
                                        setActiveForm(null)
                                        resetErr()
                                    }}>{t('account.address-input-button')}</button>
                                    <button type='submit' className={`${classes['button-save']} mr-0`}>{t('account.address-input-button1')}</button>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}
