import {useTranslation} from "react-i18next";
import {useState, useEffect} from "react";
import * as Yup from "yup";
import classes from "../../../../styles/pages-components/account/account-slug.module.sass";
import {Field, Form, Formik} from "formik";


/*-----Components-----*/
import {Preloader} from "../../../preloader/Preloader"

/*-----Functions-----*/
import {checkErrors} from "../../../secondary-func"

/*---Hooks------*/
import {AxiosApi} from "../../../../hooks/axios.hook"

/*--Interface----*/

interface Person{
    first_name: string,
    last_name: string,
    email: string,
    old_email: string
}

export function EmailForm({
                              object,
                              setPerson,
                              setActiveForm,
                              saveChange,
                              setIsSuccess,
                              setErrors,
                              setAlertSuccess,
                              apiErrors,
                              setApiErrors
}:{
    object:Person,
    apiErrors: any,
    setPerson:(value:any)=>void,
    setActiveForm:(value:string|null)=>void
    saveChange: (value:any)=>void,
    setIsSuccess: (value: any)=>void,
    setErrors: (value: any)=>void,
    setAlertSuccess: (value: any)=>void,
    setApiErrors: (value: any)=>void
}){

    const {t} = useTranslation()

    const {request, loading, error, resetErr} = AxiosApi()

    const [passwordType, setPasswordType] = useState(true)


    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setIsSuccess(false)
            setErrors([{field: '', message: error.message}])
        }else{
            setErrors([])
            setApiErrors({})
        }
    }, [error])


    const AddressSchema = Yup.object().shape({
        old_email: Yup.string().email('Invalid email').required('Required'),
        email: Yup.string().email('Invalid email').required('Required'),
        password: Yup.string().required('Required')

    })

    const initialVal = {
        old_email: object.email,
        email: '',
        password: ''
    }

    const submitForm = async (values)=>{
        resetErr()
        await request(process.env.API_EDIT_EMAIL['ru'], 'POST', values)
            .then(result=>{
                if(result.data.changed){
                    setIsSuccess(true)
                    setPerson({...object, old_email: values.old_email, email: values.email})
                    setActiveForm('verifyCode')
                }
            }).catch(e=>{
                console.log(e)
            })

    }


    return(
        <div className={`${classes['wrap-form-email-address']} col-lg-6 pl-0`}>
            <div>
                <p className='mb-0'>{t('account.personal-details-text1')}</p>
                <p className='mb-0'>{object!==null && object.email?object.email:''}</p>
            </div>
            <div>
                <p className='mb-0'>{t('account.personal-details-text2')}</p>
                <p className='mb-0'>{t('account.personal-details-text3')}</p>
            </div>
            <Formik
                initialValues={initialVal}
                enableReinitialize
                validationSchema={AddressSchema}
                onSubmit={(values) => {
                    submitForm(values)
                }}
            >
                {({
                      errors,
                      values,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      touched
                  })=>{

                    return(
                        <Form onSubmit={handleSubmit} className={classes['form-edit-personal']}>
                            <div className='position-relative'>
                                <div className={`${classes['overlay-preloader']} ${loading ? classes['load']: ''}`}>
                                    <div>
                                        <Preloader />
                                    </div>
                                </div>
                                <div>

                                    <div className={`pt-3 pb-3`}>
                                        <p className='mb-0'><label className='mb-0'>{t('account.personal-details-text1')}</label></p>
                                        <Field name='old_email'
                                               className={`${(errors.old_email && touched.old_email) || apiErrors.old_email ? classes['error-field']:''} w-100`}
                                               value={values.old_email}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                        />
                                        <p className={classes['errors-message']}>{apiErrors.old_email}</p>
                                    </div>
                                    <div className={`pt-3 pb-3`}>
                                        <p className='mb-0'><label className='mb-0'>{t('account.personal-details-input')}</label></p>
                                        <Field name='email'
                                               className={`${(errors.email && touched.email) || apiErrors.email ? classes['error-field']:''} w-100`}
                                               value={values.email}
                                               onBlur={handleBlur}
                                               onChange={handleChange}
                                        />
                                        <p className={classes['errors-message']}>{apiErrors.email}</p>
                                    </div>

                                    <div className={`pt-3 pb-3`}>
                                        <p className='mb-0'><label className='mb-0'>{t('identity.form-input2')}</label></p>

                                        <p className='position-relative'>
                                            <Field name='password'
                                                   type={passwordType?'password':'text'}
                                                   className={`${(errors.password && touched.password) || apiErrors.password  ? classes['error-field']:''} w-100`}
                                                   value={values.password}
                                                   onBlur={handleBlur}
                                                   onChange={handleChange}
                                            />
                                            <button className='position-absolute' onClick={()=>setPasswordType(!passwordType)}>{
                                                passwordType?'show':'hide'
                                            }</button>
                                        </p>
                                        <p className={classes['errors-message']}>{apiErrors.password}</p>
                                        <small>{t('account.personal-details-input2')}</small>
                                    </div>
                                </div>

                                <div className='mt-3'>
                                    <div className='pl-3 pr-3 pt-1 pb-1 row align-items-center justify-content-end'>
                                        <button className={classes['button-cancel']} onClick={()=> {
                                            setActiveForm(null)
                                            resetErr()
                                        }}>{t('account.address-input-button')}</button>
                                        <button type='submit' className={`${classes['button-save']} mr-0`}>{t('account.address-input-button1')}</button>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                }
            </Formik>
        </div>
    )
}
