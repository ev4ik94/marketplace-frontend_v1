import {useTranslation} from "react-i18next";
import {useState, useEffect} from "react";
import * as Yup from "yup";
import {Field, Form, Formik} from "formik";

/*---Styles-----*/
import classes from "../../../../styles/pages-components/account/account-slug.module.sass";

/*---Hooks------*/
import {AxiosApi} from "../../../../hooks/axios.hook"
import {Preloader} from "../../../preloader/Preloader"


/*-----Functions-----*/
import {checkErrors} from "../../../secondary-func"

/*-----Interface-------*/
interface IPerson{
    email: string,
    old_email: string
}


export function VerifyCode({
                               object,
                               setIsSuccess,
                               setErrors,
                               saveChange,
                               setActiveForm,
                               setAllerSuccess
}:{
    object: IPerson,
    setIsSuccess: (value: any)=>void,
    setErrors: (value: any)=>void,
    saveChange: (value: any)=>void,
    setActiveForm: (value: any)=>void,
    setAllerSuccess: (value: any)=>void
}){
    const {t} = useTranslation()
    const {request, loading, error} = AxiosApi()

    useEffect(()=>{
        if(error!==null){
            setIsSuccess(false)
            if(typeof error === 'string'){
                setErrors(error)
            }else{
                setErrors(checkErrors(error))
            }
        }
    }, [error])

    const AddressSchema = Yup.object().shape({
        code: Yup.number().required('Required'),

    })

    const initialVal = {
        code: '',
    }

    const submitForm = async (values)=>{
        verifyEmail(values)
    }

    const verifyEmail = async(data)=>{
        const dataObj = {
            email: object.email,
            old_email: object.old_email,
            code: data.code
        }
        await request('https://api.dev.farq.uz/ru/v1.2/profile/verify_new_email', 'POST', dataObj)
            .then(result=>{
                saveChange(dataObj)
                setActiveForm(null)
            }).catch(e=>{})

    }

    return(
        <Formik
            initialValues={initialVal}
            enableReinitialize
            validationSchema={AddressSchema}
            onSubmit={(values) => {
                submitForm(values)
            }}
        >
            {({
                errors,
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                touched
            })=>{
                return (
                    <Form onSubmit={handleSubmit} className={classes['form-edit-personal']}>
                        <div className=''>
                            <p>{t('account.personal-details-verify_code.tx2')}</p>
                            <div className='position-relative'>
                                <div className={`${classes['overlay-preloader']} ${loading ? classes['load']: ''}`}>
                                    <div>
                                        <Preloader />
                                    </div>
                                </div>
                                <div className={`pt-3 pb-3`}>
                                    <p className='mb-0'><label className='mb-0'>{t('account.personal-details-verify_code.tx1')}</label></p>
                                    <Field name='code'
                                           className={`${errors.code && touched.code ? classes['error-field']:''} w-100 col-lg-6`}
                                           value={values.code}
                                           onBlur={handleBlur}
                                           onChange={handleChange}
                                    />
                                </div>
                            </div>

                            <div className='mt-3'>
                                <div className='pl-3 pr-3 pt-1 pb-1 row align-items-center justify-content-end'>
                                    <button className={classes['button-cancel']} onClick={()=>setActiveForm(null)}>
                                        {t('account.address-input-button')}</button>
                                    <button type='submit' className={`${classes['button-save']} mr-0`}>{t('account.address-input-button1')}</button>
                                </div>
                            </div>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}
