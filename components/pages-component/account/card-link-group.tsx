import Link from 'next/link'
import {useRouter} from "next/router"
import {useTranslation} from "react-i18next";

/*---Icons---*/
import {Card} from '../../icons/Card'
import {AddressBook} from "../../icons/Address-book"
import {TimeClock} from '../../icons/time-oclock'
import {PersonCircle} from 'react-bootstrap-icons'

/*---Bootstrap---*/
import {Container} from "react-bootstrap"


/*---VirtDb----*/
import {link} from "../../../virtDb/arrLinksAccount"

/*---styles---*/
import classes from '../../../styles/pages-components/account/card-group.module.sass'

export function CardLinkGroup({logout}:{logout:()=>void}){

    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router

    const HTMLIcons = (icon)=>{
        switch(icon){
            case 'TimeClock':
                return <TimeClock />
            case 'Card':
                return <Card />
            case 'AddressBook':
                return <AddressBook />
            case 'PersonCircle':
                return <PersonCircle />
            default:
                return ''
        }
    }
    return(
        <Container fluid>
            <div className={`${classes['card-group-wrap']} d-flex flex-wrap justify-content-start mt-3 mb-3`}>
                {
                    link.map(item=>{
                        return(
                            <div className={`col-lg-4`} key={item.id}>
                                <Link href={`/account/${item.slug}`} locale={locale}>
                                    <a>
                                        <div>
                                            <div className={`${classes['title-card']}`}>
                                                {HTMLIcons(item.icon)}
                                                <span className='mb-0'>{item.name['ru']}</span>
                                            </div>
                                            <div className={`${classes['description-card']}`}>
                                                <p className='mb-0'>{item.description}</p>
                                            </div>
                                            <div className={`${classes['link-card']}`}>
                                                <span>{item.link_name}</span>
                                            </div>
                                        </div>
                                    </a>
                                </Link>
                            </div>
                        )
                    })
                }
            </div>
            <button onClick={logout} className={`${classes['sign-out-button']} d-lg-none d-block col-12`}>{t('account.sign-out')}</button>
        </Container>
    )
}
