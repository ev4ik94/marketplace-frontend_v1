import {useRouter} from "next/router"
import classes from "../../../styles/pages-components/products/products.module.sass";
import {ChevronDown, ChevronRight} from "react-bootstrap-icons";
import Link from "next/link";
import React, {useState} from "react";


export function FilterProducts({filters, selectAttribute, stateShow, setStateShow}){

    const router = useRouter()
    const {locale} = router
    const category_slug = router.query.category_slug



    const [rangePrice, setRangePrice] = useState({
        min: '',
        max: ''
    })


    const renderFilters = (data)=>{
        const arrFilt = []
        for(let value in data){
            arrFilt.push({
                type: value,
                data: data[value]
            })
        }
        return arrFilt.map((item, index)=>{
            if(item.type==='attributes') {
                return (<div key={index}>{renderAttributes(item.data)}</div>)
            }
            else if(item.type==='brands'){
                return (<div key={index}>{renderBrands(item.data)}</div>)
            }else if(item.type==='categories'){
                return (<ul className={classes['filter-categories']} key={index}>{renderCategories(item.data, data['append_categories'])}</ul>)
            }
            return ('')
        })
    }



    const renderBrands = (brands)=>{
        const filterBlock = document.getElementById('filter-container') as HTMLElement
        const positionTop = filterBlock!==null?filterBlock.offsetTop:0
        let activeDrop = false


        return(
            <div className={`${classes['filter_value_groups']} p-4 border-top  ${brands.length>6?classes['dropdown-filters']:''} position-relative`}>
                <p className='font-weight-bold'>Бренды</p>
                {brands.length>6?(
                    <button className={classes['show_more']} onClick={(e)=>{
                        const containerFilter = e.currentTarget.parentElement as HTMLElement
                        const isActive = containerFilter.classList.contains(classes['active_block'])
                        const activeElement = containerFilter.querySelector(`[data-item=filter-item-${0}]`) as HTMLElement
                        const height = activeElement.offsetHeight
                        const filterItemTop = containerFilter.offsetTop

                        if(!isActive){
                            containerFilter.style.height = `${height+130}px`
                            containerFilter.classList.add(classes['active_block'])
                            activeDrop = true
                        }else{
                            containerFilter.style.height = ``
                            containerFilter.classList.remove(classes['active_block'])
                            activeDrop = false
                            filterBlock.scroll({
                                top: filterItemTop-positionTop,
                                behavior: 'smooth'
                            })
                        }
                    }}>
                        {!activeDrop?<ChevronDown />:''}
                    </button>):''}
                <div data-item={`filter-item-0`}>
                    {
                        brands.map((child, index)=>{
                            return(
                                <div className={`${classes['filter_attributes_values']} d-flex mb-2`} key={index}>
                                    <div className={classes['check_box']} onClick={()=>selectAttribute([{name: `brands[${index}]`, value: isActiveFilter(`brands[${index}]`)?null:child.slug}])}>
                                        {isActiveBrand(child.slug)?(<span>✓</span>):''}
                                    </div>
                                    <p className='ml-2'>{child.name}</p>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )

    }

    const renderCategories = (categories, append_categories)=>{

        return(
            <li className='ml-4'>
                {
                    categories.parent!==null?(<Link href={`/products/${categories.parent.slug}`} locale={locale}>
                        <a>{categories.parent.name}</a>
                    </Link>):''
                }
                <ul className='pl-3'>
                    {
                        category_slug === categories.slug ? (<li>
                            <p className='mb-0 font-weight-bold'>{categories.name}</p>
                        </li>):( <Link href={`/products/${categories.slug}`} locale={locale}>
                            <a>{categories.name}</a>
                        </Link>)
                    }
                    <ul className='pl-3'>
                        {
                            categories.children.map((child, index)=>{
                                if(category_slug === child.slug){
                                    return(
                                        <li key={index}>
                                            <p className='mb-0'>{child.name}</p>
                                        </li>
                                    )
                                }else{
                                    return(
                                        <li key={index}>
                                            <Link href={`/products/${child.slug}`} locale={locale}>
                                                <a>{child.name}</a>
                                            </Link>
                                        </li>
                                    )
                                }
                            })
                        }
                    </ul>
                    {
                        append_categories.map((item, index)=>{
                            return(
                                <p key={index} className='mb-0'>
                                    <Link href={`/products/${item.slug}`} locale={locale}>
                                        <a>{item.name}</a>
                                    </Link>
                                </p>
                            )
                        })
                    }
                </ul>
            </li>
        )

    }

    function renderPrices(dataPrice){
        let filterActive = {}
        const queries = router.query
        if(dataPrice){
            for(let price in queries) {

                if (price.match(/price/g) !== null) {

                    if(price.match(/min/g) !== null){
                        filterActive['min'] = Number(queries[price])
                    }

                    if(price.match(/max/g) !== null){
                        filterActive['max'] = Number(queries[price])
                    }
                }
            }
            return (dataPrice.values || []).map((item, index)=>{
                return(
                    <div key={index}>
                        <div className={`${classes['filter_attributes_values']} d-flex mb-2`} key={index}>
                            <div className={classes['check_box']} onClick={()=>{
                                selectAttribute([
                                    {
                                        name: `price[min]`,
                                        value: item.min
                                    },
                                    {
                                        name: `price[max]`,
                                        value: item.max
                                    }
                                ])
                            }}>
                                {filterActive['min']===item.min&&filterActive['max']===item.max? <span>✓</span>:''}
                            </div>
                            <p className='ml-2'>{item.name}</p>
                        </div>
                    </div>
                )
            })
        }

        return ('')
    }


    /*-------Проверка на существующий фильтр-----------*/

    function isActiveFilter(query_name){
        const routArr = router.query
        let state = false
        for(let val in routArr){
            if(val===query_name){
                state = true
            }
        }
        return state
    }

    const isActiveBrand = (slug)=>{
        const routArr = router.query
        let state = false
        for(let val in routArr){
            if(routArr[val]===slug){
                state = true
            }
        }

        return state
    }

    const renderAttributes = (attributes)=>{
        const filterBlock = document.getElementById('filter-container') as HTMLElement
        const positionTop = filterBlock!==null?filterBlock.offsetTop:0

        return (attributes || []).map((item, index)=>{
            let activeDrop = false
            return(
                <div className={`${classes['filter_value_groups']} p-4 border-top ${item.children.length>6?classes['dropdown-filters']:''} position-relative`} key={index}>
                    <p>{item.name}</p>
                    {item.children.length>6?(
                        <button className={classes['show_more']} onClick={(e)=>{
                            const containerFilter = e.currentTarget.parentElement as HTMLElement
                            const isActive = containerFilter.classList.contains(classes['active_block'])
                            const itemActive = containerFilter.querySelector(`[data-item=filter-item-${index}]`) as HTMLElement
                            const height = itemActive.offsetHeight
                            const filterItemTop = containerFilter.offsetTop

                            if(!isActive){
                                containerFilter.style.height = `${height+130}px`
                                containerFilter.classList.add(classes['active_block'])
                                activeDrop = true
                            }else{
                                containerFilter.style.height = ``
                                containerFilter.classList.remove(classes['active_block'])
                                activeDrop = false
                                filterBlock.scroll({
                                    top: filterItemTop-positionTop,
                                    behavior: 'smooth'
                                })
                            }
                        }}>
                            {!activeDrop?<ChevronDown />:'true'}
                        </button>):''}
                    <div data-item={`filter-item-${index}`}>
                        {
                            item.children.map((child, index)=>{

                                return(
                                    <div className={`${classes['filter_attributes_values']} d-flex mb-2`} key={index}>
                                        <div className={classes['check_box']} onClick={()=>{
                                            selectAttribute([{name: `attributes[${item.id}][${index}]`,
                                                value: isActiveFilter(`attributes[${item.id}][${index}]`)?null:child.id}])
                                        }}>
                                            {isActiveFilter(`attributes[${item.id}][${index}]`)? <span>✓</span>:''}
                                        </div>
                                        <p className='ml-2'>{child.name}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            )
        })
    }

    return(
        <div className={`${classes['filters-products']} px-0 ${stateShow?classes['active']:''} col-lg-3 col-md-4 col-sm-5 col-9`} id='filter-container'>
            <div className={`${classes['sticky-container']}`}>
                {renderFilters(filters)}
                <div className={`${classes['filter_value_groups']} p-4 border-top`}>
                    <p className='font-weight-bold'>Цена</p>

                    <div>
                        <div className={`${classes['range_input_price']} d-flex mb-3 position-relative`}>
                            <form action="" className='d-flex' onSubmit={(e)=>{
                                e.preventDefault()
                                selectAttribute([
                                    {
                                        name: 'price[min]',
                                        value: rangePrice.min
                                    },
                                    {
                                        name: 'price[max]',
                                        value: rangePrice.max
                                    }
                                ])
                            }}>
                                <div className={`pl-0`}>
                                    <input type="text" value={rangePrice.min} placeholder='min'
                                           onChange={(e)=>{
                                        if(e.target.value.match(/[Aa-zZ]/g)===null){
                                            setRangePrice({...rangePrice, min: e.target.value})
                                        }

                                    }}/>

                                </div>
                                <span>to</span>
                                <div className={`pl-0`}>
                                    <input type="text" value={rangePrice.max} placeholder='max'
                                           onChange={(e)=>{
                                        if(e.target.value.match(/[Aa-zZ]/g)===null){
                                            setRangePrice({...rangePrice, max: e.target.value})
                                        }
                                    }}/>

                                </div>
                                <button className='position-absolute' type='submit' onSubmit={(e)=>{
                                    e.preventDefault()
                                }}>
                                    <ChevronRight />
                                </button>
                            </form>
                        </div>
                        {renderPrices(filters['price'])}
                    </div>
                </div>
            </div>
        </div>
    )
}
