import InputMask from "react-input-mask"


export const MaskInput = ({field, form, className,  ...props})=>{

    return(
        <InputMask
            mask = '99 999 99 99'
            name="phone"
            className={className}
            maskChar={null}
            size="16"
            alwaysShowMask = {true}
            {...props}
        />
    )
}
