import {useTranslation} from "react-i18next";
import {useRouter} from 'next/router'
import {useState, useEffect} from "react";
import * as Yup from "yup";
import {Field, Form, Formik} from "formik";

/*-----Styles-----*/
import classes from "../../../../styles/pages-components/identity/forms.module.sass";


/*----Hooks---*/
import {AxiosApi} from "../../../../hooks/axios.hook"
import {checkErrors} from "../../../secondary-func";


interface IErrors{
    field: string,
    message: string
}

export function ForgotPassword({setSuccessSubmit, errors, setErrors, setAlertSuccess, apiErrors, setApiErrors}:
                                   {
                                       setSuccessSubmit: (value:boolean)=>void,
                                       errors: IErrors[],
                                       setErrors: (value: IErrors[])=>void,
                                       setAlertSuccess: (value: string)=>void,
                                       apiErrors: any,
                                       setApiErrors: (value:any)=>void
                                   }){

    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router
    const [getToken, setToken] = useState('')
    const [formsState, setFormsState] = useState({
        emailForm: true,
        codeForm: false,
        passwordForm: false
    })

    /*------Hooks---*/
    const {request, loading, error} = AxiosApi()

    const SignInSchema = Yup.object().shape({
        email: Yup.string().email('Invalid email').required(t('input.error-message1')),
        code: Yup.string()
            .min(8, 'Too Short!')
            .max(50, 'Too Long!')
            .required(t('input.error-message')),
        password: Yup.string().required('Required'),
        password_confirmation: Yup.string()
            .test('password', t('input.error-message-equal-password"'), function(value:string) {
                return value === this.resolve(Yup.ref('password'))
            }).required('Required')

    })

    /*----UseEffects----*/

    useEffect(()=>{
        if(error!==null){
            if(error.errors){
                setApiErrors(checkErrors(error.errors))
            }
            setErrors([{field: '', message: error.message}])
        }else{
            setErrors([])
            setApiErrors({})
        }
    }, [error])

    /*------Request API-----*/

    const submitForm = async(values)=>{
        const data = {
            email: values.email,
            token: getToken,
            password: values.password,
            password_confirmation: values.password_confirmation
        }
        await request (`${process.env.PASSWORD_FORGOT_RESET[locale]}`, 'POST', data)
            .then(result=>{
                setAlertSuccess(result.data.message)
                setErrors([])
                setApiErrors([{}])
                setSuccessSubmit(true)
                setTimeout(()=>router.push('/identity/signin', undefined, {locale}), 1000)
            }).catch(e=>console.log(e))
    }

    const sendEmail = async(email)=>{
        await request(`${process.env.PASSWORD_FORGOT_EMAIL[locale]}`, 'POST', {email})
            .then(()=>{
                setFormsState({...formsState, emailForm: false, codeForm: true})
                setErrors([])
                setApiErrors([{}])
            }).catch(e=>console.log(e))

    }

    const sendCode = async(email, code)=>{
        await request(`${process.env.PASSWORD_FORGOT_CODE[locale]}`, 'POST', {email, code})
            .then(result=>{
                setToken(result.data.token)
                setFormsState({...formsState, codeForm: false, passwordForm: true})
                setErrors([])
                setApiErrors([{}])
            }).catch(e=>console.log(e))
    }



    return(
        <Formik initialValues={{
            email: '',
            code: '',
            password: '',
            password_confirmation: ''

        }}
                validationSchema={SignInSchema}
                onSubmit={(values) => {
                    submitForm(values)
                }}>
            {({
                  errors,
                  values,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  touched
              })=>{

                return(

                    <Form className={`pt-3 col-lg-6 mx-auto ${classes['form-wrap']}`} onSubmit={handleSubmit}>


                        <div className={`${classes['inputs-group-forgot-pass']} border-bottom-0`}>

                            {
                                formsState.emailForm ? (
                                    <>
                                    <h3 className={`text-center pb-3 pt-3`}>{t('identity.form-forgot-password-1')}</h3>
                                    <div className={`${classes['group-forms']} position-relative`}>
                                    <label className={`${classes['label-f']} ${(errors.email && touched.email) || apiErrors.email ? classes['error-label']:''}`}>{t('identity.form-input1')}</label>
                                    <Field name='email'
                                           required
                                           className={`${errors.email && touched.email ? classes['error-field']:''} ${classes['input-f']}`}
                                           value={values.email}
                                           onBlur={(e)=>{
                                               handleBlur(e)
                                               if(!values.email.length){
                                                   e.currentTarget.parentElement.classList.remove(classes['input-focus'])
                                               }
                                           }}
                                           onChange={(e)=>{
                                               handleChange(e)
                                               e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                               setApiErrors([{}])
                                           }}
                                           onFocus={(e)=>{
                                               e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                           }}

                                    />
                                    <div className={`${(errors.email && touched.email) || apiErrors.email?'d-block':'d-none'} ${classes['errors-message']}`}>
                                        <p className='mb-0'>{errors.email || apiErrors.email}</p>
                                    </div>
                                </div></>):''
                            }

                            {
                                formsState.codeForm ? (<>
                                    <h3 className={`text-center pb-3 pt-3`}>{t('identity.form-forgot-password-2')}</h3>
                                    <div className={`${classes['group-forms']} position-relative`}>
                                    <p>{t('account.personal-details-code')}</p>
                                    <Field name='code'
                                           required
                                           className={`${(errors.code && touched.code) || apiErrors.code ? classes['error-field']:''} ${classes['input-f']}`}
                                           value={values.code}
                                           onBlur={(e)=>{
                                               handleBlur(e)
                                               if(!values.code.length){
                                                   e.currentTarget.parentElement.classList.remove(classes['input-focus'])
                                               }
                                           }}
                                           onChange={(e)=>{
                                               handleChange(e)
                                               e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                               setApiErrors([{}])
                                           }}
                                           onFocus={(e)=>{
                                               e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                           }}

                                    />
                                    <div className={`${(errors.code && touched.code) || apiErrors.code?'d-block':'d-none'} ${classes['errors-message']}`}>
                                        <p className='mb-0'>{errors.code||apiErrors.code}</p>
                                    </div>
                                </div></>):''
                            }

                            {
                                formsState.passwordForm ? (<div>
                                    <h3 className={`text-center pb-3 pt-3`}>{t('identity.form-forgot-password-3')}</h3>
                                    <div className={`${classes['group-forms']} position-relative`}>
                                        <label className={`${classes['label-f']} ${(errors.password && touched.password) || apiErrors.password ? classes['error-label']:''}`}>{t('account.personal-details-input4')}</label>
                                        <Field name='password'
                                               type="password"
                                               required
                                               className={`${errors.password && touched.password ? classes['error-field']:''} ${classes['input-f']}`}
                                               value={values.password}
                                               onBlur={(e)=>{
                                                   handleBlur(e)
                                                   if(!values.password.length){
                                                       e.currentTarget.parentElement.classList.remove(classes['input-focus'])
                                                   }
                                               }}
                                               onChange={(e)=>{
                                                   handleChange(e)
                                                   e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                                   setApiErrors([{}])
                                               }}
                                               onFocus={(e)=>{
                                                   e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                               }}

                                        />
                                        <div className={`${(errors.password && touched.password) || apiErrors.password?'d-block':'d-none'} ${classes['errors-message']}`}>
                                            <p className='mb-0'>{errors.password||apiErrors.password}</p>
                                        </div>
                                    </div>

                                    <div className={`${classes['group-forms']} position-relative`}>
                                        <label className={`${classes['label-f']} ${(errors.password_confirmation && touched.password_confirmation) || apiErrors.password_confirmation ? classes['error-label']:''}`}>{t('identity.form-signup-label3')}</label>
                                        <Field name='password_confirmation'
                                               required
                                               type='password'
                                               className={`${errors.password_confirmation && touched.password_confirmation ? classes['error-field']:''} ${classes['input-f']}`}
                                               value={values.password_confirmation}
                                               onBlur={(e)=>{
                                                   handleBlur(e)
                                                   if(!values.password_confirmation.length){
                                                       e.currentTarget.parentElement.classList.remove(classes['input-focus'])
                                                   }
                                               }}
                                               onChange={(e)=>{
                                                   handleChange(e)
                                                   e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                                   setApiErrors([{}])
                                               }}
                                               onFocus={(e)=>{
                                                   e.currentTarget.parentElement.classList.add(classes['input-focus'])
                                               }}

                                        />
                                        <div className={`${(errors.password_confirmation && touched.password_confirmation)||apiErrors.password_confirmation?'d-block':'d-none'} ${classes['errors-message']}`}>
                                            <p className='mb-0'>{errors.password_confirmation||apiErrors.password_confirmation}</p>
                                        </div>
                                    </div>
                                </div>):''
                            }
                        </div>
                        <div>
                            <button className={classes['submit-button']} onClick={(e)=>{
                                e.preventDefault()
                                if(formsState.emailForm && values.email.length){
                                    if(!loading){
                                        sendEmail(values.email)
                                    }
                                }else if(formsState.codeForm && values.code.length){
                                    if(!loading){
                                        sendCode(values.email, values.code)
                                    }
                                }else{
                                    if(!errors.password && !errors.password_confirmation){
                                        if(!loading){
                                            submitForm(values)
                                        }
                                    }
                                }
                            }} disabled={loading}>
                                Submit
                            </button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
}
