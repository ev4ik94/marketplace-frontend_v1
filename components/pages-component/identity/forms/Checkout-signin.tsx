import Link from 'next/link'
import {useTranslation} from "react-i18next"

/*---components---*/
import {SignIn} from "./SignIn"


/*---Styles---*/
import classes from '../../../../styles/pages-components/identity/checkout-signin.module.sass'



/*---Interfaces----*/
interface IErrors{
    field: string,
    message: string
}

export function CheckoutSignIn({setSuccessSubmit, errors, setErrors, setAlertSuccess, apiErrors, setApiErrors}:
                                   {
                                       setSuccessSubmit: (value:boolean)=>void,
                                       errors: IErrors[],
                                       setErrors: (value: IErrors[])=>void,
                                       setAlertSuccess: (value:string)=>void,
                                       apiErrors: any,
                                       setApiErrors: (value:any)=>void
                                   }){
    const {t} = useTranslation()


    return(
        <div className={`d-flex justify-content-around flex-lg-nowrap flex-wrap flex-lg-row flex-column-reverse`}>
            <SignIn
                setSuccessSubmit={setSuccessSubmit}
                errors={errors}
                setErrors={setErrors}
                setAlertSuccess={setAlertSuccess}
                apiErrors={apiErrors}
                setApiErrors={setApiErrors}
            />
            <div className={`${classes['vertical-line']} position-relative d-lg-block d-none`} data-title={t('word.or')}>
                <div className=''/>
            </div>
            <ContinueCheckout />
        </div>
    )
}

function ContinueCheckout(){
    return(
        <div className={`${classes['wrap-continue']} mb-lg-0 mb-3`}>
            <h3 className='pt-3 pb-3'>Новый пользователь</h3>
            <p>Don't have an account? No problem, you can check out as a guest. You'll have the option to create an account during checkout.</p>
            <Link href={`/checkout`}>
                <a>Продолжить как гость</a>
            </Link>
        </div>
    )
}
