import Link from 'next/link'
import {useRouter} from "next/router"

/*-----Styles----*/
import classes from '../../../styles/pages-components/help/side-bar.module.sass'

export function SideBarCategories({categories}){

    const router = useRouter()
    const {locale} = router

    const renderCategories = (arrCat)=>{

        return arrCat.map((item, index)=>{

            return(
                <>
                    {
                        item.children.length ? (<li key={index} className={`${router.query.slug===item.slug?classes['active']:''}`}>
                            <Link href={`/help/${item.slug}`} locale={locale}>
                                <a>{item.title}</a>
                            </Link>
                            <ul>{renderCategories(item.children)}</ul>
                        </li>) : (<li key={index} className={`${router.query.slug===item.slug?classes['active']:''}`}>
                            <Link href={`/help/${item.slug}`} locale={locale}>
                                <a className='pl-3'>{item.title}</a>
                            </Link>
                        </li>)
                    }
                </>
            )
        })
    }

    return(
        <div className={`col-lg-3 ${classes['wrap-side-bar']}`}>
            <ul className={`position-sticky mt-3 ${classes['categories-list']}`} style={{top: '15px'}}>{renderCategories(categories)}</ul>
        </div>
    )
}
