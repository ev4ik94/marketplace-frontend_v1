import {useTranslation} from "react-i18next"
import Link from 'next/link'
import {useRouter} from "next/router"


/*----Styles----*/
import classes from '../../../styles/pages-components/help/feedBack-help.module.sass'

/*----Icons-----*/
import {TelephoneFill, EnvelopeOpen, ChatLeftText} from "react-bootstrap-icons"

export function FeedBackHelp(){
    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router
    return(
        <div>
            <h1 className='font-weight-bold'>{t('home.feedBack-title')}</h1>
            <div className={`${classes['feed-back-items']} d-flex mt-3`}>
                <Link href={'/'} locale={locale}>
                    <a className='col-lg-4 pl-0'>
                        <div className={`${classes['item-feed-back']} h-100`}>
                            <div className={'text-center'}>
                                <TelephoneFill />
                                <p className='mb-0 font-weight-bold mt-1'>Call</p>
                            </div>
                            <p className='mb-0 mt-3 text-center'>{t('home.feedBack-text1')}</p>
                        </div>
                    </a>
                </Link>
                <Link href={'/'} locale={locale}>
                    <a className='col-lg-4'>
                        <div className={`${classes['item-feed-back']} h-100`}>
                            <div className={'text-center'}>
                                <EnvelopeOpen />
                                <p className='mb-0 font-weight-bold mt-1'>Email</p>
                            </div>
                            <p className='mb-0 mt-3 text-center'>{t('home.feedBack-text2')}</p>
                        </div>
                    </a>
                </Link>
                <Link href={'/'} locale={locale}>
                    <a className='col-lg-4'>
                        <div className={`${classes['item-feed-back']} h-100`}>
                            <div className={'text-center'}>
                                <ChatLeftText />
                                <p className='mb-0 font-weight-bold mt-1'>Chat</p>
                            </div>
                            <p className='mb-0 mt-3 text-center'>{t('home.feedBack-text3')}</p>
                        </div>
                    </a>
                </Link>
            </div>
        </div>
    )
}
