import Slider from "react-slick"
import {useRef, useState, useEffect} from 'react'
import Link from "next/link";
import {useRouter} from "next/router"


/*----Styles---*/
import classes from '../../../styles/pages-components/home/carousel-banner.module.sass'


/*---Bootstrap-icons----*/
import {PauseCircle, PlayCircle, RecordFill} from "react-bootstrap-icons"
import {PauseTransparent} from "../../icons/Pause-transparent"

/*----Interface---*/
interface IBanner{
    id: number,
    src: string,
    title: string,
    text1: string,
    text2: string,
    button: string | null
}

export default function CarouselBanner({banner}:{banner:IBanner[]}){

    const g = useRef(null)
    const router = useRouter()
    const {locale} = router
    const [stateBtnPause, setState] = useState(true)
    const bannerButton = useRef(null)
    const [windowWidth, setWindow] = useState(0)

    const [currentSlide, setCurrentSlide] = useState(0)

    useEffect(()=>{
        if(typeof window !=='undefined'){
            setWindow(window.innerWidth)
            window.onresize = ()=>setWindow(window.innerWidth)
        }
    }, [])

    const settings = {
        dots: true,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: false,
        dotsClass: classes['carousel-controls'],
        appendDots: dots => (
            <div>
                <ul style={{ margin: "0px" }}> {dots} </ul>
            </div>
        ),
        customPaging: i => {

            if(windowWidth>=992){
                return(
                    <>
                        {
                            i!==currentSlide?(
                                <button onClick={()=>{
                                    goToSlide(i)
                                    if(!stateBtnPause){
                                        playSlide()
                                    }
                                }}><RecordFill /></button>
                            ):(<>
                                {
                                    stateBtnPause?<button onClick={()=>pauseSlide()} className={classes['banner-pause-play']} ref={bannerButton}>
                                        <PauseTransparent />
                                    </button>:<button onClick={()=>playSlide()} className={classes['banner-pause-play']} ref={bannerButton}><PlayCircle /></button>
                                }
                            </>)
                        }
                    </>

                )
            }

            return(
                <button className={`${classes['']}`}><RecordFill /></button>
            )
        },
        afterChange: (e)=>{
            setCurrentSlide(e)
        }

    }

    useEffect(()=>{
        setState(g.current.props.autoplay)
    }, [])



    const pauseSlide = ()=>{
        g.current.slickPause()
        setState(false)
    }
    const playSlide = ()=>{
        setState(true)
        g.current.slickPlay()
    }

    const goToSlide = (index)=>{
        g.current.slickGoTo(index)
        setCurrentSlide(index)
    }





    return(
        <div className={`${classes['carousel-wrap']} col-lg-6 col-12 pl-0 pr-lg-3 pr-0`} id='carousel-home'>

        <Slider {...settings} ref={g} className={classes['slick-container']}>
            {
                banner.map(item=>{
                    return(
                        <div key={item.id} className={`position-relative h-100`}>
                            <div className={classes['image-banner-home']}>
                                <img src={item.src} alt="" />
                            </div>
                            <div className={`${classes['banner_text']}`}>
                                <h1 className='font-weight-bold'>{item.title}</h1>
                                <p className='font-weight-bold mb-1'>{item.text1}</p>
                                <p className={`mb-1 ${classes['banner-description-text']}`}>{item.text2}</p>
                                {
                                    item.button!==null?(<Link href={'/'} locale={locale}>
                                        <a>{item.button}</a>
                                    </Link>): ''
                                }
                            </div>
                        </div>
                    )
                })
            }

        </Slider>
        </div>
    )
}
