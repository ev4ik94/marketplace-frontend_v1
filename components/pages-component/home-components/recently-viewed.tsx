import { useTranslation } from 'react-i18next'
import Link from 'next/link'
import {useRouter} from "next/router"
import Slider from "react-slick"

/*---Bootstrap-components---*/
import {Container} from "react-bootstrap"

/*---Components---*/
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"

/*---Icons---*/
import {TimeClock} from '../../icons/time-oclock'
import {ArrowRight} from 'react-bootstrap-icons'

/*---Styles----*/
import classes from '../../../styles/pages-components/home/recently-viewed.module.sass'
import {CostReplace} from "../../secondary-func";

/*---Interface----*/

interface IRecently{
    id: number,
    brand: {},
    category?: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

export function RecentlyViewed({recently}:{recently:IRecently[]}){

    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    arrows: false,
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]

    }

    return(
        <Container fluid className={`${classes['recently-wrap']}`}>
            <div className={'pl-0 d-lg-flex d-none'}>
                <div className={`col-lg-2 d-flex justify-content-center flex-column`}>
                    <div className={'d-flex'}>
                        <div className={`d-flex flex-column justify-content-center`}>
                            <TimeClock />
                        </div>
                        <div className='d-lg-block d-flex justify-content-between'>
                            <p className={`position-relative pl-3`}>{t('recently.block-title')}</p>
                            <p className='mb-0 pl-3'>
                                <Link href={'/recently-viewed'} locale={locale}>
                                    <a className={classes['link-recently-all']}>{t('recently.block-sub-title')}</a>
                                </Link>
                            </p>
                        </div>
                    </div>
                </div>
                <div className={`col-lg-10 d-flex`}>
                    <div className='col-lg-11 d-flex'>
                        {
                            (recently || []).map((item, index)=>{
                                if(index<4){
                                    return(
                                        <div className={`${classes['recent-item']} d-flex pl-0 col-lg-3`} key={index}>
                                            <Link href={`/product/${item.warehouse.slug}/${item.slug}`}>
                                                <a className='d-flex'>
                                                    <div className={`${classes['picture-item']}`}>
                                                        <LazyLoadImage image={{
                                                            src: item.images[0].original_image,
                                                            srcSet: item.images[0].image_srcset,
                                                            alt: item.name
                                                        }}/>
                                                    </div>
                                                </a>
                                            </Link>

                                            <div className={`col-lg-7`}>
                                                <p className='mb-0'>{t('recently.block-text1')}</p>
                                                <div className={'mt-1'}>
                                                    <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                        <a><p className='mb-0'>{item.name}</p></a>
                                                    </Link>
                                                    <div></div>
                                                    <p className={'font-weight-bold mb-0 mt-1'}>{CostReplace(item.price.price + '')} UZS</p>
                                                    {item.price && item.price.old_price?(<p className='font-weight-bold mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            })
                        }
                    </div>

                    {
                        recently.length > 4 ? (<Link href={'/recently-viewed'} locale={locale}>
                            <a className={classes['button-see-more']}>{t('recently-button')}</a>
                        </Link>):''
                    }
                </div>
            </div>

            <div className={`${classes['mobile__recently_block']} d-lg-none d-block pl-0 pr-0`}>
                <div className='d-flex align-items-center justify-content-between flex-wrap'>
                    <p className='mb-0 font-weight-bold'>{t('recently.block-title')}</p>
                    <p className='mb-0'>
                        <Link href={'/recently-viewed'} locale={locale}>
                            <a>{t('recently-button-1')} <span><ArrowRight /></span></a>
                        </Link>
                    </p>
                </div>
                <Slider {...settings} className={classes['mobile__recently_slick-container']}>
                    {
                        (recently || []).map((item, index)=>{
                            if(index<6){
                                return(
                                    <div className={`${classes['recent-item']} d-flex pl-0 col-lg-3`} key={index}>
                                        <Link href={`/product/${item.warehouse.slug}/${item.slug}`}>
                                            <a className='d-flex'>
                                                <div className={`${classes['picture-item']}`}>
                                                    <LazyLoadImage image={{
                                                        src: item.images[0].original_image,
                                                        srcSet: item.images[0].image_srcset,
                                                        alt: item.name
                                                    }}/>
                                                </div>
                                            </a>
                                        </Link>

                                        <div className={`col-lg-7`}>
                                            <p className='mb-0'>{t('recently.block-text1')}</p>
                                            <div className={'mt-1'}>
                                                <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                    <a><p className='mb-0'>{item.name}</p></a>
                                                </Link>
                                                <div></div>
                                                <p className={'font-weight-bold mb-0 mt-1'}>{CostReplace(item.price.price + '')} UZS</p>
                                                {item.price && item.price.old_price?(<p className='font-weight-bold mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                        })
                    }
                </Slider>
            </div>
        </Container>
    )
}


