import Slider from "react-slick"
import Link from 'next/link'
import {useRouter} from "next/router"
import {useRef, useState, useEffect} from 'react'
import { useTranslation } from 'react-i18next'



/*---Styles---*/
import classes from '../../../styles/pages-components/home/carousel-products.module.sass'

/*----Bootstrap-icons---*/
import {ChevronRight, ChevronLeft, Star} from "react-bootstrap-icons"

/*---Components----*/
import {LazyLoadImage} from '../../preloader/Lazy-Load-Image'
import {CostReplace} from "../../secondary-func"


/*---Interface----*/
interface IProducts {
    id: number,
    brand: {},
    category?: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}





export function CarouselProducts({products}:{products:IProducts[]}){

    const carousel = useRef(null)
    const timer = useRef(null)
    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    const [stateButtons, setStateButtons] = useState({
        prevActive: false,
        nextActive: true
    })
    const [time, setTime] = useState('')
    let counter = 0

    var countDownDate = new Date('Sep 5, 2021 15:37:25').getTime();
    var result = '';

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: false,
        arrows: false,
        afterChange: (prev, next)=>{
            setStateButtons({...stateButtons, nextActive: prev!==7, prevActive: prev!==0})
        },
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    }

    const clickNext = ()=>{
        carousel.current.slickNext()
    }

    const clickPrev = ()=>{
        carousel.current.slickPrev()
    }

    useEffect(()=>{
        var x = setInterval(()=>{
            var now = new Date().getTime();
            var distance = countDownDate - now;

            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            setTime(days + "d " + hours + "h "+ minutes + "m " + seconds + "s ")

            if (distance < 0) {
                clearInterval(x);
                setTime('EXPIRED')
            }
        }, 1000)
    }, [])



    return(
        <div className={`col-lg-6 col-12 pl-lg-3 pr-lg-3 pl-0 pr-0 ${classes['wrap-carousel-products']} d-flex flex-column justify-content-between`}>
            <div className={`${classes['top-element-carousel']} position-relative`}>
                <button className={`position-absolute ${stateButtons.prevActive?classes['controls-carousel']:''}`} onClick={clickPrev}>
                    <ChevronLeft />
                </button>
                <div className='col-12 overflow-hidden mx-auto' id='carousel-product'>
                    <h4 className='font-weight-bold mt-3 mb-1'>{t('carousel-products-home.banner-1')}</h4>
                    <p>{t('carousel-products-home.banner-2')}</p>
                    {
                        products.length?(<Slider {...settings} ref={carousel}>
                            {
                                products.map((item, index)=>{

                                    return(
                                        <div key={index}>
                                            <div className={`${classes['top-info']}`}>
                                                <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                    <a>
                                                        <div className={`${classes['picture-product']}`}>
                                                            <LazyLoadImage image={{
                                                                src: item.images[0].original_image,
                                                                srcSet: item.images[0].image_srcset,
                                                                alt: item.name
                                                            }}/>
                                                        </div>
                                                    </a>
                                                </Link>
                                                <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                    <a>
                                                        <p className='mb-0'>{item.name}</p>
                                                    </a>
                                                </Link>
                                            </div>
                                            <div className={`${classes['rating-block']} d-none`}>
                                                <button className={'pr-1'}>
                                                    <Star/>
                                                </button>
                                                <button className={'pr-1'}>
                                                    <Star/>
                                                </button>
                                                <button className={'pr-1'}>
                                                    <Star/>
                                                </button>
                                                <button className={'pr-1'}>
                                                    <Star/>
                                                </button>
                                                <button className={'pr-1'}>
                                                    <Star/>
                                                </button>
                                                <span>(2555)</span>
                                            </div>
                                            <div className={`${classes['bot-info']}`}>
                                                <p className='mb-0'>{CostReplace(item.price.price + '')} сум</p>
                                                {item.price.old_price!==null?(<p className='mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </Slider>):''
                    }

                </div>
                <button className={`position-absolute ${stateButtons.nextActive?classes['controls-carousel']:''}`} onClick={clickNext}>
                    <ChevronRight />
                </button>
            </div>

            <div className={`${classes['bot-element-product']}`}>
                <div className={'d-flex justify-content-between mt-3'}>
                    <div className='d-flex flex-lg-row flex-column'>
                        <p className={`font-weight-bold mb-0`}>
                            <span>Сделка</span> недели
                        </p>

                    </div>
                    <div className={`d-flex ${classes['timer-block-sale']}`}>
                        <p className={'mb-0 mr-2'}>До конца осталось</p>
                        <div style={{width: '150px'}} className='text-center'>{time}</div>
                    </div>
                </div>
                <div className={`d-flex pt-3 flex-wrap flex-lg-nowrap flex-sm-nowrap flex-md-nowrap`}>
                    <div className={`col-lg-4 ${classes['picture-product-bot']}`}>
                        <div>
                            {
                                products[0] && products[0].images[0]?(<LazyLoadImage image={{
                                    src: products[0]?products[0].images[0].original_image:'',
                                    srcSet: products[0]?products[0].images[0].image_srcset:'',
                                    alt: products[0]?products[0].name:''
                                }}/>):''
                            }

                        </div>
                    </div>
                    <div className={`col-lg-8 d-flex flex-column justify-content-center`}>
                        <Link href={`/product/${products[0]?products[0].slug:''}/${products[0]?products[0].warehouse.slug:''}`} locale={locale}>
                            <a>{products[0]?products[0].name:''}</a>
                        </Link>
                        <p className={`font-weight-bold mb-0`}>{products[0]?products[0].price.price:0} UZS</p>
                        {products[0]&&products[0].price.old_price!==null?(<p className='mb-0'>{products[0]?products[0].price.old_price:0} UZS</p>):''}
                    </div>
                </div>
            </div>
        </div>
    )
}


