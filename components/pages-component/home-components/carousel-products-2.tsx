import Slider from "react-slick"
import Link from 'next/link'
import {useRef, useState} from 'react'
import {useRouter} from "next/router"
import { useTranslation } from 'react-i18next'


/*----Bootstrap-icons---*/
import {ChevronRight, ChevronLeft, Star} from "react-bootstrap-icons"
import classes from "../../../styles/pages-components/home/carousel-products.module.sass"


/*---Components---*/
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"
import {CostReplace} from "../../secondary-func"


/*---Interface----*/
interface IProducts {
    id: number,
    brand: {},
    category?: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}



export function CarouselProducts2({products}:{products:IProducts[]}){

    const carousel = useRef(null)
    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router

    const [stateButtons, setStateButtons] = useState({
        prevActive: false,
        nextActive: true
    })

    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        swipe: false,
        arrows: false,
        afterChange: (prev, next)=>{
            setStateButtons({...stateButtons, nextActive: prev!==7, prevActive: prev!==0})
        },
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    swipe: true,
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                }
            }
        ]
    }


    const clickNext = ()=>{
        carousel.current.slickNext()
    }

    const clickPrev = ()=>{
        carousel.current.slickPrev()
    }


    return(
        <div className={`${classes['element-carousel']} position-relative`}>
            <p className='mb-0 font-weight-bold'>{t('carousel-products-home.title')}</p>
            <p>{t('carousel-products-home.subTitle')}</p>
            <button className={`position-absolute ${stateButtons.prevActive?classes['controls-carousel']:''}`} onClick={clickPrev}>
                <ChevronLeft />
            </button>
            <div className='col-lg-11 col-12 overflow-hidden mx-auto' id='carousel-product'>
                {
                    products.length?(<Slider {...settings} ref={carousel}>
                        {
                            products.map((item, index)=>{
                                return(
                                    <div key={index}>
                                        <div className={`${classes['top-info']}`}>
                                            <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                <a>
                                                    <div className={`${classes['picture-product']}`}>
                                                        <LazyLoadImage image={{
                                                            src: item.images[0].original_image,
                                                            srcSet: item.images[0].image_srcset,
                                                            alt: item.name
                                                        }}/>
                                                    </div>
                                                </a>
                                            </Link>
                                            <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                <a>
                                                    <p>{item.name}</p>
                                                </a>
                                            </Link>
                                        </div>

                                        <div className={`${classes['bot-info']}`}>
                                            <p className='mb-0'>{CostReplace(item.price.price + '')} UZS</p>
                                            {item.price.old_price!==null?(<p className='mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                        </div>

                                    </div>
                                )
                            })
                        }
                    </Slider>):''
                }

            </div>
            <button className={`position-absolute ${stateButtons.nextActive?classes['controls-carousel']:''}`} onClick={clickNext}>
                <ChevronRight />
            </button>
        </div>
    )
}
