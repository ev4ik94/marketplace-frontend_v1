import {useState, useEffect} from 'react'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'


/*----Styles----*/
import classes from '../../../styles/pages-components/cart/cart-product.module.sass'


/*---Components---*/
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"


/*---Icons----*/
import {Trash, PlusCircleFill} from "react-bootstrap-icons"
import {MinusCircle} from '../../icons/Minus-circle'

import {CostReplace} from "../../secondary-func";


/*----Interface---*/
interface ICart{
    cart_id: number,
    quantity: number,
    sku: ISku
}

interface ISku{
    brand: string,
    category: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {
        price: number,
        old_price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

export function CartProduct({
                                cart,
                                quantityAdd,
                                quantitySub,
                                deleteProduct,
                                updateCartProduct}:
                                {
                                    cart:ICart[]
                                    quantityAdd: (id:number, quantity: number)=>void,
                                    quantitySub: (id:number, quantity: number)=>void,
                                    deleteProduct: (id:number)=>void,
                                    updateCartProduct: ()=>void
                                }){

const {t} = useTranslation()

    return(
        <div className={`${classes['wrap-cart']} col-lg-11 mx-auto`}>
            {
                cart.map((item, index)=>{

                    return(
                        <div key={index}>
                            <Link href={`/product/${item.sku.warehouse.slug}/${item.sku.slug}`}>
                                <a className='d-lg-none d-block mb-3'>{item.sku.name}</a>
                            </Link>
                            <div  className={`d-flex`}>
                                <div className={`${classes['picture-product']} col-lg-3 col-6`}>
                                    <LazyLoadImage image={{
                                        src: item.sku.images[0].original_image,
                                        srcSet: item.sku.images[0].image_srcset,
                                        alt: item.sku.name
                                    }}/>
                                </div>
                                <div className={`col-lg-7 col-6 d-flex flex-column justify-content-around`}>
                                    <div className={`col-lg-2 d-lg-none d-flex flex-column p-0 mb-3`}>
                                        <p className={`mb-0 mt-1`}>{CostReplace(item.sku.price.price+'')} СУМ</p>
                                    </div>
                                    <Link href={`/product/${item.sku.warehouse.slug}/${item.sku.slug}`}>
                                        <a className='d-lg-block d-none'>{item.sku.name}</a>
                                    </Link>
                                    <div className={`${classes['control-quantity']} d-flex mb-3`} >
                                        <button
                                            className={`${item.quantity>1?classes['active-btn']: classes['disable-btn']}`}
                                            onClick={()=>quantitySub(item.cart_id, item.quantity)}
                                            onMouseLeave={()=>updateCartProduct()}>
                                            <MinusCircle />
                                        </button>
                                        <span>{item.quantity}</span>
                                        <button
                                            onClick={()=>quantityAdd(item.cart_id, item.quantity)}
                                            onMouseLeave={()=>updateCartProduct()}>
                                            <PlusCircleFill />
                                        </button>
                                    </div>
                                    <button className={classes['delete-btn']} onClick={()=>deleteProduct(item.cart_id)}>
                                        <Trash />
                                        <span>{t('cart-page.button-remove')}</span>
                                    </button>
                                </div>
                                <div className={`col-lg-2 d-lg-flex d-none flex-column`}>
                                    <p className={`mb-0 mt-1`}>{CostReplace(item.sku.price.price+'')} СУМ</p>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}
