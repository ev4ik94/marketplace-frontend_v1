import Link from 'next/link'
import {useTranslation} from "react-i18next"
import {useRouter} from "next/router"
import Slider from "react-slick"

/*---Components----*/
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"
import {CostReplace} from "../../secondary-func"
import {Preloader} from "../../preloader/Preloader"

/*---Icons---*/
import {X, ChevronLeft, ChevronRight} from 'react-bootstrap-icons'
import {Save} from "../../icons/Save"

/*---Styles---*/
import classes from '../../../styles/pages-components/compare/compare-list-products.module.sass'
import {useEffect, useState} from "react";
import {setCompare} from "../../../redux/actions/actioncompare";


/*----Hooks---*/
import {AxiosApi} from "../../../hooks/axios.hook"

/*-----Functions-------*/
import {isSave} from "../../secondary-func"
import {Spinner} from "react-bootstrap";

/*---Interfaces----*/
interface ICompares{
    items: IItemsCompare[],
    category_slug: string | null,
    attribute_groups: []
}

interface IItemsCompare{
    id: number,
    brand: {},
    category?: {},
    attribute_groups: [],
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}


const recently = [
    {
        id: 128,
        image_urls :[
            {
                original_image: 'https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6394/6394628_sd.jpg;maxHeight=1000;maxWidth=1000',
                image_srcset: 'https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6394/6394628_sd.jpg;maxHeight=1000;maxWidth=1000'
            }
        ],
        name: 'Recently Prod',
        price: 150000
    }
]


export function CompareListProducts({
                                        compares,
                                        openModal,
                                        setCompare,
                                        slide,
                                        slide2,
                                        nextSlide,
                                        prevSlide,
                                        settings,
                                        saved,
                                        setSaved
}:{
    compares:ICompares,
    saved: IItemsCompare[],
    setSaved: (compare: any)=>void
    openModal:(id: number, state:boolean)=>void,
    setCompare: (compare: any)=>void,
    slide: {},
    slide2: {},
    settings: {},
    nextSlide: ()=>void,
    prevSlide: ()=>void
}){
    const {t} = useTranslation()
    const [count, setCount] = useState(0)
    const [recentlyProd, setRecently] = useState([])
    const [comparesProd, setComparesProd] = useState([])
    const router = useRouter()
    const {locale} = router
    const {request} = AxiosApi()
    const [windowWidth, setWindowWidth] = useState(0)

    var mount = true


    useEffect(()=>{
        if(typeof window !=='undefined'){
            window.onresize = (e)=>{
                setWindowWidth(window.innerWidth)
            }
            setWindowWidth(window.innerWidth)
        }
    }, [])



    useEffect(()=>{
        if(mount){
            if(compares.items){
                setCount(compares.items ? compares.items.length : 0)
                setComparesProd(compares.items)
                mount = false
            }
        }
    }, [compares])



    useEffect(()=>{
        setRecently(recently)
    }, [])

    // const recentlyAdd = ()=>{
    //     if(recentlyProd.length<4){
    //         setRecently([...recentlyProd, recently[0]])
    //     }
    // }
    //
    // const recentlyRemove = (id)=>{
    //     setRecently(recentlyProd.filter((item, index)=>index!==recentlyProd.length-1))
    // }
    //
    // const addCompare = async(warehouse_slug, slug)=>{
    //     if(comparesProd.length<4){
    //         await request(`${process.env.COMPARES_ADD['ru']}/${warehouse_slug}/${slug}`).then(result=>{
    //             setComparesProd(result.data.items)
    //         }).catch(e=>console.log(e.message))
    //     }
    //
    // }

    const removeCompare = async(warehouse_slug, slug, preloader)=>{
        preloader.classList.add('d-flex')

        await request(`${process.env.COMPARES_REMOVE[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                preloader.classList.remove('d-flex')
                 setComparesProd(comparesProd.filter(item=>item.slug!==slug))
                 setCompare(result.data)
                if(!result.data.items.length){
                    router.back()
                }
            }).catch(e=>console.log(''))

    }

    /*======= Save Button =========*/
    function toggleSave (warehouse_slug, slug, elem){
        if(isSave(warehouse_slug, slug, saved)){
            deleteSave(warehouse_slug, slug, elem)
        }else{
            addSave(warehouse_slug, slug, elem)
        }
    }

    /*----------Add to Save------------*/
    const addSave = async(warehouse_slug:string, slug:string, elem)=>{
        const preloader = elem.querySelector('.spinner-border')
        preloader.classList.remove('d-none')
        await request(`${process.env.ADD_SAVED[locale]}/${warehouse_slug}/${slug}`, 'PUT')
            .then(result=>{
                preloader.classList.add('d-none')
                setSaved(result.data.list||[])
            }).catch(e=>{
            })
    }

    /*----------Delete Save------------*/
    const deleteSave = async(warehouse_slug:string, slug:string, elem)=>{
        const preloader = elem.querySelector('.spinner-border')
        preloader.classList.remove('d-none')
        await request(`${process.env.REMOVE_SAVED[locale]}/${warehouse_slug}/${slug}`, 'DELETE')
            .then(result=>{
                preloader.classList.add('d-none')
                setSaved(result.data.list||[])
            }).catch(e=>{
            })
    }

    if(!compares.items.length){
        return <div><h1>{t('compare-page.empty')}</h1></div>
    }



    return(
        <div className={`overflow-hidden ${classes['block-sticky']}`}>
            {
                compares.items.length>=3&&windowWidth<=992?(<> <button className={`${classes['btn-controls-carousel']} ${classes['btn-left']} d-lg-block d-md-block d-none`}
                                                    onClick={()=>prevSlide()} id='btn-arrow-left'>
                    <ChevronLeft />
                </button>
                    <button className={`${classes['btn-controls-carousel']} ${classes['btn-right']} d-lg-block d-md-block d-none`}
                            onClick={()=>nextSlide()} id='btn-arrow-right'>
                        <ChevronRight />
                    </button></>):''
            }
            <div className={`${classes['compare-list-carousel']}`}
                 id='compare-list-block'>
                {
                    compares.items.length>=3?(<Slider {...settings} ref={slide}>
                        {
                            (comparesProd || []).map((item, index)=>{

                                return(
                                    <div key={index} className={`${classes['item-compare-slide']} position-relative`} data-index={index}>
                                        <div className={classes['preloader-compare']}><Preloader /></div>
                                        <button onClick={(e)=>{
                                            const parent = document.querySelector("div[data-index='"+index+"']")
                                            const preloader = parent.querySelector(`.${classes['preloader-compare']}`)
                                            removeCompare(item.warehouse.slug, item.slug, preloader)
                                        }} className={`${classes['delete-compare']}`}><X /></button>

                                        <div className={`${classes['compare-image']} position-relative`}>
                                            
                                            <div className={`${classes['picture-block']} align-items-center`}>
                                                <div>
                                                    <LazyLoadImage image={{
                                                        src: item.images[0].original_image,
                                                        srcSet: item.images[0].image_srcset,
                                                        alt: ''
                                                    }}/>
                                                </div>
                                                <p className='mb-lg-3 mb-0'>
                                                    <Link href={`/product/${item.slug}/${item.warehouse.slug}`} locale={locale}>
                                                        <a title={item.name}>{item.name}</a>
                                                    </Link>
                                                </p>
                                                <button onClick={(e)=>{
                                                    const parent = document.querySelector("div[data-index='"+index+"']")
                                                    const preloader = parent.querySelector(`.${classes['preloader-compare']}`)
                                                    removeCompare(item.warehouse.slug, item.slug, preloader)
                                                }} className={`${classes['delete-compare']}`}><X /></button>
                                            </div>
                                            <button onClick={()=>openModal(item.sku_id, true)}>{t('compare-page.products-list-button1')}</button>
                                        </div>

                                        <div className={`${classes['description-compare']}`}>
                                            <p>
                                                <Link href={`/product/${item.slug}/${item.warehouse.slug}`} locale={locale}>
                                                    <a title={item.name}>{item.name}</a>
                                                </Link>
                                            </p>

                                            <p className='mb-0'>
                                                <span className='font-weight-bold'>{t('compare-page.products-list-model')}:</span>
                                                &nbsp;

                                            </p>
                                            <p className='mb-0'>
                                                <span className='font-weight-bold'>{t('compare-page.products-list-brand')}:</span>
                                                &nbsp;
                                                <span>{item.brand.name}</span>
                                            </p>

                                        </div>

                                        <div className={`${classes['prices-compare']} mt-1`}>
                                            <div>
                                                <p className='mb-0'>{t('compare-page.products-list-price-title1')}</p>
                                                <p className='font-weight-bold mb-0'>{CostReplace(item.price.price + '')} сум</p>
                                            </div>
                                            {
                                                item.price && item.price.old_price!==null?(<div>
                                                    <p className='mb-0'>{t('compare-page.products-list-price-title2')}</p>
                                                    <p className='font-weight-bold mb-0'>{CostReplace(item.price.old_price + '')} сум</p>)
                                                </div>):''
                                            }

                                        </div>
                                        <button className={`${classes['button-save-compare']} add-save-button position-relative ${isSave(item.warehouse.slug, item.slug, saved)?'added-save':''} align-items-center mt-3 mb-3 d-flex`}
                                                onClick={(e)=>{
                                                    toggleSave(item.warehouse.slug, item.slug, e.currentTarget)
                                                }}>
                                            <Save />
                                            <Spinner animation="border" role="status" className={`${classes['spinner-button']} d-none`}>
                                                <span className="sr-only">Loading...</span>
                                            </Spinner>
                                            <p className='mb-0'>{t('account.address-input-button1')}</p>
                                        </button>
                                    </div>
                                )
                            })
                        }
                    </Slider>):(<div className='d-flex'>{
                        (comparesProd || []).map((item, index)=>{

                            return(
                                <div key={index} className={`${classes['item-compare-slide']} col-lg-4`} data-index={index}>
                                    <div className={classes['preloader-compare']}><Preloader /></div>
                                    <button onClick={(e)=>{
                                        const parent = document.querySelector("div[data-index='"+index+"']")
                                        const preloader = parent.querySelector(`.${classes['preloader-compare']}`)
                                        removeCompare(item.warehouse.slug, item.slug, preloader)
                                    }} className={`${classes['delete-compare']}`}><X /></button>

                                    <div className={`${classes['compare-image']} position-relative`}>
                                        
                                        <div className={`${classes['picture-block']} align-items-center`}>
                                            <div>
                                                <LazyLoadImage image={{
                                                    src: item.images[0].original_image,
                                                    srcSet: item.images[0].image_srcset,
                                                    alt: ''
                                                }}/>
                                            </div>
                                            <p className='mb-lg-3 mb-0'>
                                                <Link href={`/product/${item.slug}/${item.warehouse.slug}`} locale={locale}>
                                                    <a title={item.name}>{item.name}</a>
                                                </Link>
                                            </p>
                                            <button onClick={(e)=>{
                                                const parent = document.querySelector("div[data-index='"+index+"']")
                                                const preloader = parent.querySelector(`.${classes['preloader-compare']}`)
                                                removeCompare(item.warehouse.slug, item.slug, preloader)
                                            }} className={`${classes['delete-compare']}`}><X /></button>
                                        </div>
                                        <button onClick={()=>openModal(item.sku_id, true)}>{t('compare-page.products-list-button1')}</button>
                                    </div>

                                    <div className={`${classes['description-compare']}`}>
                                        <p>
                                            <Link href={`/product/${item.slug}/${item.warehouse.slug}`} locale={locale}>
                                                <a title={item.name}>{item.name}</a>
                                            </Link>
                                        </p>

                                        <p className='mb-0'>
                                            <span className='font-weight-bold'>{t('compare-page.products-list-model')}:</span>
                                            &nbsp;

                                        </p>
                                        <p className='mb-0'>
                                            <span className='font-weight-bold'>{t('compare-page.products-list-brand')}:</span>
                                            &nbsp;
                                            <span>{item.brand.name}</span>
                                        </p>

                                    </div>

                                    <div className={`${classes['prices-compare']} mt-1`}>
                                        <div>
                                            <p className='mb-0'>{t('compare-page.products-list-price-title1')}</p>
                                            <p className='font-weight-bold mb-0'>{CostReplace(item.price.price + '')} сум</p>
                                        </div>
                                        {
                                            item.price && item.price.old_price!==null?(<div>
                                                <p className='mb-0'>{t('compare-page.products-list-price-title2')}</p>
                                                <p className='font-weight-bold mb-0'>{CostReplace(item.price.old_price + '')} сум</p>)
                                            </div>):''
                                        }

                                    </div>
                                    <button className={`${classes['button-save-compare']} add-save-button position-relative ${isSave(item.warehouse.slug, item.slug, saved)?'added-save':''} align-items-center mt-3 mb-3 d-flex`}
                                    onClick={(e)=>{
                                        toggleSave(item.warehouse.slug, item.slug, e.currentTarget)
                                    }}>
                                        <Save />
                                        <Spinner animation="border" role="status" className={`${classes['spinner-button']} d-none`}>
                                            <span className="sr-only">Loading...</span>
                                        </Spinner>
                                        <p className='mb-0'>{t('account.address-input-button1')}</p>
                                    </button>
                                </div>
                            )
                        })
                    }</div>)
                }
                {/*{*/}
                {/*    count < 4 ?(<>{*/}
                {/*        recentlyProd.map((item, index)=>{*/}

                {/*            return(*/}
                {/*                <div key={index} className={`${classes['item-recently']}`} data-id={index}>*/}
                {/*                    <div className={`${classes['controls-recently-prod']} d-flex justify-content-between`}>*/}
                {/*                        <button onClick={()=>addCompare(item.warehouse.slug, item.slug)}>{t('compare-page.products-list-button')}</button>*/}
                {/*                        <button onClick={()=>recentlyRemove(item.id)}><X /></button>*/}
                {/*                    </div>*/}
                {/*                    <div className={`${classes['compare-image']}`}>*/}
                {/*                        <div className='p-3'>*/}
                {/*                            <LazyLoadImage image={{*/}
                {/*                                src: item.images[0].original_image,*/}
                {/*                                srcSet: item.images[0].image_srcset,*/}
                {/*                                alt: ''*/}
                {/*                            }}/>*/}
                {/*                        </div>*/}
                {/*                        <button>{t('compare-page.products-list-button1')}</button>*/}
                {/*                    </div>*/}

                {/*                    <div className={`${classes['description-compare']}`}>*/}
                {/*                        <p>*/}
                {/*                            <Link href={'/'}>*/}
                {/*                                <a>{item.name}</a>*/}
                {/*                            </Link>*/}
                {/*                        </p>*/}
                {/*                        <p className='mb-0'>*/}
                {/*                            <span className='font-weight-bold'>{t('compare-page.products-list-model')}:</span>*/}
                {/*                            &nbsp;*/}
                {/*                        </p>*/}
                {/*                        <p className='mb-0'>*/}
                {/*                            <span className='font-weight-bold'>{t('compare-page.products-list-brand')}:</span>*/}
                {/*                            &nbsp;*/}
                {/*                        </p>*/}

                {/*                    </div>*/}

                {/*                    <div className={`${classes['prices-compare']} mt-3`}>*/}
                {/*                        <div>*/}
                {/*                            <p className='mb-0'>{t('compare-page.products-list-price-title1')}</p>*/}
                {/*                            <p className='font-weight-bold mb-0'>{CostReplace(item.price + '')} UZS</p>*/}
                {/*                        </div>*/}
                {/*                        <div>*/}
                {/*                            <p className='mb-0'>{t('compare-page.products-list-price-title2')}</p>*/}
                {/*                            <p className='font-weight-bold mb-0'>{CostReplace(item.price + '')} UZS</p>*/}
                {/*                        </div>*/}
                {/*                    </div>*/}

                {/*                    <button className={`${classes['button-save-compare']} d-flex align-items-center mt-3 mb-3`}>*/}
                {/*                        <Save />*/}
                {/*                        <p className='mb-0'>{t('account.address-input-button1')}</p>*/}
                {/*                    </button>*/}
                {/*                </div>*/}
                {/*            )*/}
                {/*        })*/}
                {/*    }*/}

                {/*        <div className={`${classes['button-add']} d-flex align-items-center p-3 */}
                {/*        ${!recentlyProd.length?classes['button-add-sin']:''}`}>*/}
                {/*            <button onClick={()=>recentlyAdd()} disabled={recentlyProd.length===4}>*/}
                {/*                <Plus />*/}
                {/*                <p>{t('compare-page.products-list-button2')}</p>*/}
                {/*            </button>*/}
                {/*        </div>*/}
                {/*    </>):('')*/}
                {/*}*/}
            </div>
            {/*<div className={`${classes['compares-products-stick']} ${!compareSticky? 'd-none':''}`}>*/}
            {/*    <Slider {...settings} ref={slide2}>*/}
            {/*        {*/}
            {/*            (comparesProd || []).map((item, index)=>{*/}

            {/*                return(*/}
            {/*                    <div key={index} className={classes['item-compare-slide']}>*/}

            {/*                        <button onClick={(e)=>{*/}
            {/*                            const parent = e.currentTarget.parentElement*/}
            {/*                            const preloader = parent.querySelector(`.${classes['preloader-compare']}`)*/}
            {/*                            removeCompare(item.warehouse.slug, item.slug, preloader)*/}
            {/*                        }} className={`${classes['delete-compare']}`}><X /></button>*/}

            {/*                        <div className={`${classes['compare-image']} position-relative`}>*/}
            {/*                            <div className={classes['preloader-compare']}><Preloader /></div>*/}
            {/*                            <div className='d-flex'>*/}
            {/*                                <LazyLoadImage image={{*/}
            {/*                                    src: item.images[0].original_image,*/}
            {/*                                    srcSet: item.images[0].image_srcset,*/}
            {/*                                    alt: ''*/}
            {/*                                }}/>*/}
            {/*                                <p>*/}
            {/*                                    <Link href={`/product/${item.slug}/${item.warehouse.slug}`}>*/}
            {/*                                        <a title={item.name}>{item.name}</a>*/}
            {/*                                    </Link>*/}
            {/*                                </p>*/}
            {/*                            </div>*/}

            {/*                        </div>*/}

            {/*                        <div className={`${classes['description-compare']}`}>*/}

            {/*                            <p className='mb-0'>*/}
            {/*                                <span className='font-weight-bold'>{t('compare-page.products-list-model')}:</span>*/}
            {/*                                &nbsp;*/}

            {/*                            </p>*/}
            {/*                            <p className='mb-0'>*/}
            {/*                                <span className='font-weight-bold'>{t('compare-page.products-list-brand')}:</span>*/}
            {/*                                &nbsp;*/}
            {/*                                <span>{item.brand.name}</span>*/}
            {/*                            </p>*/}

            {/*                        </div>*/}

            {/*                        <div className={`${classes['prices-compare']} mt-1`}>*/}
            {/*                            <div>*/}
            {/*                                <p className='font-weight-bold mb-0'>{CostReplace(item.price.price + '')} сум</p>*/}
            {/*                            </div>*/}
            {/*                            {*/}
            {/*                                item.price && item.price.old_price!==null?(<div>*/}
            {/*                                    <p className='font-weight-bold mb-0'>{CostReplace(item.price.old_price + '')} сум</p>)*/}
            {/*                                </div>):''*/}
            {/*                            }*/}

            {/*                        </div>*/}
            {/*                        <button className={`${classes['button-save-compare']} d-flex align-items-center mt-3 mb-3`}>*/}
            {/*                            <Save />*/}
            {/*                            <p className='mb-0'>{t('account.address-input-button1')}</p>*/}
            {/*                        </button>*/}
            {/*                    </div>*/}
            {/*                )*/}
            {/*            })*/}
            {/*        }*/}
            {/*    </Slider>*/}
            {/*</div>*/}

        </div>
    )
}
