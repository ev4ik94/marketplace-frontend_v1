import {useState} from 'react'
import {useTranslation} from "react-i18next"
import Slider from "react-slick"



/*----Styles----*/
import classes from '../../../styles/pages-components/compare/compare-characters-table.module.sass'

/*---Icons---*/
import {Check} from 'react-bootstrap-icons'


/*---Interfaces----*/

interface IPackages{
    name: string,
    package_id: number,
    attributes: IAttributes[]
}

interface IAttributes{
    attribute_names: {},
    skus: {
        values: {
            name: string
        }[]
    }[],
    caption: string|null,
    name: string,
    is_same: boolean
}

interface ICompares{
    id: number,
    brand: {},
    category?: {},
    attribute_groups: [],
    code: string,
    images: {image_srcset: string,
        original_image: string,}[],
    name: string,
    sku_id: number,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}


export function CompareCharactersTable({
                                           packages,
                                           compare,
                                           sortSkus,
                                           arrRef,
                                           settings}:{
    packages:IPackages[],
    compare: ICompares[],
    sortSkus:number[],
    settings: {},
    arrRef:{}[]

}){
    const {t} = useTranslation()
    const [showDiferent, setShowDiferent] = useState(false)

    let a = new Array(null, null, null ,null)

    let b = a.map((item, index)=>{
        if(compare[index]){
            return packages[index]
        }else{
            return null
        }
    })

    console.log(b)
    

    // const a = packages.map(item=>{
    //     if()
    // })

    return(
        <div id='characters-compare'>
            <div className={`${classes['title-characters-table']} d-flex align-items-center flex-wrap`}>
                <h3 className='mb-0'>{t('compare-page.characters-table-title')}</h3>
                <div className='d-flex align-items-center'>
                    <div className={`${classes['check-box']}`} onClick={()=>setShowDiferent(!showDiferent)}>
                        {showDiferent?<Check />:''}
                    </div>
                    <p className='mb-0'>{t('compare-page.characters-table-check-box')}</p>
                </div>
            </div>

            <div className={`${classes['attributes-table-groups']}`} id='attributes-group'>
                {
                    packages.map((item, index)=>{

                        return(
                            <div key={index} data-title={item.name}>
                                <p className='mb-0'>{item.name}</p>
                                <div>
                                    {
                                        item.attributes.map((attribute, index)=>{
                                            let attrNames = []

                                            for(let k =0; k<sortSkus.length; k++){
                                                attrNames.push({
                                                    id: sortSkus[k],
                                                    name: attribute.skus[sortSkus[k]].values[0].name
                                                })
                                            }
                                            for(let i=0; i<compare.length; i++){
                                                if(!attrNames[i]){
                                                    attrNames.push({})
                                                }
                                            }
                                           
                                            const showDifferentValues = !attribute.is_same?classes['light-diferent']:''
                                            return(
                                                <div key={index} className={`${classes['item-attribute-group']} `}>
                                                    <p>{attribute.name}</p>

                                                    <Slider {...settings} ref={slider=>arrRef.push(slider)}>
                                                        {
                                                            attrNames.map((attr, index)=>{
                                                                return(
                                                                    <div key={index} className={`${showDiferent?showDifferentValues:''}  ${classes['item-character-compare']}`}>
                                                                        {
                                                                            attr.name?<p className='mb-0 text-center'>{attr.name}</p>:''
                                                                        }
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </Slider>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        )
                    })
                }
            </div>

        </div>
    )
}
