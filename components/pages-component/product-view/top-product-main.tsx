import classes from "../../../styles/pages-components/product/product-view.module.sass"
import {useRouter} from "next/router";
import {useState, useEffect} from 'react'
import { useTranslation } from 'react-i18next'


/*---Bootstrap Icons----*/
import {ArrowsAngleExpand, Star, ThreeDots} from "react-bootstrap-icons"
import {Cart} from '../../icons/Cart'
import {Save} from '../../icons/Save'
import {Compare} from '../../icons/Compare'
import {Box} from '../../icons/Box'
import {PickUp} from '../../icons/pickup'

/*---Components---*/
import {CostReplace} from '../../secondary-func'
import Link from "next/link"
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"



/*---Bootstrap Components---*/
import {Spinner} from 'react-bootstrap'
import {Truck} from "../../icons/Truck";
import {Garantiya} from "../../icons/Garantiya";


/*---Interface---*/

interface IProductInfo{
    id: number,
    brand: {
        name: string,
        slug: string
    },
    category: {
        slug: string
    },
    skus: {
        id: number,
        sku_slug: string,
        warehouse_slug: string
    }[],
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[]
    attribute_groups: [],
    name: string,
    price: {
        old_price: number,
        price: number
    },
    shop:{
        name: string,
        slug: string
    },
    warehouse: {
        name: string,
        slug: string
    },
    slug: string,
    breadcrums: IBreadCrumbs[],
    variations: IVariations[]
}

interface IBreadCrumbs{
    category_id: number,
    name: string
}



interface IVariations{
    name: string,
    children: IChild[],
}

interface IChild{
    id: number,
    name: string,
    sku_ids: {

    }[],
    value: string
}



export function TopProductMain({
                                   productInfo,
                                   handleShow,
                                   addToCart,
                                   loading,
                                   isAddedCart,
                                   isAddedSave,
                                   addSave,
                                   isAddedCompare,
                                   loadingSave,
                                   loadingProduct,
                                   loadingCompare,
                                   addToCompare,
                                   deleteCompare,
                                   deleteSave
}:
                                   {
                                       productInfo:IProductInfo,
                                       handleShow:()=>void,
                                       addToCart:(warehouse?:string, slug?:string)=>void,
                                       loading: boolean,
                                       isAddedCart: boolean,
                                       isAddedCompare: boolean,
                                       isAddedSave: boolean,
                                       loadingSave: boolean,
                                       addSave: (warehouse_slug:string, slug:string)=>void,
                                       loadingProduct: boolean,
                                       loadingCompare: boolean,
                                       addToCompare: (warehouse_slug:string, slug: string, category_slug:string)=>void,
                                       deleteSave: (warehouse_slug:string, slug:string)=>void,
                                       deleteCompare: (warehouse_slug:string, slug:string)=>void
                                   })
{


    const router = useRouter()
    const {locale} = router
    const {sku_slug, product_slug} = router.query
    const {t} = useTranslation()
    const [variations, setVariations] = useState<IVariations[]>([])
    const [currentImage, setCurrentImage] = useState(null)

    useEffect(()=>{
        if(productInfo!==null){
            setCurrentImage(productInfo.images&&productInfo.images.length?productInfo.images[0]:null);
        }
    }, [])

    useEffect(()=>{
        if(productInfo!==null&&productInfo.variations){
            variationsFilter()
        }
    }, [productInfo])

    /*======== Handler Functions =============*/

    const clickPreviewImage = (e,obj)=>{
        setCurrentImage(obj)
        const allSlidesParent = document.querySelector(`.${classes['slider-nav']}>div`)
        const arraySlides = allSlidesParent!==null?Array.from(allSlidesParent.children):[]
        arraySlides.forEach(item=>item.classList.remove(classes['active']))
        e.currentTarget.classList.add(classes['active'])
    }

    const variationsFilter = ()=>{
        let arrVariations = []
        for(let value in productInfo.variations){
            let obj = {
                name: productInfo.variations[value].name,
                children: productInfo.variations[value].children
            }
            arrVariations.push(obj)
        }
        setVariations(arrVariations)

    }

    const filterSkusVariations = (skuId:number, value: string)=>{

        if(productInfo!==null&&productInfo.skus){
            let product = productInfo.skus.filter(item=>item.id===skuId)
            if(product.length){
                return product[0][value]?product[0][value]:[]
            }
        }

        return productInfo!==null && productInfo.images ? productInfo.images: []

    }

    if(productInfo!==null){

        return(
            <div className={`${classes['top-product-wrap']} d-flex flex-wrap`}>

                <div className={`col-lg-7 pl-0 ${classes['wrap-image-preview']}`}>
                    <div className={`${classes['product-info-review-block']}`}>
                        <div className={`mt-1`}>
                            <h3 className='mb-3'>{productInfo!==null&&productInfo.name?productInfo.name:''}</h3>
                        </div>

                        <div className={`${classes['review-block']} d-flex mt-1`}>
                            <div className={`${classes['rating-block']} d-flex`}>
                                <button>
                                    <Star />
                                </button>
                                <button>
                                    <Star />
                                </button>
                                <button>
                                    <Star />
                                </button>
                                <button>
                                    <Star />
                                </button>
                                <button>
                                    <Star />
                                </button>
                            </div>
                            <div className={`${classes['reviews-info']} d-flex align-items-center`}>
                                <button className={`${classes['review-buttons-item']}`}>
                                    4.5 (14 Reviews)
                                </button>
                                <button className={`${classes['review-buttons-item']}`}>
                                    25 Answered Questions
                                </button>
                                <p className='mb-0 pl-1 text-center'><span className='font-weight-bold'>{t('model')}:</span> {productInfo.code}</p>
                            </div>
                        </div>
                    </div>
                    <div className={`align-self-baseline position-sticky ${classes['view-image-wrap']}`} style={{top: '10px'}}>
                        <div className={`position-relative`} onClick={handleShow}>
                            <button className={`position-absolute`}>
                                <ArrowsAngleExpand />
                            </button>
                            <div>
                                <LazyLoadImage image={{
                                    src: currentImage!==null && currentImage.original_image?currentImage.original_image:'',
                                    srcSet: currentImage!==null && currentImage.image_srcset?currentImage.image_srcset:'',
                                    alt: ''
                                }}/>
                            </div>
                        </div>

                        <div className={`${classes['slider-nav']}`}>

                            <div className={`d-flex align-items-center justify-content-center`} >
                                {
                                    filterSkusVariations(Number(sku_slug),'image_urls').map((item,index)=>{
                                        const windowWidth = window.innerWidth
                                        let countShow = windowWidth >= 600 ? 5 :3
                                        if(index<countShow){
                                            return(
                                                <div onClick={(e)=>clickPreviewImage(e,item)} key={index} className={index===0?classes['active']:''}>
                                                    <LazyLoadImage image={{
                                                        src: item.original_image,
                                                        srcSet: item.image_srcset,
                                                        alt: ''
                                                    }}/>
                                                </div>
                                            )
                                        }else if(index===(countShow+1)){
                                            return(
                                                <button onClick={handleShow} key={index} className={`${classes['btn-more-view']}`}>
                                                    <ThreeDots />
                                                </button>
                                            )
                                        }
                                    })
                                }

                                {

                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className={`col-lg-5 col-12 ${classes['info-product-cost-variations']}`}>
                    <div className={`${classes['block-payment-buy']}`}>
                        <p className={`${classes['text-cost']}`}>{CostReplace(productInfo.price.price+'')} СУМ</p>
                        <div className={`${classes['refund-conditions']} d-flex mb-3`}>
                            <div className={classes['icon']}>
                                <Box />
                            </div>
                            <div>
                                <p className='font-weight-bold mb-0'>{t('product-view-refund-1')}</p>
                                <p className='mb-0'>{t('product-view-refund-2')} <Link href='/' locale={locale}>
                                    <a>{t('product-view-refund-3')}</a>
                                </Link></p>
                            </div>
                        </div>
                        <div className={classes['variations-title-block']}>
                            <p className='mb-0 text-center'>{t('product-view-variations-title')}</p>
                        </div>
                        <div className={`${classes['variations-block']} mb-3 mt-3`}>
                            {
                                variations.map((item,index)=>{
                                        return(
                                            <div key={index}>

                                                <div className={`${classes['block-colors']}`}>
                                                    {
                                                        item.children.map((child, index)=>{
                                                            const dataSku = productInfo.skus.filter(item=>item.id===child.sku_ids[0]).length?
                                                                productInfo.skus.filter(item=>item.id===child.sku_ids[0])[0]:null

                                                            if(sku_slug!==dataSku.sku_slug){
                                                                return(
                                                                    <Link href={`/product/${dataSku!==null&&dataSku?dataSku.warehouse_slug:''}/${dataSku!==null&&dataSku?dataSku.sku_slug:''}`} key={index}>
                                                                        <a className='col-lg-4 col-md-3 col-sm-3 col-12 d-block pl-0 pr-0 mb-3 mr-3'>
                                                                            <div className={`${classes['item-color']}
                                                                                ${Number(sku_slug)===Array.from(child.sku_ids)[0]?`${classes['active']}`:''}`}
                                                                            >
                                                                                <div>
                                                                                    <p className={'mb-0'}>{child.name}</p>
                                                                                </div>
                                                                                <p className='mb-0 mt-1'>{
                                                                                    child.sku_ids.map((sku,index)=>{
                                                                                        return(<span key={index}>{CostReplace(filterSkusVariations(Number(sku), 'price')+'')}</span>)
                                                                                    })
                                                                                } СУМ</p>
                                                                            </div>
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }else{
                                                                return(<div className={`col-lg-4 col-md-3 col-sm-3 col-12 ${classes['item-color']} ${classes['item_active']} mr-3 mb-3`} key={index}>
                                                                    <div>
                                                                        <p className={'mb-0'}>{child.name}</p>
                                                                    </div>
                                                                <p className='mb-0 mt-1'>{
                                                                    child.sku_ids.map((sku,index)=>{
                                                                        return(<span key={index}>{CostReplace(filterSkusVariations(Number(sku), 'price')+'')}</span>)
                                                                    })
                                                                } СУМ</p>
                                                            </div>
                                                                )
                                                            }

                                                        })
                                                    }
                                                </div>
                                            </div>
                                        )


                                })
                            }
                        </div>
                        <div className={`${classes['conditions-delivery']} mb-3 pl-3`}>
                            <div>
                                <div className={`${classes['icon']} d-lg-block d-none`}>
                                    <Truck />
                                </div>
                                <div>
                                        <p>{t('product-list.product-delivery')}: <span>{t('product.delivery')}</span></p>
                                </div>
                            </div>
                            <div>
                                <div className={`${classes['icon']} d-lg-block d-none`}>
                                    <Garantiya />
                                </div>
                                <div>
                                    <p>{t('product-list.product-garantiya')}: <span>{t('product.garantiya')}</span></p>
                                </div>
                            </div>
                            <div>
                                <div className={`${classes['icon']} d-lg-block d-none`}>
                                    <PickUp />
                                </div>
                                <div>
                                    <p>{t('product-view-conditions-1')}: <span>{t('product-view-conditions-2')}</span></p>
                                </div>
                            </div>
                        </div>
                        {
                            isAddedCart?(<Link href='/cart' locale={locale}>
                                <a className={`position-relative added-cart add-cart-button ${classes['add_cart']}`}><Cart /><span>{t('button-added-cart')}</span></a>
                            </Link>):(<button
                                onClick={() => addToCart(product_slug.toString(), sku_slug.toString())}
                                style={{opacity: loading?'.8':'1'}}
                                className={`position-relative add-cart-button`}>
                                <Cart />
                                <span>{t('cart-button')}</span>
                                <Spinner animation="border" role="status" className={`${loading?'d-block':'d-none'} ${classes['spinner-button']}`}>
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                            </button>)

                        }

                        <div className={`d-flex ${classes['buttons-save-compare']}`}>
                            <button className={`add-save-button position-relative ${isAddedSave?'added-save':''} align-items-center`}
                                    onClick={()=>{
                                        if(isAddedSave){
                                            deleteSave(product_slug.toString(), sku_slug.toString())
                                        }else{
                                            addSave(product_slug.toString(), sku_slug.toString())
                                        }
                                    }}>
                                <Save />
                                <Spinner animation="border" role="status" className={`${loadingSave?'d-block':'d-none'}
                                ${classes['spinner-button']}`}>
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                                {t('button-save')}
                            </button>

                            <button className={`add-compare-button ${isAddedCompare?'added-compare':''} position-relative`} onClick={()=>{
                                if(isAddedCompare){
                                    deleteCompare(product_slug.toString(), sku_slug.toString())
                                }else{
                                    addToCompare(product_slug.toString(), sku_slug.toString(), productInfo.category.slug)
                                }
                            }}>
                                <Compare />
                                <Spinner animation="border" role="status" className={`${loadingCompare?'d-block':'d-none'}
                                ${classes['spinner-button']}`}>
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                                {t('compare-button.compare')}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    return(<></>)





}
