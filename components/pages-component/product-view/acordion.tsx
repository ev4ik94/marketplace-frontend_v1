import {useEffect, useState} from 'react'
import { useTranslation } from 'react-i18next'


/*---Icons----*/
import {ChevronDown} from "react-bootstrap-icons"


/*----Styles----*/
import classes from '../../../styles/pages-components/product/acardion.module.sass'

/*----Components----*/
import {Characters} from './characters'
import {Review} from "./review";


/*-----Interfaces----*/

interface ICharacters{
    id: number,
    name: string,
    description: string,
    attributes: IAttributes[]
}

interface IAttributes{
    id: number,
    name: string,
    values: [],
    group_id: number,
    group_name: string,
    sku_id: number
}

export function Accordion({characters}:{characters:ICharacters[]}){

    const {t} = useTranslation()


    return(
        <div className={`${classes['wrap-characters-component']} mx-3 mb-5 pt-3`}>
                <ul className={`pl-0`}>
                    <li className={`${classes['container-wrap-characters']} pl-0`}>
                        <div className={`${classes['title-characters']} d-flex px-0`} onClick={(e)=>{
                            let elem = e.currentTarget.parentElement
                            if(elem && elem!==null){
                                elem.classList.toggle(`${classes['active']}`)

                                const elemDropDown = elem.querySelectorAll(`.${classes['drop-down-items']}`).length?
                                    elem.querySelectorAll(`.${classes['drop-down-items']}`)[0] as HTMLElement : null

                                const childElement = elemDropDown.firstChild!==null?elemDropDown.firstChild as HTMLElement:null

                                const height = elemDropDown && elemDropDown!==null && childElement!==null
                                    ?elemDropDown.offsetHeight + childElement.offsetHeight + 10: 0

                                if(elem.classList.contains(`${classes['active']}`)){
                                    elemDropDown.style.height = `${height}px`
                                }else{
                                    elemDropDown.style.height = ``
                                }

                            }
                        }}>
                            <div>
                                <p className="mb-0">{t('product-view-link.specification')}</p>
                            </div>
                            <div className={`${classes['close-btn']} ml-auto`}>
                                <ChevronDown />
                            </div>
                        </div>
                        <Characters characters={characters}/>
                    </li>
                    <li className={`${classes['container-wrap-characters']} pl-0`}>
                        <div className={`${classes['title-characters']} d-flex px-0`} onClick={(e)=>{
                            let elem = e.currentTarget.parentElement
                            if(elem && elem!==null){
                                elem.classList.toggle(`${classes['active']}`)

                                const elemDropDown = elem.querySelectorAll(`.${classes['drop-down-items']}`).length?
                                    elem.querySelectorAll(`.${classes['drop-down-items']}`)[0] as HTMLElement : null

                                const childElement = elemDropDown.firstChild!==null?elemDropDown.firstChild as HTMLElement:null

                                const height = elemDropDown && elemDropDown!==null && childElement!==null
                                    ?elemDropDown.offsetHeight + childElement.offsetHeight + 10: 0

                                if(elem.classList.contains(`${classes['active']}`)){
                                    elemDropDown.style.height = `${height}px`
                                }else{
                                    elemDropDown.style.height = ``
                                }

                            }
                        }}>
                            <div>
                                <p className="mb-0">{t('product-view-link.review')}</p>
                            </div>
                            <div className={`${classes['close-btn']} ml-auto`}>
                                <ChevronDown />
                            </div>
                        </div>
                        <Review />
                    </li>

                </ul>
        </div>
    )

}
