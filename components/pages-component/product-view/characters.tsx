import {useEffect, useState} from 'react'
import { useTranslation } from 'react-i18next'


/*----Styles----*/
import classes from '../../../styles/pages-components/product/acardion.module.sass'

/*-----Interfaces----*/

interface ICharacters{
    id: number,
    description: string,
    name: string,
    attributes: IAttributes[]
}

interface IAttributes{
    id: number,
    values: [],
    group_id: number,
    name: string,
    sku_id: number
}

export function Characters({characters}:{characters:ICharacters[]}){

    const [charactersArr, setCharacters] = useState([])


    useEffect(()=>{
        setCharacters(characters)
    }, [characters])


    return(
        <div className={`${classes['wrap-characters-component']} ${classes['drop-down-items']}  pl-0`}>
                <ul className={`pl-0`}>
                    {
                        charactersArr.map((item,index)=>{
                            return(
                                <div className={'row pt-3 pb-1 border-bottom'} key={index}>
                                    <div className={`col-lg-3`}>
                                        <p>{item.name}</p>
                                    </div>
                                    <div className={`col-lg-9`}>
                                    {
                                        item.attributes.map((child, index)=>{

                                            return(
                                                <div key={index} className="row mb-2">

                                                    <div className="col-lg-6 col-md-6 d-flex">
                                                        <p className="text-left mb-0" dangerouslySetInnerHTML={{__html:child.name}} />
                                                    </div>
                                                    <div className="col-lg-6 col-md-6">
                                                        <div className="text-left mb-0">
                                                            {
                                                                child.values.map((value, index)=>{
                                                                    return(
                                                                        <span dangerouslySetInnerHTML={{__html:value.name+(child.values.length > 1?", ":"")}} key={index}/>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    </div>

                                </div>
                            )
                        })
                    }
                </ul>
        </div>
    )
}
