import Link from 'next/link'
import React, {useEffect, useState, useRef} from 'react'
import {useRouter} from "next/router"
import { useTranslation } from 'react-i18next'


/*---Components-bootstrap---*/
import {DropdownButton, Navbar, Nav} from 'react-bootstrap'

/*--Components---*/
import {BottomNavbar} from "./header/Bottom-Navbar"
import {SearchForm} from "./header/Search"


/*----Icons----*/
import {Globe, GeoAlt, ChevronRight} from 'react-bootstrap-icons'
import {Cart} from '../icons/Cart'
import { Profile } from '../icons/Profile'


/*---Style---*/
import classes from '../../styles/main-components/header.module.sass'


/*----Hooks----*/
import {useAuth} from "../../hooks/authentication.hook"



/*----Interfaces---*/

interface IMenu {
    id: number,
    type: string,
    name:string,
    slug: string,
    recursive_children:IChildren[]
}


interface IChildren{
    id: number,
    type: string,
    name:string,
    slug: string,
    recursive_children:IChildren[]
}


interface IUser{
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    password: string,
    address: string
}

interface IProductsR{
    brand: string,
    category: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string
    }[],
    name: string,
    price: {
        price: number,
        old_price: number
    },
    shop: {
        name: string,
        slug: string
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface IBrands{
    id: number,
    name: string,
    slug: string
}

interface ICart{
    items: [],
    total_price: number,
    total_quantity: number
}

export function Header({
                           menu,
                           cart,
                           setCart,
                           onToggleMenu,
                           brands,
                           cartObj,
                           recently,
                           popular,
                           save
}:
                           {
                               menu:IMenu[],
                               cart:number,
                               cartObj:ICart,
                               onToggleMenu: (value: number, state: boolean)=>void,
                               setCart: (value:any)=>void,
                               brands: IBrands[],
                               recently: IProductsR[],
                               popular: IProductsR[],
                               save: IProductsR[]
                           }){

    const [showSubMenu, SetShowSubMenu] = useState(false)
    const [subChildren, setSubChildren] = useState([])
    const [subTitleId, setSubTitleId] = useState('')
    const {t} = useTranslation()

    const [newObjCategories, setCategories] = useState([])
    const [currentChild, setCurrentChild] = useState([])
    const [titleCurrentCat, setTitleCurrentCat] = useState({
        name: '',
        slug: ''
    })
    const [userData, setUserData] = useState<IUser | null>(null)

    /*------ Mobile Categories Render----*/
    const [currentCatMobile, setCurrentCatMobile] = useState([])
    const [subTitleName, setSubTitleName] = useState(null)

    const router = useRouter()
    const {locale} = router



    const {checkAuth, getUserData, logout} = useAuth()

    const [brandM, setBrandM] = useState('')


    const subMenu = useRef(null)
    const collapse = useRef(null)


    useEffect(()=>{
        if(typeof window !== 'undefined'){
            if(checkAuth()){
                setUserData(getUserData())
            }

        }
    }, [])



    useEffect(()=>{

        let newCat = [
            {
                id: 0,
                type: 'menu',
                name:'Продукты',
                slug: 'category',
                children: menu!==null?menu:[]
            },
            {
                id: 1,
                type: 'menu',
                name:'Бренды',
                slug: 'brand',
                children: brands!==null?brands:[]
            }

        ]


        setCurrentChild(menu!==null&&menu.length?menu[0].recursive_children:[])
        setCategories(newCat)
        setCurrentCatMobile(newCat)
        setTitleCurrentCat({...titleCurrentCat, name: menu!==null&&menu.length?menu[0].name:'', slug: menu!==null&&menu.length?menu[0].slug:''})
    }, [menu])


    const eventClickMobileMenu = (slug, name, children)=>{

        const windowWidth = window.innerWidth

        setSubTitleName(name)
        setCurrentCatMobile(children)
        setSubTitleId(slug)

        if(windowWidth<=992){

            const children = findChild(newObjCategories, slug)

            if(children){
                if(children.length){
                    SetShowSubMenu(true)
                    setSubChildren(children)
                    setTimeout(()=>{
                        collapse.current.style.height = subMenu.current.offsetHeight + 85 + 'px'
                    }, 10)

                }
            }

        }


    }

    const findChild = (arr, slug)=>{

        for(let i=0; i<arr.length; i++){
            if(arr[i] && arr[i].slug===slug){
                return arr[i].children
            }
            for(let k=0; k<arr[i].children.length; k++){
                if(arr[i].children[k] && arr[i].children[k].id===slug){
                    return arr[i].children[k].recursive_children
                }

                if(arr[i].children[k].recursive_children&&arr[i].children[k].recursive_children.length){

                    for(let l=0; l<arr[i].children[k].recursive_children.length; l++){
                        if(arr[i].children[k].recursive_children[l] && arr[i].children[k].recursive_children[l].slug===slug){
                            return arr[i].children[k].recursive_children[l].recursive_children
                        }
                    }
                }
            }
        }

        return false

    }

    const hoverClassRemoveAdd = (e)=>{
        let parentWrap = e.currentTarget.parentElement
        const links = parentWrap && parentWrap!==null ? Array.from(parentWrap.querySelectorAll('.title-link-menu')):[]

        links.forEach(item=>{
            let elem = item as HTMLElement
            if(elem.classList.contains(classes['hover-active'])){
                elem.classList.remove(classes['hover-active'])
            }
        })

        e.currentTarget.classList.add(classes['hover-active'])
    }


    const renderMenu = ()=>{

        return newObjCategories.map((item, index)=>{
            let brand = item.slug==='brand'

            return(
                <DropdownButton
                    key={index}
                    id={`dropdown-button-drop-${index}-e`}
                    title={item.name}
                    className={`${classes['dropdown-link-title']}`}
                    data-name={item.name}
                    data-id={item.id}
                    onToggle={(e)=>{
                        onToggleMenu(item.id, e)

                        if(e){
                            setCurrentChild(item.children.length?(item.children[0].recursive_children?item.children[0].recursive_children:item.children[0].categories): [])
                            setTitleCurrentCat({name: item.children.length?item.children[0].name: '', slug:  item.children.length?item.children[0].slug: ''})
                           
                        }
                    }}

                >

                    <div className={`d-flex p-0`}>
                        <div className={`col-lg-5 pl-0 pr-0`}>
                            {
                                item.children.map((item, index)=>(
                                    <p onMouseOver={(e)=>{
                                        hoverClassRemoveAdd(e)
                                        setCurrentChild(item.recursive_children?item.recursive_children:(item.categories || []))
                                        setTitleCurrentCat({name: item.name, slug: item.slug})
                                       
                                    }} key={index} className={`mb-0 title-link-menu ${index===0?classes['hover-active']:''} position-relative`}
                                    >
                                        {item.name} <ChevronRight />
                                    </p>
                                ))
                            }
                        </div>
                        <div className={`col-lg-7`}>
                            <Link href={`/products/${titleCurrentCat.slug}`} locale={locale}>
                                <a>{titleCurrentCat.name}</a>
                            </Link>

                            <div className={`d-flex flex-wrap mt-3`}>
                                {
                                    f(currentChild, brand?titleCurrentCat.slug:null)
                                }
                            </div>
                        </div>
                    </div>

                </DropdownButton>
            )
        })
    }


    const f = (children, brand=null)=>{

        return(
            <>

                {
                    (children || []).map((item, index, arr)=>{

                        const childrenArr = item.recursive_children?item.recursive_children:(item.children||[])

                        if(childrenArr.length){

                            return(
                                <div className='col-lg-6 pt-0 pb-0 pl-1 pr-1 mb-3 align-self-baseline' key={index}>
                                    <>
                                        <Link href={`/products/${item.slug}${brand!==null?`?brands[0]=${titleCurrentCat.slug}`:''}`} locale={locale}>
                                            <a className={`font-weight-bold ${classes['title-link']}`}>{item.name}</a>
                                        </Link>
                                        <div>{f(childrenArr, brand)}</div></>
                                </div>
                            )
                        }else{

                            if((index+1) < 6){

                                return(
                                    <p key={item.slug} className='mb-0'>
                                        <Link href={`/products/${item.slug}${brand!==null?`?brands[0]=${titleCurrentCat.slug}`:''}`} locale={locale}>
                                            <a className='d-block'>{item.name}</a>
                                        </Link>
                                    </p>
                                )
                            }else if((index+1) ===6){
                                return(
                                    <p key={item.slug} className={`${classes['link-more']} mb-0`}>
                                        <Link href={`/products/${item.slug}${brand!==null?`?brands[0]=${titleCurrentCat.slug}`:''}`} locale={locale}>
                                            <a className='d-block'>
                                                <span className='position-static'>See more</span>
                                                <ChevronRight />
                                            </a>
                                        </Link>
                                    </p>
                                )
                            }
                        }
                    })
                }
            </>
        )
    }


    /*----- Mobile Version Menu START-----*/

    const renderCategories = (children)=>{

        return (children || []).map((item, index)=>{

            const childrenCat = item.children ? item.children : (item.recursive_children?item.recursive_children:
                (item.categories?item.categories:[]))

            return (
                <div onClick={(e)=>{
                    const brand = e.currentTarget.getAttribute('data-brand')
                    if(brand!==''){
                        setBrandM(brand)
                    }
                    searchParent(newObjCategories, item.slug)
                    setCurrentCatMobile(childrenCat)
                    setSubTitleName({...subTitleName, name: item.name, slug: item.slug})
                }} className={`${childrenCat.length?classes['has-child']:''} 
                                ${classes['item-mobile-menu']}`} key={index} data-brand={item.categories?item.slug:''}>
                    {childrenCat.length? (<p className='mb-0'>{item.name}</p>) :
                        (<Link href={`/products/${item.slug}${brandM!==''?`?brands[0]=${brandM}`:''}`} locale={locale}>
                        <a className='d-block'>{item.name}</a>
                    </Link>)
                    }
                </div>
            )
        })
    }

    function returnCat(slug){

        for(let k=0; newObjCategories.length>k; k++){
            const arrChild = newObjCategories[k].children || newObjCategories[k].recursive_children
            if(newObjCategories[k].slug===slug){
                setSubTitleName(null)
                setBrandM('')
                setCurrentCatMobile(newObjCategories)
            }
            for(let j=0; arrChild.length>j; j++){
                if(arrChild[j].slug===slug){
                    setSubTitleName({...subTitleName, name: newObjCategories[k].name, slug: newObjCategories[k].slug})
                    setCurrentCatMobile(newObjCategories[k].children || newObjCategories[k].recursive_children)
                }else{
                    if(arrChild[j].recursive_children){
                        for(let l=0; arrChild[j].recursive_children.length>l; l++){
                            if(arrChild[j].recursive_children[l].slug===slug){
                                setSubTitleName({...subTitleName, name: arrChild[j].name, slug: arrChild[j].slug})
                                setCurrentCatMobile(arrChild[j].recursive_children)
                            }
                        }
                    }
                }
            }
        }
    }

    const searchParent = (arr, slug, prevSet=false)=>{

        for(let i=0; i<arr.length; i++){
            const children = arr[i].children?arr[i].children:(arr[i].recursive_children?arr[i].recursive_children:[])

            const isChild = children.filter(item=>item.slug===slug)
            if(isChild.length){

                if(prevSet){

                }else{
                    setCurrentCatMobile(children)
                }
                break
            }else{
                searchParent(children, slug)
            }
        }
    }



    /*----- Mobile Version Menu END-----*/



    const renderChild = (children)=>{

        return (children || []).map((item)=>{
            if(item.recursive_children.length){

                return(
                    <DropdownButton
                        drop='right'
                        key={item.slug}
                        id={`dropdown-button-drop--${item.slug}`}
                        title={item.name}
                        data-name={item.name}
                        data-slug={item.slug}
                        className={classes['drop-down-sub']}
                        onClick={()=>eventClickMobileMenu(item.slug, item.name, item.recursive_children)}
                    >

                        {renderChild(item.recursive_children)}
                        {
                            item.recursive_children.length?(<Link href={`/categories/${item.slug}`} locale={locale}>
                                <a>Все категории</a>
                            </Link>):''
                        }
                    </DropdownButton>

                )
            }else{
                return(
                    <Link href={`/products/${item.slug}`} key={item.slug} locale={locale}>
                        <a className={classes['nav-link']} key={item.slug}>{item.name}</a>
                    </Link>
                )
            }

        })
    }




    return (
        <div className={`${classes['header-wrap']}`}>
            <Navbar expand="lg" className={`${classes['nav-bar']} overflow-lg-auto`} variant='dark'>
                <Link href={'/'} locale={locale}>
                    <a>
                        <Navbar.Brand className='d-flex'>
                            <div className={classes['icon-brand']}>
                                <img src="/logo-farq.svg" alt=""/>
                            </div>
                            <div className='d-lg-flex d-none flex-column justify-content-end'><span>electronics</span></div>
                        </Navbar.Brand>
                    </a>
                </Link>

                <Navbar.Toggle aria-controls="basic-navbar-nav" className={classes['toggle-button']}/>
                <SearchForm />
                <Link href={'/cart'} locale={locale}>
                        <a className={`d-lg-none nav-link ${classes['md-link']} ${cart>0?classes['active']:''} position-relative`} >
                            <Cart />
                            <span className={`${cart>9?classes['more']:''} position-absolute`}>{cart>9?9:cart}</span>
                        </a>
                </Link>

                <Navbar.Collapse id="basic-navbar-nav" className='w-100' ref={collapse}>

                    <Nav className={`ml-lg-auto ${classes['top-nav-link']} justify-content-lg-end justify-content-start pt-3 flex-row`}>
                        <Link href={'/'} locale={locale}>
                            <a className={`nav-link`}>
                                <GeoAlt />
                                <span>Ташкент</span>
                            </a>
                        </Link>

                        <div className={`d-flex align-items-center ${classes['dropdown-button-nav']} ${classes['dropdown-languages-btn']}`}>
                            <Globe />
                            <DropdownButton
                                id={`dropdown-button-drop-languages`}
                                title={router.locale}
                                onToggle = {(e, event, metadata)=>{

                                }}

                            >
                                <div>
                                    <div>
                                        <Link href={router.route} locale='ru'>
                                            <a href="">
                                                <p className='mb-0'>Русский</p>
                                            </a>
                                        </Link>
                                    </div>
                                    <div>
                                        <Link href={router.route} locale='uz'>
                                            <a href="">
                                                <p className='mb-0'>Uzbek</p>
                                            </a>
                                        </Link>
                                    </div>
                                    <div>
                                        <Link href={router.route} locale='en'>
                                            <a href="">
                                                <p className='mb-0'>English</p>
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                            </DropdownButton>
                        </div>
                        <Link href={'/cart'} locale={locale}>
                            <a className={`d-lg-block d-none nav-link ${cart>0?classes['active']:''} position-relative`}>
                                <Cart />
                                <span className={`${cart>9?classes['more']:''}`}>{cart>9?9:cart}</span>
                            </a>
                        </Link>
                    </Nav>
                    <div className={`d-lg-none ${classes['collapse-navbar']} position-relative`}>
                        <div className='mt-3'>
                            {
                                userData!==null?(<div className={`${classes['item-mobile-menu']} my-0`}>
                                <Link href='/account' locale={locale}>
                                   <a className="d-flex align-items-center">
                                        <Profile className='mr-2'/>
                                        <p className="mb-0">{userData.first_name?userData.first_name:(userData.email?userData.email:'')}</p>
                                   </a>
                                </Link>
                            </div>):(<div className={`${classes['item-mobile-menu']} my-0`}>
                                <Link href='/identity/signin' locale={locale}>
                                    <a className='d-block'>{t('header.dropdown-account.signin-text')}</a>
                                </Link>
                            </div>)
                            }
                            {
                                subTitleName!==null?(<button className={classes['btn-prev-cat']} onClick={()=>{
                                    returnCat(subTitleName.slug)

                                }}>{subTitleName.name}</button>):''
                            }
                            <div className={classes['categories-menu-mobile']}>
                                {renderCategories(currentCatMobile)}
                            </div>
                        </div>

                        <Nav className={`mr-auto col-lg-7 col-12 pr-0 ${classes['top-nav-link']} ${classes['rgt-link']} justify-content-end `} id='bottom-navBar'>
                            <div className={`${classes['item-mobile-menu']} my-0 d-lg-block d-none`}>
                                <Link href='/identity/signin' locale={locale}>
                                    <a className='d-block'>Вход</a>
                                </Link>
                            </div>
                            <div className={classes['item-mobile-menu']}>
                                <Link href={'/recently-viewed'} locale={locale}>
                                    <a className='d-block'>Недавно просмотренные</a>
                                </Link>
                            </div>
                            <div className={`${classes['item-mobile-menu']} my-0`}>
                                <Link href={'/track-order'} locale={locale}>
                                    <a className='d-block'>Статус заказа</a>
                                </Link>
                            </div>
                            <div className={classes['item-mobile-menu']}>
                                <Link href={'/recently-viewed/'} locale={locale}>
                                    <a className='d-block'>Сохраненное</a>
                                </Link>
                            </div>

                        </Nav>
                    </div>


                </Navbar.Collapse>
            </Navbar>
            <div className='d-lg-block d-none'>
                <BottomNavbar
                    renderMenu={renderMenu}
                    userData = {userData}
                    onToggleMenu={onToggleMenu}
                    logout={logout}
                    setCart={setCart}
                    cart={cartObj}
                    recently={recently}
                    popular={popular}
                    save={save}
                />
            </div>
        </div>
    )
}

