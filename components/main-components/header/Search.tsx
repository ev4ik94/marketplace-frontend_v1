import React, {useState, useEffect} from "react"
import {useRouter} from "next/router"
import Link from 'next/link'
import { useTranslation } from 'react-i18next'

/*----Icons--------*/
import {Search, X} from 'react-bootstrap-icons'

/*----Bootstrap----*/
import {Button, Form, FormControl} from "react-bootstrap"

/*------Components----*/
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image"


/*----Style----*/
import classes from "../../../styles/main-components/header.module.sass"

/*----Hooks----*/
import {AxiosApi} from "../../../hooks/axios.hook"


export function SearchForm(){

    const router = useRouter()
    const {locale} = router
    const {t} = useTranslation()
    /*----Values Data------*/
    const [searchVal, setSearchVal] = useState('')
    const [generics, setGenerics] = useState([])
    const [products, setProducts] = useState([])
    const [category_slug, setCategory] = useState('')

    const {request} = AxiosApi()

    const requestSearch = async()=>{
        if(searchVal.length>1){
            await request(`${process.env.SEARCH['ru']}?q=${searchVal}`)
                .then(result=>{
                    setGenerics(result.data.generic)
                    setProducts(result.data.products)
                    setCategory(result.data.products.length?result.data.products[0].category.slug:'')
                })
        }else{
            setGenerics([])
            setProducts([])
        }
    }

    useEffect(()=>{
        requestSearch()
    }, [searchVal])

    const renderGenerics = (generic)=>{
        return (generic || []).map((item, index)=>{
            return(
                <div key={index}>
                    <p className='mb-0'>{item}</p>
                </div>
            )
        })
    }

    const renderProducts = (products)=>{
        return (products || []).map((item, index)=>{
            return(
                <div key={index} className='d-flex'>
                    <div className={`${classes['product-image']} col-lg-4`}>
                        <LazyLoadImage image={{
                            src: item.images[0].original_image,
                            srcSet: item.images[0].image_srcset,
                            alt: item.name
                        }}/>
                    </div>
                    <div className={`${classes['product-name']} col-lg-8 align-items-center d-flex`}>
                        <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                            <a><p className='mb-0'>{item.name}</p></a>
                        </Link>
                    </div>
                </div>
            )
        })
    }


    return(
        <div className={`${classes['form-search-wrap']} col-lg-6 col-md-8 col-sm-8 col-6 position-relative`}>
            <Form inline className={`${classes['form-search-nav']} position-relative`}>
                <FormControl
                    type="text"
                    placeholder={t('header.search.placeholder')}
                    className="mr-sm-2 w-100"
                    value={searchVal}
                    onChange={(e)=>setSearchVal(e.target.value)}
                />
                <Button className={`position-absolute`} onClick={()=>{
                    if(category_slug!==''){
                        router.push({
                            pathname: '/products/[category_slug]',
                            query: {
                                category_slug
                            }
                        }, undefined, {locale})
                    }
                }}><Search /></Button>
                {generics.length || products.length ?
                    (<Button className={`position-absolute`} style={{right: '50px'}} onClick={()=>{
                        setGenerics([])
                        setProducts([])
                        setSearchVal('')
                    }}><X /></Button>):''}
            </Form>
            {generics.length && products.length ? (<div className={`${classes['result-search']} d-flex`}>
                <div className='col-lg-6 col-12  border-right-lg border-right-md-0 border-right-sm-0'>
                    {renderGenerics(generics)}
                </div>
                <div className='col-lg-6 d-lg-block d-none'>
                    {renderProducts(products)}
                </div>
            </div>): ''}
        </div>
    )
}
