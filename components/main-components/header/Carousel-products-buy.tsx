import {useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import Slider from "react-slick"
import Link from "next/link";
import {useRouter} from "next/router"
import {LazyLoadImage} from "../../preloader/Lazy-Load-Image";

/*---Styles----*/
import classes from "../../../styles/main-components/header/carousel-products-buy.module.sass";

/*---Icons---*/
import {ChevronLeft, ChevronRight} from "react-bootstrap-icons";
import {Cart} from '../../icons/Cart'
import {CostReplace} from "../../secondary-func";
import {map} from "react-bootstrap/ElementChildren";

/*---Hooks----*/
import {AxiosApi} from "../../../hooks/axios.hook"
import {Spinner} from "react-bootstrap";


/*---Interfaces----*/
interface IProducts{
    id: number,
    brand: {},
    category?: {},
    code: string,
    images: {
        image_srcset: string,
        original_image: string,
    }[],
    name: string,
    price: {
        old_price: number|null,
        price: number
    },
    slug: string,
    warehouse: {
        name: string,
        slug: string
    }
}

interface ICart{
    items: ICartItems[],
    total_price: number,
    total_quantity: number
}

interface ICartItems{
    cart_id: number,
    quantity: number,
    sku: IProducts
}



export function CarouselProductsBuy({
                                        products,
                                        count,
                                        cart,
                                        setCart,
                                        prevCount}:
                                        {
                                            products:IProducts[],
                                            count:number,
                                            cart: ICart,
                                            setCart: (value:any)=>void,
                                            prevCount:number}){

    const carousel = useRef(null)
    const {t} = useTranslation()
    const router = useRouter()
    const {locale} = router
    const {request, loading} = AxiosApi()

    const [stateButtons, setStateButtons] = useState({
        prevActive: false,
        nextActive: true
    })
    const total = 15


    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: count,
        slidesToScroll: count,
        autoplay: false,
        arrows: false,
        afterChange: (prev, next)=>{
            console.log(prev)
            setStateButtons({...stateButtons, nextActive: prev!==prevCount, prevActive: prev!==0})
        },
    }



    const clickNext = ()=>{
        carousel.current.slickNext()
    }

    const clickPrev = ()=>{
        carousel.current.slickPrev()
    }

    /*-----Проверка на продукт в корзину добавлен----*/

    const isAddCart = (slug, warehouse_slug)=>{
        let arr = ((cart&&cart.items) || []).filter(item=>item.sku.slug===slug && item.sku.warehouse.slug===warehouse_slug)
        return arr.length !== 0
    }



    /*-----Request добавить в корзину----*/

    const addToCart = async(e,warehouseSlug, slug)=>{

        const button = Array.from(e.currentTarget.getElementsByClassName(classes['spinner-button']))
        button.forEach(item=>{
            let btn = item as HTMLElement
            btn.classList.remove('d-none')
        })


        await request(`${process.env.ADD_TO_CART[locale]}/${warehouseSlug}/${slug}`, 'PUT')
            .then(result=>{

                setCart(result.data)

                button.forEach(item=>{
                    let btn = item as HTMLElement
                    btn.classList.add('d-none')
                })

            }).catch(e=>{
                console.log(e)
            })


    }

    return(
        <>
            <div className={`${classes['products-carousel-buy']} position-relative`}>
                {
                    products.length>4?( <button className={`position-absolute ${classes['controls-button']} ${stateButtons.prevActive?classes['active']:''}`} onClick={clickPrev}>
                        <ChevronLeft />
                    </button>):('')
                }
                <div className={`col-11 overflow-hidden mx-auto`}>
                    {
                        products.length>4?(<Slider {...settings} ref={carousel}>
                            {
                                (products || []).map((item, index)=>{

                                    if(total>=(index+1)){
                                        return(
                                            <div key={index} className='p-3 d-flex flex-column justify-content-between'>
                                                <div className={`${classes['top-info']} d-flex flex-column`}>
                                                    <div className=''>
                                                        <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                            <a>
                                                                <div className={`${classes['picture-product']} p-0`}>
                                                                    <LazyLoadImage image={{
                                                                        src: item.images[0].original_image,
                                                                        srcSet: item.images[0].image_srcset,
                                                                        alt: ''
                                                                    }}/>
                                                                </div>
                                                            </a>
                                                        </Link>
                                                    </div>
                                                    <div className=''>
                                                        <p className='mb-0'>
                                                            <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                                <a>{item.name}</a>
                                                            </Link>
                                                        </p>
                                                        <div>
                                                            <p className='mb-0 font-weight-bold'>{CostReplace(item.price.price + '')} СУМ</p>
                                                            {item.price && item.price.old_price?(<p className='mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                                        </div>
                                                    </div>
                                                </div>
                                                {
                                                    isAddCart(item.slug, item.warehouse.slug)?
                                                        (<Link href={'/cart'} locale={locale}>
                                                            <a className={`${classes['added-cart']} pr-0 mt-3 d-flex justify-content-center align-items-center`}>
                                                                <Cart />
                                                                <span className='position-static'>{t('button-added-cart')}</span>
                                                            </a>
                                                        </Link>):(
                                                            <button
                                                                className={`justify-content-center d-flex ${classes['btn-buy-product']} mt-3`}
                                                                disabled={loading}
                                                                onClick={(e)=>{
                                                                    addToCart(e,item.warehouse.slug, item.slug)

                                                                }}
                                                            >

                                                                <Cart />

                                                                <p className='mb-0'>{t('cart-button')}</p>
                                                                <Spinner animation="border" role="status" className={`d-none ${classes['spinner-button']}`}>
                                                                    <span className="sr-only">Loading...</span>
                                                                </Spinner>
                                                            </button>
                                                        )
                                                }

                                            </div>
                                        )
                                    }
                                    return ''
                                })
                            }
                        </Slider>):(<div className={`d-flex`}>{
                            (products || []).map((item, index)=>{
                                return(
                                    <div key={index} className='p-3 d-flex flex-column col-4'>
                                        <div className={`${classes['top-info']} d-flex flex-column`}>
                                            <div className=''>
                                                <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                    <a>
                                                        <div className={`${classes['picture-product']} p-0`}>
                                                            <LazyLoadImage image={{
                                                                src: item.images[0].original_image,
                                                                srcSet: item.images[0].image_srcset,
                                                                alt: ''
                                                            }}/>
                                                        </div>
                                                    </a>
                                                </Link>
                                            </div>
                                            <div className=''>
                                                <p className='mb-0'>
                                                    <Link href={`/product/${item.warehouse.slug}/${item.slug}`} locale={locale}>
                                                        <a>{item.name}</a>
                                                    </Link>
                                                </p>
                                                <div>
                                                    <p className='mb-0 font-weight-bold'>{CostReplace(item.price.price + '')} СУМ</p>
                                                    {item.price && item.price.old_price?(<p className='mb-0'>{CostReplace(item.price.old_price + '')} UZS</p>):''}
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            isAddCart(item.slug, item.warehouse.slug)?
                                                (<Link href={'/cart'} locale={locale}>
                                                    <a className={`${classes['added-cart']} pr-0 mt-3 d-flex justify-content-center align-items-center`}>
                                                        <Cart />
                                                        <span className='position-static'>{t('button-added-cart')}</span>
                                                    </a>
                                                </Link>):(
                                                    <button
                                                        className={`justify-content-center d-flex ${classes['btn-buy-product']} mt-3`}
                                                        disabled={loading}
                                                        onClick={(e)=>{
                                                            addToCart(e,item.warehouse.slug, item.slug)

                                                        }}
                                                    >

                                                        <Cart />

                                                        <p className='mb-0'>{t('cart-button')}</p>
                                                        <Spinner animation="border" role="status" className={`d-none ${classes['spinner-button']}`}>
                                                            <span className="sr-only">Loading...</span>
                                                        </Spinner>
                                                    </button>
                                                )
                                        }

                                    </div>
                                )
                            })
                        }</div>)
                    }


                </div>
                {
                    products.length>4?( <button className={`${stateButtons.nextActive?classes['active']:''} position-absolute ${classes['controls-button']}`} onClick={clickNext}>
                        <ChevronRight />
                    </button>):('')
                }

            </div>
        </>
    )
}


