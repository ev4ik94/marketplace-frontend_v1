cd /var/www/front_prod/

pwd

echo '--------------------------------->> Docker network create'
docker network create --driver overlay proxy_network || true

echo '--------------------------------->> Docker private registry login'
cat ./docker.registry.password.txt | docker login -u username --password-stdin registry.dev.farq.uz || true

echo '--------------------------------->> Docker pull registry.dev.farq.uz'
docker pull registry.dev.farq.uz/production-front:latest || true

echo '--------------------------------->> Docker stack deploy frontend'
docker stack deploy -c docker-compose.prod.yml --resolve-image always --with-registry-auth frontend || true

echo '--------------------------------->> Docker prune'
docker container prune -f || true
docker image prune -f || true

exit
