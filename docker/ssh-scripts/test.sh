cd /var/www/front_test/

pwd
echo '--------------------------------->> trying to compose down'
docker-compose -f docker-compose.test.yml down

echo '--------------------------------->> compose up'
docker-compose -f docker-compose.test.yml up -d

echo '--------------------------------->> copying next.config.js'
docker exec nextjs_test sh -c "cp -rf next.config.test.js next.config.js"

echo '--------------------------------->> docker prune'
docker container prune -f
docker image prune -f

echo '--------------------------------->> nginx restart to reload conf'
docker exec -it nginx_server sh -c "nginx -s reload"

echo '--------------------------------->> script is done'
exit
