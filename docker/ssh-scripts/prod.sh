#!/bin/bash

cd /var/www/front_prod/

pwd

echo '--------------------------------->> Building an image from the Dockerfile before tagging the image'
docker build --compress --no-cache -t registry.dev.farq.uz/production-front:latest -f ./docker/prod/Dockerfile ./

echo '--------------------------------->> Docker private registry login'
cat ./docker/ssh-scripts/docker.registry.password.txt | docker login -u username --password-stdin registry.dev.farq.uz

echo '--------------------------------->> Push the produced image'
docker image push registry.dev.farq.uz/production-front:latest

echo '--------------------------------->> Remove the produced image'
docker rmi registry.dev.farq.uz/production-front

echo '--------------------------------->> Docker prune'
docker container prune -f
docker image prune -f

echo '--------------------------------->> Copying the docker-compose.prod.yml to production server'
sshpass -p jMY8Awd6CAEGE2EY462HWHL5LA53sF scp -o StrictHostKeyChecking=no -r /var/www/front_prod/docker-compose.prod.yml /var/www/front_prod/docker/ssh-scripts/prod.remote.sh /var/www/front_prod/docker/ssh-scripts/docker.registry.password.txt username@185.74.6.139:/var/www/front_prod/

echo '--------------------------------->> Running Production server'
sshpass -p jMY8Awd6CAEGE2EY462HWHL5LA53sF ssh -o StrictHostKeyChecking=no username@185.74.6.139 sh /var/www/front_prod/prod.remote.sh

echo '--------------------------------->> script is done'
exit
