cd /var/www/front/

pwd
echo '--------------------------------->> trying to compose down'
docker-compose -f docker-compose.dev.yml down

echo '--------------------------------->> compose up'
docker-compose -f docker-compose.dev.yml up -d

echo '--------------------------------->> copying next.config.js'
docker exec nextjs_dev sh -c "cp -rf next.config.dev.js next.config.js"

echo '--------------------------------->> docker prune'
docker container prune -f
docker image prune -f

echo '--------------------------------->> nginx restart to reload conf'
docker exec -it nginx_server sh -c "nginx -s reload"

echo '--------------------------------->> script is done'
exit
