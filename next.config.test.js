const API = 'https://marketplace.elliesoft.com'
const VERSION = '1.0.0'
const API_PRODUCTION = 'https://api.test.dev.farq.uz'
const VERSION_PRODUCTION = 'v1.2'


module.exports = {
    i18n: {
        locales: ['en', 'ru', 'uz'],
        defaultLocale: 'ru',
        localeDetection: false
    },
    env: {
        API_CATEGORIES: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/categories`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/categories`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/categories`
        },
        API_BRANDS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/home_brands`, //https://farq-api.elliesoft.com/api/v1.1/brands
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/home_brands`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/home_brands`
        },
        API_BRAND: {
            "ru": `${API_PRODUCTION}/${VERSION_PRODUCTION}/brand`, //https://farq-api.elliesoft.com/api/v1.1/brand/{slug}
        },
        API_REFRESH_TOKEN: `${API_PRODUCTION}/sanctum/csrf-cookie`,
        API_REGISTER: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/register`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/register`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/register`
        },
        API_LOGIN: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/login`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/login`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/login`
        },
        API_LOGOUT: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/logout`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/logout`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/logout`
        },
        API_PRODUCTS_HOME: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/home_products`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/home_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/home_products`
        },
        API_PRODUCT_VIEW: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/product`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/product`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/product`
        },
        API_PRODUCTS_LIST:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/filter_products`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/filter_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/filter_products`
        },

        GET_POPULAR_PRODUCTS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/home_products`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/home_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/home_products`
        },



        //PROFILE API---------------------------------------------------------

        API_ADDRESS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/address`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/address`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/address`
        },
        API_GET_ADDRESS_LIST: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address/list`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address/list`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address/list`
        },
        API_GET_ADDRESS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address`, //https://api.dev.farq.uz/ru/v1.2/profile/address/{address_id} GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address`
        },
        API_ADD_ADDRESS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address/create`, //https://api.dev.farq.uz/ru/v1.2/profile/address/create   POST
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address/create`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address/create`
        },
        API_ADDRESS_DEFAULT: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address`, //https://api.dev.farq.uz/ru/v1.2/profile/address/make_default/{address_id}   PUT
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address`
        },
        API_EDIT_ADDRESS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address`, //https://api.dev.farq.uz/ru/v1.2/profile/address/{address_id}   PUT
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address`
        },
        API_DELETE_ADDRESS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/address`, //https://api.dev.farq.uz/ru/v1.2/profile/address/{address_id}   DELETE
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/address`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/address`
        },
        API_EDIT_NAME: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/edit_name`, //https://api.dev.farq.uz/ru/v1.2/profile/edit_name  POST
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/edit_name`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/edit_name`
        },
        API_EDIT_EMAIL: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/edit_email`, //https://api.dev.farq.uz/ru/v1.2/profile/change_email  POST
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/edit_email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/edit_email`
        },
        API_EDIT_PASSWORD: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/profile/change_password`, //https://api.dev.farq.uz/ru/v1.2/profile/change_password  POST
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/profile/change_password`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/profile/change_password`
        },

        // GET LIST DISTRICT, REGIONS API---------------------------------------------------

        API_GET_REGIONS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/address_list/regions`, //https://api.dev.farq.uz/ru/v1.2/address_list/regions  GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/address_list/regions`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/address_list/regions`
        },
        API_GET_DISTRICTS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/address_list/districts`, //https://api.dev.farq.uz/ru/v1.2/address_list/districts  GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/address_list/districts`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/address_list/districts`
        },

        //CART API-------------------------------------------------------------------

        GET_CART: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/cart_list`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/cart_list`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/cart_list`
        },
        DEVICE: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/device`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/device`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/device`
        },
        ADD_TO_CART: { //method PUT query warehouse_slug/slug
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/cart_add`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/cart_add`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/cart_add`
        },
        REMOVE_TO_CART: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/cart_remove`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/cart_remove`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/cart_remove`
        },

        EDIT_TO_CART: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/cart_edit`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/cart_edit`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/cart_edit`
        },

        NEWS: {
            "ru": `${API}/ru/${VERSION}/blog`,
            "uz": `${API}/uz/${VERSION}/blog`,
            "en": `${API}/en/${VERSION}/blog`
        },
        COMPARES_GET_LIST: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_list`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_list`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_list`
        },
        COMPARES_ADD: { //method PUT query warehouse_slug/slug
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_add`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_add`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_add`
        },
        COMPARES_REMOVE: { //method DELETE query warehouse_slug/slug
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_remove`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_remove`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_remove`
        },
        COMPARES_REMOVE_ALL: { //method DELETE
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_remove_all`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_remove_all`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_remove_all`
        },
        COMPARES_REMOVE_ALL_ADD: { //method DELETE
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_rem_add`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_rem_add`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_rem_add`
        },

        COMPARES_SUGGESTIONS: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_suggestions`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_suggestions`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_suggestions`
        },
        COMPARES_SUGGEST: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/comparison_suggest`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/comparison_suggest`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/comparison_suggest`
        },
        GET_RECENTLY_LIST: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/recently_products`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/recently_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/recently_products`
        },
        REMOVE_RECENTLY_ALL: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/remove_all_recently_viewed`, //https://api.dev.farq.uz/ru/v1.2/remove_all_recently_viewed // DELETE
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/remove_all_recently_viewed`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/remove_all_recently_viewed`
        },
        REMOVE_RECENTLY: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/remove_recently_viewed`, //https://api.dev.farq.uz/ru/v1.2/remove_recently_viewed/{warehouse_slug}/{sku_slug} // DELETE
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/remove_recently_viewed`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/remove_recently_viewed`
        },

        ORDER_SEND: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/place_order`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/place_order`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/place_order`
        },
        GET_TRACK_ORDER_ID: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/track`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/track`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/track`
        },

        GET_SAVED:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/saved_products`,
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/saved_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/saved_products`
        },
        ADD_SAVED:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/add_saved_products`, //https://api.dev.farq.uz/ru/v1.2/add_saved_products/{warehouse_slug}/{sku_slug} // PUT
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/add_saved_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/add_saved_products`
        },
        REMOVE_ALL_SAVED:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/remove_all_saved_products`, //https://api.dev.farq.uz/ru/v1.2/remove_all_saved_products // DELETE
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/remove_all_saved_products`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/remove_all_saved_products`
        },
        REMOVE_SAVED:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/remove_saved_product`, //https://api.dev.farq.uz/ru/v1.2/remove_saved_product/{warehouse_slug}/{sku_slug} // DELETE
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/remove_saved_product`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/remove_saved_product`
        },
        SEND_EMAIL_CODE:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/send_email_verification`, //https://api.dev.farq.uz/ru/v1.2/auth/send_email_verification // POST params: email
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/send_email_verification`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/send_email_verification`
        },
        VERIFICATION_EMAIL:{
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/verify_email`, //https://api.dev.farq.uz/ru/v1.2/auth/verify_email // POST params: email, code
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/verify_email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/verify_email`
        },

        FORGET_PASSWORD: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/forgot-password-email`, //https://api.dev.farq.uz/ru/v1.2/auth/forgot-password-email // POST params: email
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/forgot-password-email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/forgot-password-email`

        },

        FILTER_PRODUCT: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/get_filters`, //https://api.dev.farq.uz/ru/v1.2/get_filters?category_slug=112 // GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/get_filters`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/get_filters`

        },

        SEARCH: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/live/search`, //https://api.dev.farq.uz/ru/v1.2/live/search?q={string} // GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/live/search`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/live/search`

        },

        /*--------FORGOT PASSWORD--------------------*/

        PASSWORD_FORGOT_EMAIL: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/forgot-password-email`, //https://api.dev.farq.uz/ru/v1.2/auth/forgot-password-email  // POST  {email}
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/forgot-password-email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/forgot-password-email`
        },
        PASSWORD_FORGOT_CODE: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/retrive-reset-password-code-email`, //https://api.dev.farq.uz/ru/v1.2/auth/retrive-reset-password-code-email   // POST  {email, code}
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/retrive-reset-password-code-email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/retrive-reset-password-code-email`
        },
        PASSWORD_FORGOT_RESET: {
            "ru": `${API_PRODUCTION}/ru/${VERSION_PRODUCTION}/auth/reset-password-email`, //https://api.dev.farq.uz/ru/v1.2/auth/reset-password-email    // POST  {email, token, password, password_confirmation}
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/auth/reset-password-email`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/auth/reset-password-email`
        },
        /*-------------Track Order-------*/

        TRACK_ORDER: {
            "ru": `https://api.test.dev.farq.uz/ru/v1.2/track`, //https://api.dev.farq.uz/ru/v1.2/track/{id} // GET
            "uz": `${API_PRODUCTION}/uz/${VERSION_PRODUCTION}/track`,
            "en": `${API_PRODUCTION}/en/${VERSION_PRODUCTION}/track`

        }

    }
}
