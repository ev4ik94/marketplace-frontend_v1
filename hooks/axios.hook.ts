import {useState, useCallback, useEffect} from 'react'
import {useRouter} from "next/router"
import axios from 'axios'

/*---Functions---*/
import {randomId} from "../components/secondary-func";



export function AxiosApi(){

    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const router = useRouter()
    const {locale}  = router

    /*-----Abort Controller -----*/

    const source = axios.CancelToken.source()
    var count = 0

    const resetErr = ()=>{
        setError(null)
    }



    const request = useCallback(async(url, method='GET', data=null, headers={})=>{
        setLoading(true)
        const instance = axios.create({
            withCredentials: true,
            cancelToken: source.token
        })



        await instance.interceptors.request.use(request=>{
              
                if(!localStorage.getItem('deviceId')){
                    let random_id = localStorage.getItem('randomId')

                    if (!random_id) {
                        random_id = randomId()
                        localStorage.setItem('randomId', random_id)
                    }

                    request.headers['random-id'] = random_id

                } else {
                    const deviceId = localStorage.getItem('deviceId')

                    request.headers['device-id'] = deviceId
                }
                return request
            },
            error=>{
                return Promise.reject(error)
            })


        await instance.interceptors.response.use(response=>{
                setLoading(false)
            if(response.headers['device-id']){

                localStorage.setItem('deviceId', response.headers['device-id'])

                if(!!localStorage.getItem('randomId')){
                    localStorage.removeItem('randomId')
                }
            }
            return response
            },
            error=>{

            setLoading(false)
            const {response, config} = error

                if(response && response.status){
                    switch(response.status){
                        case 400:
                            setError(response.data)
                            break;
                        case 401:
                            if(window!==undefined) {
                                (window as any).request = JSON.stringify({
                                    method: 'POST',
                                    url:  process.env.API_LOGOUT['ru']
                                })
                            }
                            instance({
                                method: 'POST',
                                url: process.env.API_LOGOUT['ru']
                            }).then(()=> {
                                localStorage.removeItem('user')
                                router.push('/', undefined, {locale})
                            }).catch(e=>console.log(e))

                            break;
                        case 403:
                            console.log('403')
                            break;
                        case 404:
                            console.log('404')
                            break;
                        case 419:

                            count+=1
                            if(count<=1){
                                new Promise((resolve, reject)=>{
                                    resolve(document.cookie = 'test_session=; Domain=.farq.uz; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;')
                                }).then(()=>{
                                    console.log(document.cookie)
                                    instance({
                                        method: 'GET',
                                        url: process.env.API_REFRESH_TOKEN
                                    })
                                }).then(()=>{
                                    document.cookie = 'XSRF-TOKEN=; Domain=.farq.uz; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;'
                                    if((window as any).request===null || !(window as any).request){

                                        (window as any).request = JSON.stringify({
                                            method,
                                            url
                                        })
                                    }

                                    const reqData = JSON.parse((window as any).request)

                                    instance({
                                        method: reqData.method || 'GET',
                                        url: reqData.url || ''
                                    }).catch(e=>alert(e))
                                }).then(()=>{
                                    count = 0;
                                    (window as any).request = null
                                })

                            }
                            break;
                        case 500:
                            console.log('500')
                            break;
                        default:
                            break;
                    }
                }else{
                    if(error && error!==null){
                        console.log('error server')
                    }
                }
            return Promise.reject(error)

            })


        return await instance({
            method,
            url,
            data,
            headers
        })







    }, [])

    return {request, loading, error, resetErr}
}
