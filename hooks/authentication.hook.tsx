import {useState} from 'react'
import {useRouter} from "next/router"
import {AxiosApi} from "./axios.hook"

var CryptoJS = require("crypto-js");

import {decryptString} from '../components/secondary-func'


/*---Interface---*/
interface IUser{
    id: number,
    name: string,
    lastName: string,
    email: string,
    password: string,
    address: string,
    phone: string
}

export function useAuth(){
    const [user, setUser] = useState<IUser | null>(null)
    const router = useRouter()
    const {request} = AxiosApi()

    const login = (data)=>{
        var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), 'secret_key_farq-uJ8CKmiX').toString();
        localStorage.setItem('user', ciphertext)
        setUser(data)
    }



    const logout = async()=>{
        request(process.env.API_LOGOUT['ru'], 'POST').then(()=>{
            localStorage.removeItem('user')
            setUser(null)
            router.push('/')
        }).catch(e=>console.log(e.message))

    }

    const checkAuth = ()=>{
         return localStorage.getItem('user')!==null
    }

    const getUserData = ()=>{
        const userData = decryptString(localStorage.getItem('user'))
        return userData
    }

    return {user, login, logout, checkAuth, getUserData}

}
