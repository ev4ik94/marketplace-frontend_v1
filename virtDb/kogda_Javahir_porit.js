
export const productTest = {
    items: [
        {
            brand: 'Shivaki',
            category: {},
            code: 0,
            images: [
                {
                    image_srcset: '/images/pic1.png',
                    original_image: '/images/pic1.png'
                },
                {
                    image_srcset: '/images/pic2.png',
                    original_image: '/images/pic2.png'
                },
                {
                    image_srcset: '/images/pic3.png',
                    original_image: '/images/pic3.png'
                },
                {
                    image_srcset: '/images/pic4.png',
                    original_image: '/images/pic4.png'
                }
            ],
            name: 'Product Test',
            price: {
                price: 12000,
                old_price: 9000
            },
            shop: {
                name: 'string',
                slug: 'string'
            },
            slug: 'artel_test_product',
            warehouse: {
                name: 'artel-product-test1',
                slug: 'artel-product-test1'
            }
        },
        {
            brand: 'Shivaki',
            category: {},
            code: 0,
            images: [
                {
                    image_srcset: '/images/pic1.png',
                    original_image: '/images/pic1.png'
                },
                {
                    image_srcset: '/images/pic2.png',
                    original_image: '/images/pic2.png'
                },
                {
                    image_srcset: '/images/pic3.png',
                    original_image: '/images/pic3.png'
                },
                {
                    image_srcset: '/images/pic4.png',
                    original_image: '/images/pic4.png'
                }
            ],
            name: 'Product Test',
            price: {
                price: 12000,
                old_price: 9000
            },
            shop: {
                name: 'string',
                slug: 'string'
            },
            slug: 'artel_test_product',
            warehouse: {
                name: 'artel-product-test1',
                slug: 'artel-product-test1'
            }
        },
        {
            brand: 'Shivaki',
            category: {},
            code: 0,
            images: [
                {
                    image_srcset: '/images/pic1.png',
                    original_image: '/images/pic1.png'
                },
                {
                    image_srcset: '/images/pic2.png',
                    original_image: '/images/pic2.png'
                },
                {
                    image_srcset: '/images/pic3.png',
                    original_image: '/images/pic3.png'
                },
                {
                    image_srcset: '/images/pic4.png',
                    original_image: '/images/pic4.png'
                }
            ],
            name: 'Product Test',
            price: {
                price: 12000,
                old_price: 9000
            },
            shop: {
                name: 'string',
                slug: 'string'
            },
            slug: 'artel_test_product',
            warehouse: {
                name: 'artel-product-test1',
                slug: 'artel-product-test1'
            }
        },
        {
            brand: 'Shivaki',
            category: {},
            code: 0,
            images: [
                {
                    image_srcset: '/images/pic1.png',
                    original_image: '/images/pic1.png'
                },
                {
                    image_srcset: '/images/pic2.png',
                    original_image: '/images/pic2.png'
                },
                {
                    image_srcset: '/images/pic3.png',
                    original_image: '/images/pic3.png'
                },
                {
                    image_srcset: '/images/pic4.png',
                    original_image: '/images/pic4.png'
                }
            ],
            name: 'Product Test',
            price: {
                price: 12000,
                old_price: 9000
            },
            shop: {
                name: 'string',
                slug: 'string'
            },
            slug: 'artel_test_product',
            warehouse: {
                name: 'artel-product-test1',
                slug: 'artel-product-test1'
            }
        }
    ]
}
