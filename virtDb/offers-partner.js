

export const offers = [
    {
    id: 13,
    name: 'Телевизоры с большим экраном',
    title:"Рекомендуемые товары",
    link_name: 'Выбрать телевизор',
    skus: [
    {
        id: 14,
        image_urls: [
            {
                md: 'images/TV.jpg',
                org: 'images/TV.jpg',
                sm: 'images/TV.jpg',
                xs: 'images/TV.jpg'
            }
        ],
        description: "Получите наилучшее изображение на телевизоре 8K. Телевизор 8K с разрешением в 4 раза больше, чем 4K-телевизор, обеспечивает наилучшее возможное разрешение для вашего домашнего кинотеатра.",
        price: 683000,
        text_1: '',
        text_2: '',
        slug: "televizor"
    }

    ]

    },
    {
        id: 15,
        name: 'Июньское мероприятие по бытовой технике',
        title:"Бытовая техника макрособытие",
        link_name: 'Предложения бытовой техники',
        skus: [
            {
                id: 14,
                image_urls: [
                    {
                        md: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-MMT-369657-210111_DER-207574.jpg;maxHeight=504;maxWidth=740',
                        org: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-MMT-369657-210111_DER-207574.jpg;maxHeight=504;maxWidth=740',
                        sm: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-MMT-369657-210111_DER-207574.jpg;maxHeight=504;maxWidth=740',
                        xs: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-MMT-369657-210111_DER-207574.jpg;maxHeight=504;maxWidth=740'
                    }
                ],
                description: "Откройте для своего дома новые возможности с великолепной кухонной техникой и оборудованием для стирки.",
                price: 683000,
                text_1: '',
                text_2: '',
                slug: "gazovye-plity"
            }

        ]

    },
    {
        id: 16,
        name: 'Лучшие предложения сотовых телефонов.',
        title:"Предложение сотовых телефонов",
        link_name: "Выбрать телефон",
        skus: [
            {
                id: 14,
                image_urls: [
                    {
                        md: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-mmt343644-201130_DER-203318.jpg;maxHeight=504;maxWidth=740',
                        org: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-mmt343644-201130_DER-203318.jpg;maxHeight=504;maxWidth=740',
                        sm: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-mmt343644-201130_DER-203318.jpg;maxHeight=504;maxWidth=740',
                        xs: 'https://pisces.bbystatic.com/image2/BestBuy_US/Gallery/pol-mmt343644-201130_DER-203318.jpg;maxHeight=504;maxWidth=740'
                    }
                ],
                description: "Активируйте свой новый телефон в магазине или онлайн.",
                price: 683000,
                text_1: '',
                text_2: '',
                slug: "smartfony"
            }

        ]

    },
    {
        id: 17,
        name: 'Продукция Artel по доступным ценам',
        title:"Предложение Artel",
        link_name: "Продукция Artel",
        skus: [
            {
                id: 14,
                image_urls: [
                    {
                        md: 'images/artel.jpg',
                        org: 'images/artel.jpg',
                        sm: 'images/artel.jpg',
                        xs: 'images/artel.jpg'
                    }
                ],
                description: "Готовите ли вы что-то новое, ищете способы расслабиться или просто оставаться на связи с самыми важными людьми, Artel дает вам больше, чем вы ожидали.",
                price: 683000,
                text_1: '',
                text_2: '',
                slug: "avtomaticeskie"
            }

        ]

    },
    {
        id: 18,
        name: 'Сэкономьте до 200 000 на микроволновых печах',
        title:"Акция на Микроволновые печи",
        link_name: "Экономить сейчас",
        skus: [
            {
                id: 14,
                image_urls: [
                    {
                        md: 'images/microwave.jpg',
                        org: 'images/microwave.jpg',
                        sm: 'images/microwave.jpg',
                        xs: 'images/microwave.jpg'
                    }
                ],
                description: "Экономия на лицо. 200 тысяч на микроволновые печи любой продукции только на FARQ",
                price: 683000,
                text_1: '',
                text_2: '',
                slug: "mikrovolnovaya-pec"
            }

        ]

    },
    {
        id: 19,
        name: 'Сделка недели',
        title:"Предложение",
        link_name: "Не пропустите",
        skus: [
            {
                id: 14,
                image_urls: [
                    {
                        md: 'images/deal-of-week.jpg',
                        org: 'images/deal-of-week.jpg',
                        sm: 'images/deal-of-week.jpg',
                        xs: 'images/deal-of-week.jpg'
                    }
                ],
                description: "Отличные предложения. Каждый день.",
                price: 683000,
                text_1: '',
                text_2: '',
                slug: "mikrovolnovaya-pec"
            }

        ]

    }
]
