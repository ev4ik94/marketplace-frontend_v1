

export const help = [
    {
        name: 'Lorem Ipsum',
        title: 'Lorem Ipsum',
        icon: '',
        slug: 'slug-1',
        children: [{
                name: 'Lorem Ipsum-child1',
                title: 'Lorem Ipsum-child1',
                icon: '',
                slug: 'slug-child-1',
                children: []
            },
            {
                name: 'Lorem Ipsum-child2',
                title: 'Lorem Ipsum-child2',
                icon: '',
                slug: 'slug-child-2',
                children: []
            },
            {
                name: 'Lorem Ipsum-child3',
                title: 'Lorem Ipsum-child3',
                icon: '',
                slug: 'slug-child-3',
                children: []
            }
        ]
    },
    {
        name: 'Lorem Ipsum',
        title: 'Lorem Ipsum',
        icon: '',
        slug: 'slug-2',
        children: [{
            name: 'Lorem Ipsum-child1',
            title: 'Lorem Ipsum-child1',
            icon: '',
            slug: 'slug-child-2',
            children: []
        },
            {
                name: 'Lorem Ipsum-child2',
                title: 'Lorem Ipsum-child2',
                icon: '',
                slug: 'slug-child-3',
                children: []
            },
            {
                name: 'Lorem Ipsum-child3',
                title: 'Lorem Ipsum-child3',
                icon: '',
                slug: 'slug-child-4',
                children: []
            }
        ]
    },
    {
        name: 'Lorem Ipsum',
        title: 'Lorem Ipsum',
        icon: '',
        slug: 'slug-5',
        children: [{
            name: 'Lorem Ipsum-child1',
            title: 'Lorem Ipsum-child1',
            icon: '',
            slug: 'slug-child-6',
            children: []
        },
            {
                name: 'Lorem Ipsum-child2',
                title: 'Lorem Ipsum-child2',
                icon: '',
                slug: 'slug-child-7',
                children: []
            },
            {
                name: 'Lorem Ipsum-child3',
                title: 'Lorem Ipsum-child3',
                icon: '',
                slug: 'slug-child-8',
                children: []
            }
        ]
    },
    {
        name: 'Lorem Ipsum',
        title: 'Lorem Ipsum',
        icon: '',
        slug: 'slug-9',
        children: [{
            name: 'Lorem Ipsum-child1',
            title: 'Lorem Ipsum-child1',
            icon: '',
            slug: 'slug-child-10',
            children: []
        },
            {
                name: 'Lorem Ipsum-child2',
                title: 'Lorem Ipsum-child2',
                icon: '',
                slug: 'slug-child-12',
                children: []
            },
            {
                name: 'Lorem Ipsum-child3',
                title: 'Lorem Ipsum-child3',
                icon: '',
                slug: 'slug-child-13',
                children: []
            }
        ]
    }
]
