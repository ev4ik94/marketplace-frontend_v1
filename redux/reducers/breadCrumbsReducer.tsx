
const breadCrumbsReducer = (state=[], action)=>{
    switch(action.type){
        case 'set-breadcrumbs':
            return action.payload
        default:
            return state
    }
}

export default breadCrumbsReducer
