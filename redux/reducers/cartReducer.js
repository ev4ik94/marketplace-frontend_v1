import {GET_CART, SET_CART} from '../constants'

const cartData = {
    items: [],
    total_price: 0,
    total_quantity: 0
}

const cartReducer = (state=cartData, action)=>{
    switch(action.type){
        case GET_CART:
            return action.payload
        case SET_CART:
            return action.payload
        default:
            return state
    }
}

export default cartReducer
