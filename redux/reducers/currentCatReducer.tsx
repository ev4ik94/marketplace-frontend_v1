
const filtersReducer = (state='', action)=>{
    switch(action.type){
        case 'set-category':
            return action.payload
        default:
            return state
    }
}

export default filtersReducer
