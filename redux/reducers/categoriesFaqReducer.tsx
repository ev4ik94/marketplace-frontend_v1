import {SET_CATEGORIES_FAQ} from '../constants'

const categoriesFaqReducer = (state=[], action)=>{
    switch(action.type){
        case SET_CATEGORIES_FAQ:
            return action.payload
        default:
            return state
    }
}

export default categoriesFaqReducer
