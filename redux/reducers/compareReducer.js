import {GET_COMPARE, DELETE_COMPARE, SET_COMPARE} from '../constants'


const compareReducer = (state={items:[], attribute_groups: [], category_slug:''}, action)=>{
    switch(action.type){
        case GET_COMPARE:
            return action.payload
        case SET_COMPARE:
            return action.payload
        case DELETE_COMPARE:
            return action.payload
        default:
            return state
    }
}

export default compareReducer
