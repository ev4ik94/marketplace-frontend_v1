import categoriesReducer from './categoriesReducers'
import compareReducer from './compareReducer'
import productReducer from "./productReducer"
import cartReducer from "./cartReducer"
import formReducer from './formReducer'
import brandsReducer from './brandsReducer'
import recentlyReducer from "./recentlyReducer"
import popularReducer from "./popularReducer"
import savedReducer from "./savedReducer"
import categoriesFaqReducer from "./categoriesFaqReducer"
import regionsReducer from "./regionsReducer"
import districtReducer from "./districtReducer"
import filtersReducer from "./filtersReducer"
import currentCatReducer from "./currentCatReducer"
import breadCrumbsReducer from "./breadCrumbsReducer"
import loadingReducer from "./loadingReducer"
import {combineReducers} from 'redux'


const rootReducers = combineReducers({
    categories: categoriesReducer,
    compare: compareReducer,
    product: productReducer,
    cart: cartReducer,
    form: formReducer,
    brands: brandsReducer,
    recently: recentlyReducer,
    popular: popularReducer,
    categoriesFaq: categoriesFaqReducer,
    saved: savedReducer,
    regions: regionsReducer,
    district: districtReducer,
    filters: filtersReducer,
    currentCat: currentCatReducer,
    breadCrumbs: breadCrumbsReducer,
    loadingState: loadingReducer

})

export default rootReducers
