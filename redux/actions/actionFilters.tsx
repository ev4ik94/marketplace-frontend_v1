import {SET_FILTERS} from "../constants";


/*--------------Interface----------------*/

interface IFilters{
    attributes: [],
    brands: [],
    breadcrumbs: [],
    categories: {},
    price: {}
}

export function setFilters(filters:IFilters, lang?:string){
    return function (dispatch){
        return dispatch({type:SET_FILTERS, payload: filters})
    }
}

