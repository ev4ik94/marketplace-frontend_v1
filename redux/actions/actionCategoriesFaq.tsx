import {SET_CATEGORIES_FAQ} from "../constants";


export const setCategoriesFaq = (categories)=>{
    return async function (dispatch){
        dispatch({type:SET_CATEGORIES_FAQ, payload: categories})
    }
}
