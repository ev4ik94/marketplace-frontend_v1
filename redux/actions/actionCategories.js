import { SET_CATEGORIES } from '../constants'


export const setCategories = (categories) => {

    return async function(dispatch) {

        dispatch({ type: SET_CATEGORIES, payload: categories })

    }
}