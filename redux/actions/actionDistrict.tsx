import {SET_DISTRICT} from "../constants";


/*--------------Interface----------------*/

interface IDistrict{
    code: string,
    country_id: number,
    id: number,
    name: string
}

export function setDistrict(regions:IDistrict[], lang?:string){
    return function (dispatch){
        return dispatch({type:SET_DISTRICT, payload: regions})
    }
}
