
export function setCurrentCategory(category: string){
    return function (dispatch){
        return dispatch({type:'set-category', payload: category})
    }
}
