

export function setBreadCrumbs(breadCrumbs){
    return async function (dispatch){
        dispatch({type:'set-breadcrumbs', payload: breadCrumbs})
    }
}
