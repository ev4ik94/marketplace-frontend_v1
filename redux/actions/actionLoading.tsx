import {SET_LOADING} from "../constants";

export function setLoading(loading){
    return function (dispatch){
        return dispatch({type:SET_LOADING, payload: loading})
    }
}

