import {SET_REGIONS} from "../constants";


/*--------------Interface----------------*/

interface IRegions{
    code: string,
    country_id: number,
    id: number,
    name: string
}

export function setRegions(regions:IRegions[], lang?:string){
    return function (dispatch){
        return dispatch({type:SET_REGIONS, payload: regions})
    }
}
