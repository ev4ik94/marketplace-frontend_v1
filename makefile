# Push to dev
push_dev: show_time
	@git branch -d dev
	@git branch dev
	@git checkout dev
	@git push -u origin dev
	@git checkout master


# Push to dev
push_test: show_time
	@git branch -d test
	@git branch test
	@git checkout test
	@git push -u origin test
	@git checkout master


# Push to staging
push_staging: show_time
	@git branch -d staging
	@git branch staging
	@git checkout staging
	@git push -u origin staging
	@git checkout master


# Push to production
push_prod: show_time
	@git branch -d production
	@git branch production
	@git checkout production
	@git push -u origin production
	@git checkout master


# Tells the time
show_time:
	@sh -c "echo 'Current time: ' && date +"%T""
